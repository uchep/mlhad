"""
# hadronization.py is a part of the MLHAD package.
# Copyright (C) 2023 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import torch
from torch import nn
from data.PDB import ParticleDatabase
import numpy as np
from pythia8 import Vec4
from models.load_2D_flow import load_cBNF, load_BNF
from data.flavor_selector import prob_flavor_selection

class Hadronization():
	def __init__(self, model, conditional = True):
		"""
		Class contains neccesary functions to produce full hadronization events from 
		trained ML model

		model (string): ---- Path to model file
		conditional (bool) : Conditioned model
		"""
		self.pd = ParticleDatabase()
		self.conditional = conditional

		# Load the model
		if self.conditional: 
			self.kinematic_generator, self.conditions_min_max = load_cBNF(model, print_details = False, deterministic = True)
		else:
			self.kinematic_generator = load_BNF(model)

	def label_encoding(self, condition_dict):
		"""
		Returns condition label

		conditions (dictionary): condition keys and values to be fed into kinematic generator

		Returns torch tensor of labels for the generator to condition on
		"""
		label = []
		m_cond, E_cond = 0,0
		# There is an implicit dependence on ordering of the label, must be the same as training dataset,
		# we stick to the convention that if there is a mass label, it will always be first in the label.
		for key in condition_dict.keys():
			if key == 'm':
				m_cond += 1
			elif key == 'E':
				E_cond += 1

		if m_cond == 1:
			m_min = self.conditions_min_max['m'][0]
			m_max = self.conditions_min_max['m'][1]
			label.append((condition_dict['m'] - m_max)/(m_min - m_max))
		if E_cond == 1:
			E_min = self.conditions_min_max['E'][0]
			E_max = self.conditions_min_max['E'][1]
			label.append(np.abs((condition_dict['E'] - E_max)/(E_min - E_max)))

		# Convert to torch tensor
		label = torch.Tensor(label)
		label = label.repeat(1,1)
		return label

	def model_kinematics(self):
		"""
		Generates px, py, pz values from trained BNF model to be used in the fragmentation chain
		
		Returns px, py, pz floats
		"""
		label = torch.tensor([0.])
		label = label.repeat(1,1)
		# Run the latent space point through the network
		pz_pT = self.kinematic_generator(torch.randn(2), c = [label], rev = True)[0][0].detach().numpy()
		
		# Sample uniform phi
		phi = 2 * np.pi * np.random.uniform()
		# Return px, py, pz
		return pz_pT[1] * np.cos(phi), pz_pT[1] * np.sin(phi), pz_pT[0] 

	def model_kinematics_conditional(self, condition_dict, s_px, s_py):
		"""
		Generates px, py, pz values from trained BNF model to be used in the fragmentation chain
		
		Returns px,py,pz floats
		"""
		# Create label from input dict
		label = self.label_encoding(condition_dict)
		
		# Produce an initial sampling of the transverse momentum.
		pz_pT = self.kinematic_generator(torch.randn(2), c = [label], rev=True)[0][0]
		
		# Collect the pT value of the new string break
		pT = pz_pT[1].detach().numpy()
		# Add this to the previous string break tranverse momentum
		phi = 2 * np.pi * np.random.uniform()
		px_h = pT * np.cos(phi) + s_px
		py_h = pT * np.sin(phi) + s_py
		pT_h = torch.tensor(np.sqrt(px_h**2 + py_h**2))
		
		# Compute the transverse mass
		mT = torch.sqrt(torch.pow(pT_h, 2) + condition_dict['m']**2)

		# Compute new label
		condition_dict['m'] = mT
		label = self.label_encoding(condition_dict)

		pz = self.kinematic_generator(torch.randn(2), c = [label], rev=True)[0].detach().numpy()[0][0]
		
		# Sample uniform phi
		#phi = 2 * np.pi * np.random.uniform()
		#pT = pT.detach().numpy()
		# Note that the px and py values here represent the new string end px and py
		px_s = -pT * np.cos(phi)
		py_s = -pT * np.sin(phi)

		# Return px, py, pz
		return px_h, py_h, pz, px_s, py_s

	def get_likelihood(self, pz_pT, condition):
		"""
		Compute the assigned model likelihood for a given phase space point (pz, pT)
		"""
		# Define the 2D standard normal dist PDF
		gauss_2D = lambda x, y: (1. / np.sqrt(2. * np.pi)) * np.exp(-((x**2 + y**2) / 2.))
	
		# Feed into the model to return latent variable and log(|jac|)
		z, log_jac_det = self.kinematic_generator(pz_pT, c = [condition])
		
		# Convert to numpy arrays for analysis
		z = z.detach().numpy()[0]
		log_jac_det = log_jac_det.detach().numpy()
		
		# Compute the likelihood for each item in the ensemble
		pz_pT_likelihood = gauss_2D(z[0], z[1]) * np.exp(log_jac_det)
		
		return pz_pT_likelihood[0]

	def model_kinematics_conditional_weighted(self, weight_switch):
		"""
		Generates px, py, pz values from trained BNF model to be used in the fragmentation chain

		weight_switch -> binary int (0 or 1) : Determines which of the two conditions is sim vs exp
		
		Returns px,py,pz floats
		"""
		# Assume that there are only two conditions and we want to reweigh from one to the other.
		# Take the first label as 'simulated' condition and the second label as 'experimental' condition

		# Create label from input dict
		if weight_switch == 0:
			label_sim = torch.Tensor([0.])
			label_sim = label_sim.repeat(1,1)
			label_exp = torch.Tensor([1.])
			label_exp = label_exp.repeat(1,1)
		else:
			label_exp = torch.Tensor([0.])
			label_exp = label_exp.repeat(1,1)
			label_sim = torch.Tensor([1.])
			label_sim = label_sim.repeat(1,1)

		# Run latent space point through the network
		pz_pT = self.kinematic_generator(torch.randn(2), c = [label_sim], rev=True)[0]

		# Compute the likelihoods for each condition
		likelihood_sim = self.get_likelihood(pz_pT, label_sim)
		
		likelihood_exp = self.get_likelihood(pz_pT, label_exp)

		# Define weight
		weight = likelihood_exp / likelihood_sim

		# Detach and convert to numpy array
		pz_pT = pz_pT.detach().numpy()[0]

		# Sample uniform phi
		phi = 2 * np.pi * np.random.uniform()
		# Return px, py, pz
		return pz_pT[1] * np.cos(phi), pz_pT[1] * np.sin(phi), pz_pT[0], weight

	def fragmentation(self, p1, p2, id1, id2, E_threshold, string_end, E_partons = 50.0, weighted = False, weight_switch = 0):
		"""
		This function generates a fragmentation event by boosting to the string systems CM frame, generating
		hadronic emission kinematics, and then boosting back to original frame.
	
		p1 (list): --------- Hadronized end four-momentum components input as list
		p2 (list): --------- Spectator end four-momentum components input as list
		id1 (int): --------- String id corresponding to hadronizing end (p1)
		id2 (int): --------- String id corresponding to non-hadronizing end (p2)
		E_threshold (float): IR center of mass energy cutoff for the fragmentation process
		string_end (string): Fragmenting string end input either 'endA' or 'endB'
		E_partons (float): - Energy of initial parton in the CM frame
	
		Returns new string and hadron ids and momenta
		"""
		# Generate new string and hadron IDs (only supports pions)
		new_IDs       = prob_flavor_selection(id1)
		new_string_id = -new_IDs[0]
		new_had_id    = new_IDs[1]
		
		v1 = Vec4(*p1)
		v2 = Vec4(*p2)

		px_i = v1.px()
		py_i = v1.py()
		E_i = v1.e()

		# Boost to the string system COM
		p_sum = v1 + v2
		v1.bstback(p_sum)
		
		# Rotate such that v1 is completely along the z-axis
		theta = v1.theta()
		phi   = v1.phi()
	
		v1.rot(0.0, -phi)
		v1.rot(-theta, phi)
		
		# Sample from new string system CM with energy W^2 = W^+ W^- 
		# Could also just use the energy from the v1 four-vector 
		# E_CM = sqrt((2.*sqrt((p1[0]+px)**2 + (p1[1]+py)**2 + (p1[2])**2)) * (2.*p2[3]))/2.
		E_CM = v1.e()

		# Generate kinematics using the CM energy of string end p1 if above threshold
		if E_CM > E_threshold:
			# Sample px, py, pz from trained model
			if self.conditional:
				condition_dict = {}
				for key in self.conditions_min_max.keys():
					if key == 'm':
						condition_dict['m'] = self.pd[new_had_id].mass
					elif key == 'E':
						condition_dict['E'] = E_CM

				if weighted:
					px, py, pz, weight = self.model_kinematics_conditional_weighted(weight_switch)
					# Incorporate artificial rescaling of energy
					pz = pz * E_CM 
				else:
					px, py, pz, px_s, py_s = self.model_kinematics_conditional(condition_dict, px_i, py_i)
					# Incorporate artifical rescaling of energy
					pz = pz * E_CM
			else:
				
				px, py, pz = self.model_kinematics()
				# Without boundaries implemented in to the network framework we need to ensure 
				# that we are in a viable kinematic regime i.e. 0.0 < pz < 1.0
				if pz < 0.0 or pz > 1.0:
					# Continue to sample until we get a viable point
					while pz < 0.0 or pz > 1.0:
						px, py, pz = self.model_kinematics()
				pz = pz * E_CM

			# Record the sampled pz and pT	
			pz_COM = pz / E_CM
			pT_COM = np.sqrt(px**2 + py**2)
			
			# Create a vector to hold the boosted value of pz
			w = Vec4(px, py, pz, np.sqrt(self.pd[new_had_id].mass**2 +(px)**2 + (py)**2 + (pz)**2))
			w.rot(0.0, -phi)
			w.rot(theta, phi)
			w.bst(p_sum)
	
			# v1 needs to be in the lab frame
			v1.rot(0.0, -phi)
			v1.rot(theta, phi)
			v1.bst(p_sum)
			# Set pz equal to the boosted back value
			pz = w.pz()
	
			# New hadron and string four momenta
			s = Vec4(-px, -py, v1.pz()-pz, np.sqrt(px**2 + py**2 + (v1.pz()-pz)**2))
			h = Vec4(v1.px()+px, v1.py()+py, pz, np.sqrt(self.pd[new_had_id].mass**2 +(v1.px()+px)**2 + (v1.py()+py)**2 + (pz)**2))
			
			# Return numpy arrays
			s = np.array([s.px(), s.py(), s.pz(), s.e()])
			h = np.array([h.px(), h.py(), h.pz(), h.e()])
			
		else: 
			# Else return zero values to signal termination in the fragmentation chain
			new_string_id = 0
			new_had_id = 0
			s = [0,0,0,0]
			h = [0,0,0,0]
			E_CM = 0.0
			weight = 0
			pz_COM = 0 
			pT_COM = 0
		
		if weighted:
			return new_string_id, s, new_had_id, h, E_CM, weight, pz_COM, pT_COM
		else:
			return new_string_id, s, new_had_id, h, E_CM, pz_COM, pT_COM

	def fragmentation_chain(self, E_partons, E_threshold = 5.0, endA_id = 2, endB_id = -2, mode = 1, print_details = False, weighted = False, weight_switch = 0):
		"""
		Generate a chain of fragmentation events using SWAE kinematics.
	
		E_partons (float): ---- Initial parton energy in GeV (easily modified to accomodate two different parton energies)
		E_threshold (float): -- IR center of mass energy cutoff for the fragmentation process
		endA_pid (int): ------- Initial string flavor for endA
		endB_pid (int): ------- Initial string flavor for endB
		mode (int): ----------- Selects the type of event to generate, mode = 1:  q qbar.
		weighted (bool): ------ Return weights associated with second model of hadronization (assuming conditional model trained on two labels)
		weight_switch (0 or 1): Determines which of the two trained conditions will be used to perform hadronization.
		print_details (bool): - When set to True the details of the entirte fragmentation chain will be output to the console
	
		Returns lists of hadron names, 4-momenta, pids, and (optionally) weights
		"""
		# Initialize momentum, pid, name log lists
		endA_p_list, endB_p_list, endC_p_list, had_p_list = [],[],[],[]
		endA_pid_list, endB_pid_list, endC_pid_list, had_pid_list = [],[],[],[]
		endA_name_list, endB_name_list, endC_name_list, had_name_list = [],[],[],[]
		COM = []
		if weighted:
			weight_list = []
			weight = 0
		E_CM_check = [E_partons]
	
		# Initialize kinematics
		if mode == 1:
			endA_m = 0.#pd.pdb.m0(endA_id)
			endB_m = 0.#pd.pdb.m0(endB_id)
			endA_p = np.array([0, 0, np.sqrt(E_partons*E_partons - endA_m*endA_m), E_partons])
			endB_p = np.array([0, 0, -np.sqrt(E_partons*E_partons - endB_m*endB_m), E_partons])
	
			# Record initial system 
			endA_p_list.append(endA_p)
			endB_p_list.append(endB_p)
			endA_pid_list.append(endA_id)
			endB_pid_list.append(endB_id)
			endA_name_list.append(self.pd[endA_id].name)
			endB_name_list.append(self.pd[endB_id].name)
	
			endA_p_0 = [endA_p[0], endA_p[1], endA_p[2], endA_p[3]]
			endB_p_0 = [endB_p[0], endB_p[1], endB_p[2], endB_p[3]]
			
			#sum_had_E = 0
			endA_term, endB_term = 0,0
			term_lim = 2
			had_sum_E_A, had_sum_E_B = 0,0

			pz_COM = 0
			pT_COM = 0
			
			# Generate the chain
			while endA_term < term_lim or endB_term < term_lim:
				endA_p_new = -np.ones(4)
				endB_p_new = np.ones(4)
				# Choose endA (>0.5) or endB (<= 0.5)
				r =  np.random.uniform()
				# If less than 0.5 choose endA
				if r > 0.5:
					if endA_term < term_lim:
						# Generate the fragmentation for endA
						#while endA_p_new[2] < 0:
						if weighted:
							endA_id_new, endA_p_new, had_id_new, had_p_new, CM_A_E_check, weight, pz_COM, pT_COM = self.fragmentation(endA_p, endB_p, endA_id, endB_id, E_threshold, 'endA', E_partons, weighted = weighted, weight_switch = weight_switch)
						else:
							endA_id_new, endA_p_new, had_id_new, had_p_new, CM_A_E_check, pz_COM, pT_COM = self.fragmentation(endA_p, endB_p, endA_id, endB_id, E_threshold, 'endA', E_partons)
						# Check if the fragmentation should be logged
						if endA_p_new[3] > 0 and CM_A_E_check > E_threshold:
							# endA should always have positive pz
							#if endA_p_new[2] < 0:
								#print("String end A should only have positive p_z --- resampling and trying again.")
							endA_id = endA_id_new 
							endA_p  = endA_p_new 
							had_id  = had_id_new 
							had_p   = had_p_new
							# Update momenta, pids
							endA_p_list.append(endA_p.tolist())
							endB_p_list.append(endB_p.tolist())
							endC_p_list.append(endA_p.tolist())
							had_p_list.append(had_p.tolist())
							endA_pid_list.append(endA_id)
							endB_pid_list.append(endB_id)
							endC_pid_list.append(endA_id)
							had_pid_list.append(had_id)
							endA_name_list.append(self.pd[endA_id].name)
							endB_name_list.append(self.pd[endB_id].name)
							endC_name_list.append(self.pd[endA_id].name)
							had_name_list.append(self.pd[had_id].name)
							E_CM_check.append(CM_A_E_check)
							had_sum_E_A += had_p[3]
							if weighted:
								weight_list.append(weight)
							COM.append(np.array([pz_COM, np.sqrt(pT_COM**2 + self.pd[had_id_new].mass**2)]))
						else:
							# Signal endA termination
							endA_term +=1
							
				# else choose endB           
				else:
					if endB_term < term_lim:
						#while endB_p_new[2] > 0:
						if weighted:
							endB_id_new, endB_p_new, had_id_new, had_p_new, CM_B_E_check, weight, pz_COM, pT_COM = self.fragmentation(endB_p, endA_p, endB_id, endA_id, E_threshold, 'endB', E_partons, weighted = weighted, weight_switch = weight_switch)
						else:
							endB_id_new, endB_p_new, had_id_new, had_p_new, CM_B_E_check, pz_COM, pT_COM = self.fragmentation(endB_p, endA_p, endB_id, endA_id, E_threshold, 'endB', E_partons)
						if endB_p_new[3] > 0 and CM_B_E_check > E_threshold:
							# endB should always have negative pz
							#if endB_p_new[2] > 0:
								#print("String end B should only have negative p_z --- resampling and trying again.")

							endB_id = endB_id_new 
							endB_p  = endB_p_new 
							had_id  = had_id_new 
							had_p   = had_p_new
							# Update momenta, pids
							endA_p_list.append(endA_p.tolist())
							endB_p_list.append(endB_p.tolist())
							endC_p_list.append(endB_p.tolist())
							had_p_list.append(had_p.tolist())
							endA_pid_list.append(endA_id)
							endB_pid_list.append(endB_id)
							endC_pid_list.append(endB_id)
							had_pid_list.append(had_id)
							endA_name_list.append(self.pd[endA_id].name)
							endB_name_list.append(self.pd[endB_id].name)
							endC_name_list.append(self.pd[endB_id].name)
							had_name_list.append(self.pd[had_id].name)
							E_CM_check.append(CM_B_E_check)
							had_sum_E_B += had_p[3]
							if weighted:
								weight_list.append(weight)
							COM.append(np.array([pz_COM, np.sqrt(pT_COM**2 + self.pd[had_id_new].mass**2)]))
						else:
							# Signal endB termination
							endB_term += 1
		# Print the deatils of the entire chain
		if print_details == True:
			# Print the chain results
			print('endA:')
			for i in range(len(endA_p_list)):
	   			print('Event no: ', i, ', ', 'pid: ', endA_pid_list[i], '(', endA_name_list[i],'), ', 'px: ', '{:.3f}'.format(endA_p_list[i][0]), ' py: ', '{:.3f}'.format(endA_p_list[i][1]), ' pz: ', "{:.3f}".format(endA_p_list[i][2]), ' E: ', '{:.3f}'.format(endA_p_list[i][3]))
	   
			print()
			print('endB:')
			for i in range(len(endB_p_list)):
	   			print('Event no: ', i, ', ', 'pid: ', endB_pid_list[i], '(', endB_name_list[i],'), ', 'px: ', '{:.3f}'.format(endB_p_list[i][0]), ' py: ', '{:.3f}'.format(endB_p_list[i][1]), ' pz: ', "{:.3f}".format(endB_p_list[i][2]), ' E: ', '{:.3f}'.format(endB_p_list[i][3]))
		
			print()
			print('endC:')
			for i in range(len(endC_p_list)):
	   			print('Event no: ', i+1, ', ', 'pid: ', endC_pid_list[i], '(', endC_name_list[i],'), ', 'px: ', '{:.3f}'.format(endC_p_list[i][0]), ' py: ', '{:.3f}'.format(endC_p_list[i][1]), ' pz: ', "{:.3f}".format(endC_p_list[i][2]), ' E: ', '{:.3f}'.format(endC_p_list[i][3]))
	
			print()
			print('hadron:')
			for i in range(len(had_p_list)):
	   			print('Event no: ', i+1, ', ', 'pid: ', had_pid_list[i], '(', had_name_list[i],'), ', 'px: ', '{:.3f}'.format(had_p_list[i][0]), ' py: ', '{:.3f}'.format(had_p_list[i][1]), ' pz: ', "{:.3f}".format(had_p_list[i][2]), ' E: ', '{:.3f}'.format(had_p_list[i][3]))

			if weighted:
				print()
				print('Event weights:', weight_list)

			print()
			print('COM energies:', E_CM_check)
			# Quick check for energy-momentum conservation 
			#sum_px, sum_py, sum_pz, sum_E = 0,0,0,0
			#for i in range(len(had_p_list)):
			#    sum_px+= had_p_list[i][0]
			#    sum_py+= had_p_list[i][1]
			#    sum_pz+= had_p_list[i][2]
			#    sum_E+= had_p_list[i][3]
			#print()
			#print('hadron sum: ', 'px: ', sum_px, 'py: ', sum_py, 'pz: ', sum_pz, 'E: ', sum_E)
		if weighted:
			return had_name_list, had_p_list, had_pid_list, weight_list, COM
		else:
			return had_name_list, had_p_list, had_pid_list, endC_p_list, COM

if __name__ == "__main__":

	# Generate the hadronization chain for partons with initial energy 50 GeV
	model_PATH = r'models/cBINN_pz_pT_mT'
	hadronization = Hadronization(model_PATH, conditional = False)
	hadronization.fragmentation_chain(500.0, E_threshold = 25., print_details=True)
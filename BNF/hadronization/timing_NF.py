from hadronization import Hadronization
import time

model_PATH = r'models/cBINN_pz_pT_mT'
hadronization = Hadronization(model_PATH, conditional = True)

# Generate 1000 events
N_events = 1000

print("Starting NF test.")

# Start the timer
start_cSWAE = time.time()
for _ in range(N_events):
	hadronization.fragmentation_chain(E_partons = 50.0, E_threshold = 5.0, print_details=False)

# Output the generation time
end_cSWAE = time.time()
print('NF generated', N_events, 'events in', end_cSWAE - start_cSWAE, 's.') # ~96 seconds
"""
# train_2D_flow.py is a part of the MLHAD package.
# Copyright (C) 2023 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import numpy as np
from tqdm import tqdm

import torch
from torch import Tensor
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from sklearn.model_selection import train_test_split

from model_2D_flow import BINN

import yaml

def train(dataloader, model, optimizer, n_dim, conditional, scheduler=None):
    """
    Training loop for BINN and cBINN
    """
    size = len(dataloader.dataset)
    model.train()
    if conditional:
        for batch, (x,l) in enumerate(dataloader):
            # Pass through the network
            z, log_jac_det = model.inn(x, c=[l])
            # Initialize loss components
            loss_inn, loss_KL, loss = 0.0, 0.0, 0.0
            # Compute the KL loss for each of the Bayesian layers in the network
            for i in range(len(model.inn.module_list)):
                for j in range(len(model.inn.module_list[i].subnet)):
                    if str(model.inn.module_list[i].subnet[j]) == 'Linear_VB()':
                        loss_KL += model.inn.module_list[i].subnet[j].KL()
            # Normalize loss to the training dataset size
            loss_KL = loss_KL / size
            # Get the batch loss
            loss_inn = model.inn_loss(z, n_dim, log_jac_det)
            # Compute the total loss
            loss = loss_KL + loss_inn
            # Reset the gradients in the optimizer
            optimizer.zero_grad()
            # Compute gradients
            loss.backward()
            # Update the network weights
            optimizer.step()
            # Update the scheduler
            if scheduler != None:
                scheduler.step(loss)
            # Print the batch info every 100 updates
            if batch % 100 == 0:
                print(f'Total loss: {loss:>8f} INN loss: {loss_inn:>8f} KL loss: {loss_KL:>8f} learning_rate: {optimizer.param_groups[0]["lr"]:>8f}')
    else: 
        for batch, x in enumerate(dataloader):
            # Pass through the network
            z, log_jac_det = model(x)
            # Initialize loss components
            loss_inn, loss_KL, loss = 0.0, 0.0, 0.0
            # Compute the KL loss for each of the Bayesian layers in the network
            for i in range(len(model.module_list)):
                for j in range(len(model.module_list[i].subnet.subnet_bayes_layers)):
                    loss_KL += model.module_list[i].subnet.subnet_bayes_layers[j].KL()
            loss_KL = loss_KL / size
            # Get the batch loss
            loss_inn = inn_loss(z, ndim, log_jac_det)
            # Compute the total loss
            loss = loss_KL + loss_inn
            # Reset the gradients in the optimizer
            optimizer.zero_grad()
            # Compute gradients
            loss.backward()
            # Update the network weights
            optimizer.step()
            # Update the scheduler
            if scheduler != None:
                scheduler.step(loss)
            # Print the batch info every 100 updates
            if batch % 100 == 0:
                print(f'Total loss: {loss:>8f} INN loss: {loss_inn:>8f} KL loss: {loss_KL:>8f} learning_rate: {optimizer.param_groups[0]["lr"]:>8f}')

class first_emission_labeled_dataset(Dataset):
    """
    Converts labeled kinematic dataset into PyTorch syntax.
    """

    def __init__(self, labels, data):
        self.data = data
        self.labels = labels

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        sample = [self.data[idx], self.labels[idx]]
        return sample

class first_emission_dataset(Dataset):
    """
    Converts kinematic dataset into PyTorch syntax.
    """

    def __init__(self, data):
        self.data = data

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, idx):
        sample = self.data[idx]
        return sample

if __name__ == '__main__':
    # Example of conditional training (1 condition)

    # Load the conditioned data
    pz_pT_labels = np.load('../../data/pz_pT_mT.npy')
    labels = np.load('../../data/labels_pz_pT_mT.npy')
    # Reshape pz_pT to match labels
    pz_pT_labels = np.reshape(pz_pT_labels, (pz_pT_labels.shape[0], pz_pT_labels.shape[1]))

    # Split data into train, validation
    labels_train, labels_val, pz_pT_labels_train, pz_pT_labels_val = train_test_split(labels, pz_pT_labels, test_size=0.2)
    print("Training set size: ", pz_pT_labels_train.shape, "Validation set size: ", pz_pT_labels_val.shape)
    
    # Convert into torch objects
    labels_train = torch.Tensor(labels_train)
    labels_val   = torch.Tensor(labels_val)
    pz_pT_labels_train = torch.Tensor(pz_pT_labels_train)
    pz_pT_labels_val   = torch.Tensor(pz_pT_labels_val)
    
    # Prepare data for DataLoader
    trainset = first_emission_labeled_dataset(labels_train, pz_pT_labels_train)
    valset   = first_emission_labeled_dataset(labels_val, pz_pT_labels_val)
    
    # Select batch size
    batch_size = 3000
    
    # Initialize data-loaders
    train_dataloader = DataLoader(trainset, batch_size = batch_size, shuffle = True)
    val_dataloader = DataLoader(valset, batch_size = batch_size, shuffle = True)
    
    # Set hyperparameters

    # Training hyperparameters
    epochs = 10
    learning_rate = 0.001
    training_size = len(pz_pT_labels_train)
    
    # Network hyperparameters
    n_dim       = 2
    input_dim   = 1
    hidden_dim  = 32 
    output_dim  = 1
    n_blocks    = 8
    resample    = True
    conditional = True 
    cond_shape  = (1,)

    # Useful info for event generation post-training, will be saved to model dictionary
    conditions_min_max = {'m': [0.13957, 1.232]}
    training_labels  = {'m':[0.13957, 0.49368, 0.54786, 0.77526, 0.89594, 0.93827, 1.18937, 1.232], 'E': [5., 16.25, 27.5, 38.75, 50.]}
    
    # Define the model
    BINN = BINN(n_dim = n_dim, training_size = training_size, input_dim = input_dim, hidden_dim = hidden_dim, 
                output_dim = output_dim, n_blocks = n_blocks, resample = resample, conditional = conditional, 
                cond_shape = cond_shape)
    
    # Print out the model architecture
    output_model_architecture = False
    if output_model_architecture:
        print("Model Architecture: ")
        print(BINN.inn)
    
    # Set the optimizer
    optimizer = torch.optim.Adam(BINN.inn.parameters(), lr=learning_rate)
    
    # Set learning rate scheduler
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode= 'min', factor = 0.9, patience = 250)
    #scheduler = None
    
    # Train
    for t in tqdm(range(epochs), ncols = 100):
        #if t == 0: 
        print()
        train(dataloader = train_dataloader, model = BINN, optimizer = optimizer, n_dim = n_dim, conditional = conditional, scheduler = scheduler)
         
    print("Training complete!")

    # Save the model
    PATH_model = 'saved_models/cBINN_pz_pT_mT_example.pth'
    torch.save(BINN.inn.state_dict(), PATH_model)
    print('Model file as been saved to', PATH_model)

    # Generate model yaml file for easy loading

    # Initialize training dictionary 
    model_dict = {'epochs': epochs, 'training_size': training_size, 'n_dim': n_dim, 'input_dim': input_dim, 'hidden_dim': hidden_dim,
                  'output_dim': output_dim, 'resample': resample, 'conditional': conditional, 'cond_shape': cond_shape, 
                  'conditions_min_max': conditions_min_max, 'n_blocks': n_blocks}

    # Output the summary file to be used when loading the model
    PATH_model_summary = 'saved_models/cBINN_pz_pT_mT_example_summary.yaml'
    with open(PATH_model_summary, 'w') as f:
        yaml.dump(model_dict, f)
    print('Model summary saved to', PATH_model_summary)
"""
# model_2D_flow.py is a part of the MLHAD package.
# Copyright (C) 2023 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import os
import sys
import numpy as np

import FrEIA.framework as Ff
import FrEIA.modules as Fm

import torch
from torch import nn
from torch import Tensor
from torch.nn.parameter import Parameter, UninitializedParameter
from torch.nn import functional as F
from torch.nn import init
from torch.nn import Module

class Linear_VB(Module):
    """
    Build a variational Bayesian linear weight layer i.e. weights are now sampled from a distribution
    """
    __constants__ = ['in_features', 'out_features']
    in_features: int
    out_features: int
    weight: Tensor
    
    def __init__(self, in_features, out_features, resample = True):
        """
        Initialize a Linear Variational Bayesian layer to be used within the multi-layer-perceptrons
        of the 2D Bayesian Normalizing Flow (BNF). 
        """
        super(Linear_VB, self).__init__()
        self.in_features = in_features 
        self.out_features = out_features
        
        # Sample random weights through each forward pass
        self.resample = resample
        # Assign as tunable parameter with dimension (1 x out_features)
        self.bias = Parameter(Tensor(self.out_features))
        # Assign as tunable parameter with dimension (out_features x in_features)
        self.mu_w = Parameter(Tensor(self.out_features, self.in_features))
        # Assign as tunable parameter with dimension (out_features x in_features)
        self.logsig2_w = Parameter(Tensor(self.out_features, self.in_features))
        
        self.random = torch.randn_like(self.logsig2_w)
        self.reset_parameters()
        
    def forward(self, input):
        # Sample random weights on each forward pass
        if self.resample:
            self.random = torch.randn_like(self.logsig2_w)
        s2_w = self.logsig2_w.exp()
        
        # Reparameterization trick
        weight = self.mu_w + s2_w.sqrt() * self.random
        return nn.functional.linear(input, weight, self.bias)
    
    def reset_parameters(self):
        stdv = 1. / np.sqrt(self.mu_w.size(1))
        self.mu_w.data.normal_(0, stdv)
        self.logsig2_w.data.zero_().normal_(-9, 0.001)
        self.bias.data.zero_()

    def deterministic(self):
        """
        Collapse the network weights to their mean value
        """
        self.resample = False
        self.weight = self.mu_w
        
    def KL(self, loguniform=False):
        """ 
        Compute the KL divergence 
        """
        KL = 0.5 * (self.mu_w.pow(2) + self.logsig2_w.exp() - self.logsig2_w - 1).sum()
        return KL


class BINN(Module):
	def __init__(self, n_dim, training_size, input_dim, hidden_dim, output_dim, n_blocks = 8, resample = True, conditional = True, cond_shape = (2,)):
		"""
		This class defines the 2D Bayesian Invertible Neural Network (BINN) a.k.a. Bayesian Normalizing Flow (BNF) with and without conditioning.
		"""
		super(BINN, self).__init__()

		# Dimension of the training distribution
		self.n_dim = n_dim
		# Size of the training dataset
		self.training_size = training_size
		# Number of subnetworks to append to append to mother subnetwork
		self.n_blocks = n_blocks
		# Use Bayesian Linear layer or deterministic Linear layer
		self.resample = resample
		# Labeled or unlabeled data
		self.conditional = conditional 
		# Size of the condition label
		self.cond_shape = cond_shape
		
		# Loss function is dependent on the size of the training dataset
		self.training_size = training_size
		
		def subnet(input_dim, output_dim):
			"""
			This function defines the subnetwork blocks that will build the sequential network.

			Each subnetwork consists of a multi-layer perceptron consisting of Bayesian 
			linear layers and ReLU activation functions. 

			returns a sequential 
			"""
			self.subnet_layers = []
			# Define the input layer
			linear_VB_layer_input  = Linear_VB(input_dim, hidden_dim, resample = self.resample)
			linear_VB_layer_hidden = Linear_VB(hidden_dim, hidden_dim, resample = self.resample)
			linear_VB_layer_output = Linear_VB(hidden_dim, output_dim, resample = self.resample)

			# Input layer
			self.subnet_layers.append(linear_VB_layer_input)
			self.subnet_layers.append(nn.ReLU())
	
			# Hidden layers
			self.subnet_layers.append(linear_VB_layer_hidden)
			self.subnet_layers.append(nn.ReLU())
			self.subnet_layers.append(linear_VB_layer_hidden)
			self.subnet_layers.append(nn.ReLU())
			
			# Output layer
			self.subnet_layers.append(linear_VB_layer_output)
	
			# Return a sequentiual net over the defined layers above
			return nn.Sequential(*self.subnet_layers)

		# Initialize the sequence network
		self.inn = Ff.SequenceINN(self.n_dim)

		# Generate the network blocks
		if self.conditional:
			for k in range(self.n_blocks):
				self.inn.append(Fm.AllInOneBlock, cond = 0, cond_shape = self.cond_shape, subnet_constructor = subnet, permute_soft = True)
		else: 
			for k in range(self.n_blocks):
				self.inn.append(Fm.AllInOneBlock, subnet_constructor = subnet, permute_soft = True)

	def inn_loss(self, z, ndim, log_jac_det):
		""" 
		Calculate the negative log-likelihood of the model with a standard normal prior 
		"""
		l = 0.5 * torch.sum( z**2, 1 ) - log_jac_det
		l = l.mean() / ndim
		return l

	def collapse_wave_function(self):
		"""
		Collapse network weights to dirac delta functions at the Gaussian mean of the distribution
		"""
		for i in range(len(self.inn.module_list)):
				for j in range(len(self.inn.module_list[i].subnet)):
					if str(self.inn.module_list[i].subnet[j]) == 'Linear_VB()':
						self.inn.module_list[i].subnet[j].deterministic()
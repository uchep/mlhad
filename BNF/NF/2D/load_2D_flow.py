"""
# load_2D_flow.py is a part of the MLHAD package.
# Copyright (C) 2023 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

from model_2D_flow import Linear_VB
from model import BINN
import yaml
import torch

def load_cBNF(model_path, print_details = False, deterministic = True):
	"""
	Function to load model file from yaml file contianing model parameters

	Returns model and dictionary with upper and lower bounds of each condition
	"""
	model_summary_PATH = model_path + '.yaml'
	
	with open(model_summary_PATH) as f:
	    model_dict = yaml.load(f, Loader=yaml.FullLoader)
	
	if print_details:
		print(model_dict)

	# Set network hyperparameters
	training_size = model_dict['training_size']
	n_dim      = model_dict['n_dim']
	input_dim  = model_dict['input_dim']
	hidden_dim = model_dict['hidden_dim']
	output_dim = model_dict['output_dim']
	n_blocks   = model_dict['n_blocks']
	resample   = model_dict['resample']
	conditional = model_dict['conditional']
	cond_shape = model_dict['cond_shape']
	conditions_min_max = model_dict['conditions_min_max']
	
	# Define the model
	cBINN = BINN(n_dim = n_dim, training_size = training_size, input_dim = input_dim, hidden_dim = hidden_dim, 
	            output_dim = output_dim, n_blocks = n_blocks, resample = resample, conditional = conditional, 
	            cond_shape = cond_shape)

	#if deterministic:
		#cBINN.collapse_wave_function()
	model = cBINN.inn

	# Load in the state dictionary (layer weights, etc.)
	model_PATH = model_path + '.pth'
	model.load_state_dict(torch.load(model_PATH))
	cBINN.resample = False
	
	if deterministic:
		cBINN.collapse_wave_function()
	#model = cBINN.inn

	model.eval()

	return model, conditions_min_max
	

def load_BNF(model_path, print_details = False, deterministic = True):
	model_summary_PATH = model_path + '.yaml'
	
	with open(model_summary_PATH) as f:
	    model_dict = yaml.load(f, Loader=yaml.FullLoader)
	
	if print_details:
		print(model_dict)

	# Set network hyperparameters
	training_size = model_dict['training_size']
	n_dim      = model_dict['n_dim']
	input_dim  = model_dict['input_dim']
	hidden_dim = model_dict['hidden_dim']
	output_dim = model_dict['output_dim']
	n_blocks   = model_dict['n_blocks']
	resample   = model_dict['resample']
	conditional = model_dict['conditional']
	cond_shape = model_dict['cond_shape']
	conditions_min_max = model_dict['conditions_min_max']
	
	# Define the model
	pBINN = BINN(n_dim = n_dim, training_size = training_size, input_dim = input_dim, hidden_dim = hidden_dim, 
	            output_dim = output_dim, n_blocks = n_blocks, resample = resample, conditional = conditional, 
	            cond_shape = cond_shape)
	
	if deterministic:
		pBINN.collapse_wave_function()
	model = pBINN.inn

	# Load in the state dictionary (layer weights, etc.)
	model_PATH = model_path + '.pth'
	model.load_state_dict(torch.load(model_PATH))
	pBINN.resample = False
	
	if deterministic:
		pBINN.collapse_wave_function()
	#model = pBINN.inn

	model.eval()
	return model
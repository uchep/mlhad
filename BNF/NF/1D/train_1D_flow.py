"""
# train_1D_flow.py is a part of the MLHAD package.
# Copyright (C) 2023 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

from model_1D_flow import *

from torch.utils.data import Dataset
from torch.utils.data import DataLoader

import yaml

import numpy as np
from tqdm import tqdm

def loss_function(target_distribution, z, log_dz_dx):
    log_likelihood = target_distribution.log_prob(z) + log_dz_dx
    return -log_likelihood.mean()

def train(model, dataloader, optimizer, target_distribution, scheduler):
    model.train()
    for batch, x in enumerate(dataloader):
        z, log_dz_dx = model(x)
        loss = loss_function(target_distribution, z, log_dz_dx)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        if scheduler != None:
            scheduler.step(loss)
        if batch % 1000 == 0:
            print(f'Total loss: {loss:>8f} learning_rate: {optimizer.param_groups[0]["lr"]:>8f}')

class first_emission_dataset(Dataset):
    """
    Creates pz dataset so that it can be used by the PyTorch syntax.
    """

    def __init__(self, data):
        self.data = data

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, idx):
        sample = self.data[idx]
        return sample

# Load training data
pz_pT = np.load('../../data/pz_pT_mT_a_0.68.npy')

# Extract pz
pz = torch.tensor(pz_pT[:,0])

# Set the hyperparameters
batch_size = 1000
epochs = 10
learning_rate = 0.01
n_components = 100
n_flows = 5

# Prepare data for PyTorch syntax
trainset = first_emission_dataset(pz)
train_dataloader = DataLoader(trainset, batch_size = batch_size, shuffle = True)

# Initialize the model
flow = N_Flow1d(n_components=n_components, n_flows=n_flows)
# Set the optimizer and learning rate scheduler
optimizer = torch.optim.Adam(flow.parameters(), lr=learning_rate)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode= 'min', factor = 0.9, patience = 500)

# Choose the target distribution
#target_distribution = torch.distributions.normal.Normal(loc = 0.0, scale = 1.0)
target_distribution = torch.distributions.uniform.Uniform(0.0, 1.0)

# Train!
for epoch in tqdm(range(epochs), ncols = 100):
    print()
    train(flow, train_dataloader, optimizer, target_distribution, scheduler)

# Plot the results
plot_learned_density = False
if plot_learned_density:
    import matplotlib.pyplot as plt
    x = np.linspace(0.,1.,1000)
    target_distribution = torch.distributions.uniform.Uniform(0.0, 1.0)
    with torch.no_grad():
        z, log_dz_dx = flow(torch.FloatTensor(x))
        px = (target_distribution.log_prob(z) + log_dz_dx).exp().cpu().numpy()

    fig, ax = plt.subplots(1,1,figsize = (6,5))
    ax.plot(x, px)
    ax.hist(pz.detach().numpy(), 50, density = True)
    ax.set_xlabel(r'$p_z$')
    ax.set_ylabel(r'$\mathrm{PDF}$')
    fig.tight_layout()
    fig.savefig('saved_models/pz_N_5_n_100_flow_a_0.68_example.pdf', dpi = 300, pad_inches = .1, bbox_inches = 'tight')

# Save the model
PATH_model = 'saved_models/NF_1D_pz_a_0.68_N_5_n_100_example.pth'
torch.save(flow.state_dict(), PATH_model)
print('Model file as been saved to', PATH_model)

# Generate and save model yaml file for easy loading
# Initialize training dictionary 
model_dict = {'epochs': epochs, 'n_components': n_components, 'n_flows': n_flows}

PATH_model_summary = 'saved_models/NF_1D_pz_a_0.68_N_5_n_100_example_summary.yaml'
with open(PATH_model_summary, 'w') as f:
	yaml.dump(model_dict, f)
print('Model summary saved to', PATH_model_summary)
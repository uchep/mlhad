"""
# model_1D_flow.py is a part of the MLHAD package.
# Copyright (C) 2023 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import torch
import torch.nn as nn

class Flow1d(nn.Module):
    def __init__(self, n_components):
        """
        Initialize 1D flow model

        n_components (int): number of Gaussians components in the mixture
        """
        super(Flow1d, self).__init__()
        # Randomly initialize means and variances of Gaussian components
        self.mus = nn.Parameter(torch.randn(n_components), requires_grad=True)
        self.log_sigmas = nn.Parameter(torch.zeros(n_components), requires_grad=True)
        self.weight_logits = nn.Parameter(torch.ones(n_components), requires_grad=True)

    def forward(self, x):
        """
        Define the forward pass of a single block

        x (torch tensor): Input data from batch

        Returns latent variable and the derivative of the transformation
        """
        x = x.view(-1,1)
        # Normalize weights to sum to one
        weights = self.weight_logits.softmax(dim=0).view(1,-1)
        # Define tensor of normal distributions with means and sigma's  
        distribution = torch.distributions.normal.Normal(self.mus, self.log_sigmas.exp())
        # Compute the Guassian mixture sum and it's log derivative
        z = (distribution.cdf(x) * weights).sum(dim=1)
        log_dz_dx = (distribution.log_prob(x).exp() * weights).sum(dim=1).log()
        return z, log_dz_dx
    
class LogitTransform(nn.Module):
    def __init__(self, alpha):
        """
        Initialize the logit transform that will be used as a non-linearity between 1D flows

        alpha (float): tunable parameter cotnrolling shape of logit function
        """
        super(LogitTransform, self).__init__()
        self.alpha = alpha 

    def forward(self, x):
        """
        Define the forward proccess for the logit transform

        x (torch tensor): Input data from batch

        Returns latent variable and the derivative of the transformation
        """
        # Compute logit variable
        x_new = self.alpha/2 + (1-self.alpha)*x
        # Compute the output and it's derivative 
        z = torch.log(x_new) - torch.log(1-x_new)
        log_dz_dx = torch.log(torch.FloatTensor([1-self.alpha])) - torch.log(x_new) - torch.log(1-x_new)
        return z, log_dz_dx
    
class N_Flow1d(nn.Module):
    def __init__(self, n_components, n_flows, alpha = 0.01):
        """
        Initialize N_Flow1d class the connects multiple 1D flow blocks

        n_compontents (int): number of Gaussians components in the mixture for each block

        n_flows (int): number of 1D flow blocks to string together

        alpha (float): tunable parameter cotnrolling shape of logit function
        """
        super(N_Flow1d, self).__init__()
        self.n_components = n_components
        self.n_flows = n_flows
        self.flow = Flow1d(n_components)
        self.alpha = alpha
        self.logit = LogitTransform(alpha = self.alpha)
        self.flow_list = []
        # Construct the full flow list based on the input parameters
        for i in range(n_flows-1):
            self.flow_list.append(self.flow)
            self.flow_list.append(self.logit)
        self.flow_list.append(self.flow)
        self.flow_list = nn.ModuleList(self.flow_list)
        
    def forward(self, x):
        """
        Define the forward proccess for the composite 1D flow

        x (torch tensor): Input data from batch

        Returns latent variable and the derivative of the transformation
        """
        z, log_dz_dx_sum = x, 0
        for flows in self.flow_list:
            z, log_dz_dx = flows(z)
            log_dz_dx_sum += log_dz_dx
        return z, log_dz_dx_sum
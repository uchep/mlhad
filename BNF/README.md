## Towards a data-driven model of hadronization with normalizing flows

This subdirectory contains the open-source code associated with "__Towards a data-driven model of hadronization with normalizing flows__" ([2311.09296](https://arxiv.org/pdf/2311.09296.pdf)). 

The relevant training datasets for each subdirectory may be found at [https://zenodo.org/records/10379697](https://zenodo.org/records/10379697). By default each subdirectory assumes all training datasets are located in the ```data/``` directory. 

### Summary

The repository consists of a hierarchical structure with three major components. The first component, found in the ```NF/``` subdirectory, contains the implementation of the one- and two-dimensional NF network, with and without conditioning. The second component, located in the ```hadronization/``` subdirectory, constitutes the integration of these NFs into fragmentation chains. Finally, the third component, located in the ```MAGIC/``` subdirectory, is the implementation of MAGIC. Detailed explanations and examples of each component can be found within the code documentation and examples. All code is written in ```Python```, developed using ```v3.11```, and heavily utilizes the ```PyTorch```, developed using ```v2.1```, deep learning library. Finally, all training datasets were produced using ```Pythia v8.309```.
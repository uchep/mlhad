"""
# load_1D_flow.py is a part of the MLHAD package.
# Copyright (C) 2023 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

from model_1D_flow import N_Flow1d
import yaml
import torch

def load_NF_N_flows(model_path, print_details = False):
	model_summary_PATH = model_path + '.yaml'
	
	with open(model_summary_PATH) as f:
	    model_dict = yaml.load(f, Loader=yaml.FullLoader)
	
	if print_details:
		print(model_dict)

	# Set network hyperparameters
	n_components = model_dict['n_components']
	n_flows = model_dict['n_flows']
	
	# Define the model
	flow = N_Flow1d(n_components=n_components, n_flows = n_flows)

	# Load in the state dictionary (layer weights, etc.)
	model_PATH = model_path + '.pth'
	flow.load_state_dict(torch.load(model_PATH))

	flow.eval()
	return flow
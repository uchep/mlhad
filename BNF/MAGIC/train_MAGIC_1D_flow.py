"""
# train_MAGIC_1d_flow.py is a part of the MLHAD package.
# Copyright (C) 2023 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

from MAGIC import *

class ObservableDataset(Dataset):
	"""
	Converts observable dataset into PyTorch syntax.
	"""

	def __init__(self, data):
		self.data = data

	def __len__(self):
		return self.data.shape[0]

	def __getitem__(self, idx):
		sample = self.data[idx]
		return sample

# Load the conditioned data
exp_observable   = np.load(r'../data/multiplicity_sim_1.5_N_5e5_1d_N_5_n_500.npy')
model_observable = np.load(r'../data/multiplicity_sim_0.68_N_5e5_1d_N_5_n_500.npy')
model_kinematics = np.load(r'../data/pz_sim_0.68_N_5e5_1d_N_5_n_500.npy')

# Print dataset shapes
print('Experimental observable shape:', exp_observable.shape)
print('Model observable shape:', model_observable.shape)
print('Model kinematics shape:', model_kinematics.shape)

# Convert into torch objects
model_observable = torch.Tensor(model_observable)
model_kinematics = torch.Tensor(model_kinematics)
exp_observable   = torch.Tensor(exp_observable)

# Prepare data for DataLoader
model_observable = ObservableDataset(model_observable)
model_kinematics = ObservableDataset(model_kinematics)
exp_observable   = ObservableDataset(exp_observable)

# Select batch size
batch_size = 5000

# Initialize data-loaders
model_observable_dataloader = DataLoader(model_observable, batch_size = batch_size, shuffle = False)
model_kinematics_dataloader = DataLoader(model_kinematics, batch_size = batch_size, shuffle = False)
exp_observable_dataloader   = DataLoader(exp_observable,   batch_size = batch_size, shuffle = True)

# Set hyperparameters
model = r'models/NF_1D_pz_a_0.68_N_5_n_500'

# Training hyperparameters
epochs = 5
learning_rate = 0.003
# Length of event buffer
dim_event = model_kinematics_dataloader.dataset.data.shape[1]
results_dir = 'fine_tuned_models/'
model_name = 'pz_fine_tuned_1D_N_5_n_500_example'
print_details = True

# Create a training instance
macroscopic_trainer = MAGIC(epochs = epochs, dim_event = dim_event, model = model, model_observable_dataloader = model_observable_dataloader, 
								 model_kinematics_dataloader = model_kinematics_dataloader, exp_observable_dataloader = exp_observable_dataloader, 
								 print_details = print_details, results_dir = results_dir, model_name = model_name)

# Set the optimizer
optimizer = torch.optim.Adam(macroscopic_trainer.model_sim.parameters(), lr=learning_rate)

# Set learning rate scheduler
#scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode= 'min', factor = 0.9, patience = 25)
#scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, 0.1)
scheduler = None

# Train!
macroscopic_trainer.train_MAGIC(optimizer = optimizer, scheduler = scheduler)
	 
print("Training complete!")

"""
# Plot the new distributions
import matplotlib.pyplot as plt
import seaborn as sns

sns.set()
sns.set_style("ticks")
sns.set_context("paper", font_scale = 1.75)
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 25

# Generate the PDFs for both models
target_distribution = torch.distributions.uniform.Uniform(0., 1.)
anchor = np.linspace(0,1, 1000)
with torch.no_grad():
	pz_CDF_init, dz_dx_init = macroscopic_trainer.model_sim_init(torch.FloatTensor(anchor))
	pz_PDF_init = (target_distribution.log_prob(pz_CDF_init) + dz_dx_init.log()).exp().cpu().numpy()
	pz_CDF, dz_dx = macroscopic_trainer.model_sim(torch.FloatTensor(anchor))
	pz_PDF = (target_distribution.log_prob(pz_CDF) + dz_dx.log()).exp().cpu().numpy()

# Plot the PDFs
fig, ax = plt.subplots(1,1,figsize=(6,5))
ax.plot(anchor, pz_PDF_init, label = r'$\mathrm{Base}$')
ax.plot(anchor, pz_PDF, label = r'$\mathrm{Fine-tuned}$')
ax.set_xlabel(r'$p_z$')
ax.set_ylabel(r'$\mathrm{PDF}$')
ax.legend(frameon=False)
fig.savefig(r'pz_base_vs_fine_tuned_1d.pdf', dpi = 300, pad_inches = .1, bbox_inches = 'tight')
"""

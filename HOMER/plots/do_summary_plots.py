import os
os.environ['OMP_NUM_THREADS'] = "24"

import numpy as np
import sys
import scipy.stats as st
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'size'   : 25}
rc('font', **font)
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
rc('text', usetex=True)
plt.rcParams['font.family']='Computer Modern'


from sklearn.model_selection import cross_val_predict, train_test_split, KFold

from sklearn.metrics import accuracy_score, precision_score, roc_auc_score
from sklearn.calibration import calibration_curve


###############################################################################
def bin_assigner(bins,val):

    # This function simply outputs the bin assignment of a given value val for a given choice of binning bins

    number_of_bins = len(bins)
    bin_number=0
    if val >= bins[-1]:
        bin_number = number_of_bins - 1
    if val < bins[0]:
        bin_number = 0
    else:
        for bin_number_aux in range(number_of_bins-1):
            if bins[bin_number_aux]<= val < bins[bin_number_aux+1]:
                bin_number = bin_number_aux
    return bin_number
###############################################################################
def neff(weights):
    return np.sum(weights)**2/(np.sum(weights**2))

###############################################################################
### scientific notation formatter
from decimal import Decimal
def fmt(x):
    base = 10
    sign_string = '-' if x < 0 else ''
    x = abs(x)
    base = 10
    dec = '%.0E' % Decimal(str(x))
    fx = float(dec[-3:])
    a = dec[0]
    if a == 1.0:
        return r'$\mathdefault{%s\times%s^{%.0f}}$' % (sign_string,base, fx)
    else:
        return r'$\mathdefault{%s%s\times%s^{%.0f}}$' % (sign_string,a,base, fx)

fmt_formatter = lambda x, pos: fmt(x)
from matplotlib.ticker import FuncFormatter


###############################################################################

import yaml 

from yaml import load
from yaml import BaseLoader as Loader
verbose=True
yaml_file = sys.argv[1]
print(yaml_file)
stream = open(yaml_file, 'r')
dictionary = yaml.load(stream, Loader)
if verbose:
    for key, value in dictionary.items():
        print (key + " : " + str(value))

#### here let's load all necessary things
results_dir = dictionary['results_dir']

### load simulation splits and hadrons, measured splits and hadrons
Nevents = int(dictionary['Nevents'])
print(Nevents)
X_sim_splits = np.load(dictionary['X_sim_splits'])[:Nevents]
print(X_sim_splits.shape)
X_sim_hadrons = np.load(dictionary['X_sim_hadrons'])[:Nevents]
print(X_sim_hadrons.shape)
X_measured_splits = np.load(dictionary['X_measured_splits'])[:Nevents]
print(X_measured_splits.shape)
X_measured_hadrons = np.load(dictionary['X_measured_hadrons'])[:Nevents]
print(X_measured_hadrons.shape)

# colors for plots
color_sim = dictionary["color_sim"]
if len(color_sim)==3:# this means it's RGB
    color_sim= list(map(float, color_sim))
color_data = dictionary["color_data"]
if len(color_data)==3:# this means it's RGB
    color_data= list(map(float, color_data))
color_first_step = dictionary["color_first_step"]
if len(color_first_step)==3:# this means it's RGB
    color_first_step= list(map(float, color_first_step))
color_second_step = dictionary["color_second_step"]
if len(color_second_step)==3:# this means it's RGB
    color_second_step= list(map(float, color_second_step))
color_new_fragmentation_function = dictionary["color_new_fragmentation_function"]
if len(color_new_fragmentation_function)==3:# this means it's RGB
    color_new_fragmentation_function= list(map(float, color_new_fragmentation_function))
color_from_split = dictionary["color_from_split"]
if len(color_from_split)==3:# this means it's RGB
    color_from_split= list(map(float, color_from_split))
color_optimal = dictionary["color_optimal"]
if len(color_optimal)==3:# this means it's RGB
    color_optimal= list(map(float, color_optimal))

# labels for plots

label_sim = dictionary["label_sim"]
label_data = dictionary["label_data"]
label_first_step = dictionary["label_first_step"]
label_second_step = dictionary["label_second_step"]
label_new_fragmentation_function = dictionary["label_new_fragmentation_function"]
label_from_split = dictionary["label_from_split"]
label_optimal = dictionary["label_optimal"]


# loading different weights per event
weights_per_event_first_step = np.load(dictionary['weights_per_event_first_step'])[:Nevents]
print(neff(weights_per_event_first_step))
weights_per_event_second_step = np.load(dictionary['weights_per_event_second_step'])[:Nevents]
print(neff(weights_per_event_second_step))
weights_per_event_new_fragmentation_function = np.load(dictionary['weights_per_event_new_fragmentation_function'])[:Nevents]
print(neff(weights_per_event_new_fragmentation_function))

try:
    weights_per_event_from_split = np.load(dictionary['weights_per_event_from_split'])[:Nevents]
    has_best_NN = True
    print(neff(weights_per_event_from_split))
except:
    has_best_NN = False
    print("No best NN")

# loading different weights per split
if int(dictionary['stringends'])==1:
    stringends = True
else:
    stringends = False
if stringends == False:
    valid_values_sim = [a and b for a,b in zip(X_sim_splits[:,:,8].flatten()==0.0,X_sim_splits[:,:,12].flatten()!=0.0)]
    valid_values_measured = [a and b for a,b in zip(X_measured_splits[:,:,8].flatten()==0.0,X_measured_splits[:,:,12].flatten()!=0.0)]
else:
    valid_values_sim = [a and b for a,b in zip(X_sim_splits[:,:,8].flatten()>=0.0,X_sim_splits[:,:,12].flatten()!=0.0)]
    valid_values_measured = [a and b for a,b in zip(X_measured_splits[:,:,8].flatten()>=0.0,X_measured_splits[:,:,12].flatten()!=0.0)]

weights_per_split_new_fragmentation_function = np.load(dictionary['weights_per_split_new_fragmentation_function'])[:int(np.sum(valid_values_sim))]

if has_best_NN:
    weights_per_split_from_split = np.load(dictionary['weights_per_split_from_split'])[:int(np.sum(valid_values_sim))]


## loading log w computed using variations or analytical_log_weight, don't care too much
sim_optimal_observable = 2*np.load(dictionary['sim_optimal_observable'])[:Nevents]
print(neff(np.exp(0.5*sim_optimal_observable)))
measured_optimal_observable = 2*np.load(dictionary['measured_optimal_observable'])[:Nevents]

# load log w computed from first step or smearing of optimal step, whatever
sim_measured_observable = 2*np.log(weights_per_event_first_step)#np.load(dictionary['sim_measured_observable'])[:Nevents]
measured_measured_observable = 2*np.log(np.load(dictionary['weights_per_event_first_step_measured'])[:Nevents])

#### Weight plots
a,b,c = plt.hist(np.exp(0.5*sim_optimal_observable),histtype='step',color=color_optimal,bins=50,label=label_optimal)
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color=color_optimal)
a,b,c = plt.hist(weights_per_event_first_step,histtype='step',color=color_first_step,bins=b,label=label_first_step)
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color=color_first_step)
a,b,c = plt.hist(weights_per_event_second_step,histtype='step',color=color_second_step,bins=b,label=label_second_step)
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color=color_second_step)
a,b,c = plt.hist(weights_per_event_new_fragmentation_function,histtype='step',color=color_new_fragmentation_function,bins=b,label=label_new_fragmentation_function)
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color=color_new_fragmentation_function)
if has_best_NN:
    a,b,c = plt.hist(weights_per_event_from_split,histtype='step',color=color_from_split,bins=b,label=label_from_split)
    plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color=color_from_split)

plt.yscale('log')
plt.xlabel('Weight')
plt.ylabel('Events')
plt.axvline(1.0,linestyle='dashed')
plt.legend(loc='best',fontsize=12,framealpha=0.1)
plt.tight_layout()
plt.xscale('log')
plt.savefig(results_dir+'event_weights.pdf')
plt.clf()

print("Pearson r")
print(str(np.round(st.pearsonr(weights_per_event_first_step,weights_per_event_second_step),4)[0]))
plt.hist2d(weights_per_event_first_step,weights_per_event_second_step,bins=100,cmap='plasma')
cbar = plt.colorbar(format=FuncFormatter(fmt_formatter), ticks=[5e1, 5e4, 1e5])
#plt.colorbar(heatmap)
#cbar.ax.set_yticklabels(['0','1','2','>3'])
for t in cbar.ax.get_yticklabels():
     t.set_fontsize(15)
#cbar.set_label('Events', rotation=270,fontsize=15)
cbar.ax.set_title('Events', rotation=0,fontsize=15)
#plt.plot(np.linspace(np.min(weights_per_event_first_step),np.max(weights_per_event_first_step),10),np.linspace(np.min(weights_per_event_first_step),np.max(weights_per_event_first_step),10),'k',label='Perfect agreement')
plt.xscale('log')
plt.yscale('log')
plt.xlabel(label_first_step)
plt.ylabel(label_second_step)
plt.title('r = '+str(np.round(st.pearsonr(weights_per_event_first_step,weights_per_event_second_step),4)[0]))
#plt.legend(loc='upper left')
plt.tight_layout()
plt.savefig(results_dir+'step1_vs_step2.pdf')
plt.clf()

if has_best_NN:
    print("Pearson r")
    print(str(np.round(st.pearsonr(weights_per_event_from_split,weights_per_event_new_fragmentation_function),4)[0]))
    plt.hist2d(weights_per_event_from_split,weights_per_event_new_fragmentation_function,bins=100,cmap='plasma')
    cbar = plt.colorbar(format=FuncFormatter(fmt_formatter), ticks=[1e0, 5e4, 1e5])
    #plt.colorbar(heatmap)
    #cbar.ax.set_yticklabels(['0','1','2','>3'])
    for t in cbar.ax.get_yticklabels():
        t.set_fontsize(15)
    #cbar.set_label('Events', rotation=270,fontsize=15)
    cbar.ax.set_title('Events', rotation=0,fontsize=15)
    #plt.plot(np.linspace(np.min(weights_per_event_from_split),np.max(weights_per_event_from_split),10),np.linspace(np.min(weights_per_event_from_split),np.max(weights_per_event_from_split),10),'k',label='Perfect agreement')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(label_from_split)
    plt.ylabel(label_new_fragmentation_function)
    plt.title('r = '+str(np.round(st.pearsonr(weights_per_event_first_step,weights_per_event_new_fragmentation_function),4)[0]))
    #plt.legend(loc='upper left')
    plt.tight_layout()
    plt.savefig(results_dir+'best_NN_vs_NN_step_2.pdf')
    plt.clf()

    print("Pearson r")
    print(str(np.round(st.pearsonr(np.exp(0.5*sim_optimal_observable),weights_per_event_new_fragmentation_function),4)[0]))
    plt.hist2d(np.exp(0.5*sim_optimal_observable),weights_per_event_new_fragmentation_function,bins=100,cmap='plasma')
    cbar = plt.colorbar(format=FuncFormatter(fmt_formatter), ticks=[1e0, 5e4, 1e5])
    #plt.colorbar(heatmap)
    #cbar.ax.set_yticklabels(['0','1','2','>3'])
    #cbar.set_label('Events', rotation=270,fontsize=15)
    for t in cbar.ax.get_yticklabels():
        t.set_fontsize(15)
    #cbar.set_label('Events', rotation=270,fontsize=15)
    cbar.ax.set_title('Events', rotation=0,fontsize=15)
    #plt.plot(np.linspace(np.min(np.exp(0.5*sim_optimal_observable)),np.max(np.exp(0.5*sim_optimal_observable)),10),np.linspace(np.min(np.exp(0.5*sim_optimal_observable)),np.max(np.exp(0.5*sim_optimal_observable)),10),'k',label='Perfect agreement')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(label_optimal)
    plt.ylabel(label_new_fragmentation_function)
    plt.title('r = '+str(np.round(st.pearsonr(np.exp(0.5*sim_optimal_observable),weights_per_event_new_fragmentation_function),4)[0]))
    #plt.legend(loc='upper left')
    plt.tight_layout()
    plt.savefig(results_dir+'exact_vs_NN_step_2.pdf')
    plt.clf()

plt.close('all')

### do high-level observable comparison
obs_names=['1-T','C','D',r'$B_{W}$',r'$B_{T}$',r'$n_{f}$',r'$n_{\mathrm{ch}}$',r'$\langle |\ln x_f|\rangle$',r'$\langle (|\ln x_f|-\langle |\ln x_f|\rangle)^2\rangle$',r'$\langle (|\ln x_f|-\langle |\ln x_f|\rangle)^3\rangle$',r'$\langle |\ln x_{\rm ch}|\rangle$',r'$\langle (|\ln x_{\rm ch}|-\langle |\ln x_{\rm ch}|\rangle)^2\rangle$',r'$\langle (|\ln x_{\rm ch}|-\langle |\ln x_{\rm ch}|\rangle)^3\rangle$']

density = True
for _ in range(X_sim_hadrons.shape[1]):
    fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
    ax2.set_ylabel('Ratio')
    ax2.set_xlabel(obs_names[_])
    ax2.set_ylim(0.8,1.2)
    if density == False:
        ax1.set_ylabel('Events')
    else:
        ax1.set_ylabel('Density')
    
    if _ == 5:
        min_val = np.min([np.min(X_sim_hadrons[:,_]),np.min(X_measured_hadrons[:,_])])
        max_val = np.max([np.max(X_sim_hadrons[:,_]),np.max(X_measured_hadrons[:,_])])
        if density == True:
            min_val = np.max([np.quantile(X_sim_hadrons[:,_],0.01),np.quantile(X_measured_hadrons[:,_],0.01)])
            max_val = np.min([np.quantile(X_sim_hadrons[:,_],0.99),np.quantile(X_measured_hadrons[:,_],0.99)])            
        asim,b,c = ax1.hist(X_sim_hadrons[:,_],bins=np.arange(min_val-0.5,max_val+1.5,1.0),histtype='step',color=color_sim,label=label_sim,density=density)
    elif _ == 6:
        min_val = np.min([np.min(X_sim_hadrons[:,_]),np.min(X_measured_hadrons[:,_])])
        max_val = np.max([np.max(X_sim_hadrons[:,_]),np.max(X_measured_hadrons[:,_])])
        if density == True:
            min_val = np.max([np.quantile(X_sim_hadrons[:,_],0.01),np.quantile(X_measured_hadrons[:,_],0.01)])
            max_val = np.min([np.quantile(X_sim_hadrons[:,_],0.99),np.quantile(X_measured_hadrons[:,_],0.99)])            
        asim,b,c = ax1.hist(X_sim_hadrons[:,_],bins=np.arange(min_val-1.0,max_val+3,2.0),histtype='step',color=color_sim,label=label_sim,density=density)
    elif _ == 0:
        min_val = np.min([np.min(1-X_sim_hadrons[:,_]),np.min(1-X_measured_hadrons[:,_])])
        max_val = np.max([np.max(1-X_sim_hadrons[:,_]),np.max(1-X_measured_hadrons[:,_])])
        if density == True:
            bins = np.linspace(np.max([np.quantile(1-X_sim_hadrons[:,_],0.01),np.quantile(1-X_measured_hadrons[:,_],0.01)]),np.min([np.quantile(1-X_sim_hadrons[:,_],0.99),np.quantile(1-X_measured_hadrons[:,_],0.99)]),25)
            #bins = np.array([np.quantile(1-X_measured_hadrons[:,_],i/10) for i in range(11)])
        else:
            bins = np.linspace(min_val,max_val,25)
        asim,b,c = ax1.hist(1-X_sim_hadrons[:,_],bins=bins,histtype='step',color=color_sim,label=label_sim,density=density)
    else:
        min_val = np.min([np.min(X_sim_hadrons[:,_]),np.min(X_measured_hadrons[:,_])])
        max_val = np.max([np.max(X_sim_hadrons[:,_]),np.max(X_measured_hadrons[:,_])])
        if density == True:
            bins = np.linspace(np.max([np.quantile(X_sim_hadrons[:,_],0.01),np.quantile(X_measured_hadrons[:,_],0.01)]),np.min([np.quantile(X_sim_hadrons[:,_],0.99),np.quantile(X_measured_hadrons[:,_],0.99)]),25)
            #bins = np.array([np.quantile(X_measured_hadrons[:,_],i/10) for i in range(11)])
        else:
            bins = np.linspace(min_val,max_val,25)
        asim,b,c = ax1.hist(X_sim_hadrons[:,_],bins=bins,histtype='step',color=color_sim,label=label_sim,density=density)
    if _ == 0:
        adata,b,c = ax1.hist(1.0-X_measured_hadrons[:,_],bins=b,histtype='step',color=color_data,label=label_data,density=density)
        
        astep1,b,c = ax1.hist(1.0-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_first_step,histtype='step',color=color_first_step,label=label_first_step,density=density)
        errstep12, b = np.histogram(1.0-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_first_step**2)

        astep2,b,c = ax1.hist(1.0-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_second_step,histtype='step',color=color_second_step,label=label_second_step,density=density)
        errstep22, b = np.histogram(1.0-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_second_step**2)

        astep2_regen,b,c = ax1.hist(1.0-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_new_fragmentation_function,histtype='step',color=color_new_fragmentation_function,label=label_new_fragmentation_function,density=density)
        errstep2_regen2, b = np.histogram(1.0-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_new_fragmentation_function**2)
        
        if has_best_NN:
            afromsplit,b,c = ax1.hist(1.0-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_from_split,histtype='step',color=color_from_split,label=label_from_split,density=density)
            errfromsplit2, b = np.histogram(1.0-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_from_split**2)

        aoptimal,b,c = ax1.hist(1.0-X_sim_hadrons[:,_],bins=b,weights=np.exp(0.5*sim_optimal_observable),histtype='step',color=color_optimal,label=label_optimal,density=density)
        erroptimal2, b = np.histogram(1.0-X_sim_hadrons[:,_],bins=b,weights=np.exp(0.5*sim_optimal_observable)**2)

    else:
        adata,b,c = ax1.hist(X_measured_hadrons[:,_],bins=b,histtype='step',color=color_data,label=label_data,density=density)
        
        astep1,b,c = ax1.hist(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_first_step,histtype='step',color=color_first_step,label=label_first_step,density=density)
        errstep12, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_first_step**2)

        astep2,b,c = ax1.hist(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_second_step,histtype='step',color=color_second_step,label=label_second_step,density=density)
        errstep22, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_second_step**2)

        astep2_regen,b,c = ax1.hist(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_new_fragmentation_function,histtype='step',color=color_new_fragmentation_function,label=label_new_fragmentation_function,density=density)
        errstep2_regen2, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_new_fragmentation_function**2)

        if has_best_NN:
            afromsplit,b,c = ax1.hist(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_from_split,histtype='step',color=color_from_split,label=label_from_split,density=density)
            errfromsplit2, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_from_split**2)

        aoptimal,b,c = ax1.hist(X_sim_hadrons[:,_],bins=b,weights=np.exp(0.5*sim_optimal_observable),histtype='step',color=color_optimal,label=label_optimal,density=density)
        erroptimal2, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=np.exp(0.5*sim_optimal_observable)**2)

    if density == False:
        errsim2=asim
        errdata2=adata
    else:
        errsim2=(asim*(b[1]-b[0])/len(X_sim_hadrons))*(1+asim*(b[1]-b[0]))/(b[1]-b[0])**2        
        errdata2=(adata*(b[1]-b[0])/len(X_measured_hadrons))*(1+adata*(b[1]-b[0]))/(b[1]-b[0])**2

        if _ == 0:

            astep1_aux, b = np.histogram(1-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_first_step)
            errstep12=(errstep12*(1.0/np.sum(weights_per_event_first_step))**2+((astep1_aux/(np.sum(weights_per_event_first_step))**2)**2) *(np.sum(weights_per_event_first_step**2)))/(b[1]-b[0])**2

            astep2_aux, b = np.histogram(1-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_second_step)
            errstep22=(errstep22*(1.0/np.sum(weights_per_event_second_step))**2+((astep2_aux/(np.sum(weights_per_event_second_step))**2)**2) *(np.sum(weights_per_event_second_step**2)))/(b[1]-b[0])**2

            astep2_regen_aux, b = np.histogram(1-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_new_fragmentation_function)
            errstep2_regen2=(errstep2_regen2*(1.0/np.sum(weights_per_event_new_fragmentation_function))**2+((astep2_regen_aux/(np.sum(weights_per_event_new_fragmentation_function))**2)**2) *(np.sum(weights_per_event_new_fragmentation_function**2)))/(b[1]-b[0])**2

            if has_best_NN:
                afromsplit_aux, b = np.histogram(1-X_sim_hadrons[:,_],bins=b,weights=weights_per_event_from_split)
                errfromsplit2=(errfromsplit2*(1.0/np.sum(weights_per_event_from_split))**2+((afromsplit_aux/(np.sum(weights_per_event_from_split))**2)**2) *(np.sum(weights_per_event_from_split**2)))/(b[1]-b[0])**2

            aoptimal_aux, b = np.histogram(1-X_sim_hadrons[:,_],bins=b,weights=np.exp(0.5*sim_optimal_observable))
            erroptimal2=(erroptimal2*(1.0/np.sum(np.exp(0.5*sim_optimal_observable)))**2+((aoptimal_aux/(np.sum(np.exp(0.5*sim_optimal_observable)))**2)**2) *(np.sum(np.exp(0.5*sim_optimal_observable)**2)))/(b[1]-b[0])**2


        else:

            astep1_aux, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_first_step)
            errstep12=(errstep12*(1.0/np.sum(weights_per_event_first_step))**2+((astep1_aux/(np.sum(weights_per_event_first_step))**2)**2)*(np.sum(weights_per_event_first_step**2)))/(b[1]-b[0])**2

            astep2_aux, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_second_step)
            errstep22=(errstep22*(1.0/np.sum(weights_per_event_second_step))**2+((astep2_aux/(np.sum(weights_per_event_second_step))**2)**2) *(np.sum(weights_per_event_second_step**2)))/(b[1]-b[0])**2

            astep2_regen_aux, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_new_fragmentation_function)
            errstep2_regen2=(errstep2_regen2*(1.0/np.sum(weights_per_event_new_fragmentation_function))**2+((astep2_regen_aux/(np.sum(weights_per_event_new_fragmentation_function))**2)**2) *(np.sum(weights_per_event_new_fragmentation_function**2)))/(b[1]-b[0])**2

            if has_best_NN:
                afromsplit_aux, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=weights_per_event_from_split)
                errfromsplit2=(errfromsplit2*(1.0/np.sum(weights_per_event_from_split))**2+((afromsplit_aux/(np.sum(weights_per_event_from_split))**2)**2) *(np.sum(weights_per_event_from_split**2)))/(b[1]-b[0])**2

            aoptimal_aux, b = np.histogram(X_sim_hadrons[:,_],bins=b,weights=np.exp(0.5*sim_optimal_observable))
            erroptimal2=(erroptimal2*(1.0/np.sum(np.exp(0.5*sim_optimal_observable)))**2+((aoptimal_aux/(np.sum(np.exp(0.5*sim_optimal_observable)))**2)**2) *(np.sum(np.exp(0.5*sim_optimal_observable)**2)))/(b[1]-b[0])**2



    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=asim,yerr=np.sqrt(errsim2),fmt='none',color=color_sim)
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=adata,yerr=np.sqrt(errdata2),fmt='none',color=color_data)
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=astep1,yerr=np.sqrt(errstep12),fmt='none',color=color_first_step)
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=astep2,yerr=np.sqrt(errstep22),fmt='none',color=color_second_step)
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=astep2_regen,yerr=np.sqrt(errstep2_regen2),fmt='none',color=color_new_fragmentation_function)
    if has_best_NN:
        ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=afromsplit,yerr=np.sqrt(errfromsplit2),fmt='none',color=color_from_split)
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=aoptimal,yerr=np.sqrt(erroptimal2),fmt='none',color=color_optimal)

    xvals = 0.5*(b[1:]+b[:-1])


    if density == False:
        chisim_data = np.sum((asim-adata)**2 / (np.where(errdata2>0,errdata2,1)+np.where(errsim2>0,errsim2,1)))/(len(b)-1)
        chistep1_data = np.sum((astep1-adata)**2 / (np.where(errdata2>0,errdata2,1)+np.where(errstep12>0,errstep12,1)))/(len(b)-1)
        chistep2_data = np.sum((astep2-adata)**2 / (np.where(errdata2>0,errdata2,1)+np.where(errstep22>0,errstep22,1)))/(len(b)-1)
        chistep2_regen_data = np.sum((astep2_regen-adata)**2 / (np.where(errdata2>0,errdata2,1)+np.where(errstep2_regen2>0,errstep2_regen2,1)))/(len(b)-1)
        if has_best_NN:
            chifromsplit_data = np.sum((afromsplit-adata)**2 / (np.where(errdata2>0,errdata2,1)+np.where(errfromsplit2>0,errfromsplit2,1)))/(len(b)-1)
        chisoptimal_data = np.sum((aoptimal-adata)**2 / (np.where(errdata2>0,errdata2,1)+np.where(erroptimal2>0,erroptimal2,1)))/(len(b)-1)
    else:
        chisim_data = np.sum((asim-adata)**2 / (np.where(errdata2>0,errdata2,(1*(b[1]-b[0])/len(X_measured_hadrons))*(1+1*(b[1]-b[0]))/(b[1]-b[0])**2)+np.where(errsim2>0,errsim2,(1*(b[1]-b[0])/len(X_sim_hadrons))*(1+1*(b[1]-b[0]))/(b[1]-b[0])**2)))/(len(b)-1)
        chistep1_data = np.sum((astep1-adata)**2 / (np.where(errdata2>0,errdata2,(1*(b[1]-b[0])/len(X_measured_hadrons))*(1+1*(b[1]-b[0]))/(b[1]-b[0])**2)+np.where(errstep12>0,errstep12,(1/len(X_sim_hadrons)**2)*(1+1/len(X_sim_hadrons))/(b[1]-b[0])**2)))/(len(b)-1)
        chistep2_data = np.sum((astep2-adata)**2 / (np.where(errdata2>0,errdata2,(1*(b[1]-b[0])/len(X_measured_hadrons))*(1+1*(b[1]-b[0]))/(b[1]-b[0])**2)+np.where(errstep22>0,errstep22,(1/len(X_sim_hadrons)**2)*(1+1/len(X_sim_hadrons))/(b[1]-b[0])**2)))/(len(b)-1)
        chistep2_regen_data = np.sum((astep2_regen-adata)**2 / (np.where(errdata2>0,errdata2,(1*(b[1]-b[0])/len(X_measured_hadrons))*(1+1*(b[1]-b[0]))/(b[1]-b[0])**2)+np.where(errstep2_regen2>0,errstep2_regen2,1)))/(len(b)-1)
        if has_best_NN:
            chifromsplit_data = np.sum((afromsplit-adata)**2 / (np.where(errdata2>0,errdata2,(1*(b[1]-b[0])/len(X_measured_hadrons))*(1+1*(b[1]-b[0]))/(b[1]-b[0])**2)+np.where(errfromsplit2>0,errfromsplit2,(1/len(X_sim_hadrons)**2)*(1+1/len(X_sim_hadrons))/(b[1]-b[0])**2)))/(len(b)-1)
        chisoptimal_data = np.sum((aoptimal-adata)**2 / (np.where(errdata2>0,errdata2,(1*(b[1]-b[0])/len(X_measured_hadrons))*(1+1*(b[1]-b[0]))/(b[1]-b[0])**2)+np.where(erroptimal2>0,erroptimal2,(1/len(X_sim_hadrons)**2)*(1+1/len(X_sim_hadrons))/(b[1]-b[0])**2)))/(len(b)-1)


    ax2.plot(xvals,np.ones(len(b)-1),color=color_data)
    ax2.plot(xvals,asim/adata,color=color_sim,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chisim_data)))
    ax2.plot(xvals,astep1/adata,color=color_first_step,label='$\chi^2$ / $N_{\mathrm{bins}}$  = '+str(round(chistep1_data,2)))
    print(str(_))
    print("p-values")
    print(st.chi2(df=len(b)-1).sf(chisim_data))
    print(st.chi2(df=len(b)-1).sf(chistep1_data))
    print(st.chi2(df=len(b)-1).sf(chistep2_data))
    print(st.chi2(df=len(b)-1).sf(chistep2_regen_data))
    if has_best_NN:
        print(st.chi2(df=len(b)-1).sf(chifromsplit_data))
    print(st.chi2(df=len(b)-1).sf(chisoptimal_data))
    
    print(astep1/adata)
    print((astep1-adata)**2 / (np.where(errstep12>0,errstep12,(1/len(X_sim_hadrons)**2)*(1+1/len(X_sim_hadrons))/(b[1]-b[0])**2)))
    ax2.plot(xvals,astep2/adata,color=color_second_step,label='$\chi^2$ / $N_{\mathrm{bins}}$  = '+str(round(chistep2_data,2)))
    print(astep2/adata)
    print((astep2-adata)**2 / (np.where(errstep22>0,errstep22,(1/len(X_sim_hadrons)**2)*(1+1/len(X_sim_hadrons))/(b[1]-b[0])**2)))
    ax2.plot(xvals,astep2_regen/adata,color=color_new_fragmentation_function,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chistep2_regen_data,2)))
    if has_best_NN:
        ax2.plot(xvals,afromsplit/adata,color=color_from_split,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chifromsplit_data,2)))
    ax2.plot(xvals,aoptimal/adata,color=color_optimal,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chisoptimal_data,2)))

    er_ratio = np.sqrt(errsim2*1/adata**2 + errdata2 * (asim**2/adata**4))
    ax2.errorbar(x=xvals,y=asim/adata,yerr=er_ratio,fmt='none',color=color_sim)

    er_ratio = np.sqrt(errstep12*1/adata**2 + errdata2 * (astep1**2/adata**4))
    ax2.errorbar(x=xvals,y=astep1/adata,yerr=er_ratio,fmt='none',color=color_first_step)

    er_ratio = np.sqrt(errstep22*1/adata**2 + errdata2 * (astep2**2/adata**4))
    ax2.errorbar(x=xvals,y=astep2/adata,yerr=er_ratio,fmt='none',color=color_second_step)

    er_ratio = np.sqrt(errstep2_regen2*1/adata**2 + errdata2 * (astep2_regen**2/adata**4))
    ax2.errorbar(x=xvals,y=astep2_regen/adata,yerr=er_ratio,fmt='none',color=color_new_fragmentation_function)

    if has_best_NN:
        er_ratio = np.sqrt(errfromsplit2*1/adata**2 + errdata2 * (afromsplit**2/adata**4))
        ax2.errorbar(x=xvals,y=afromsplit/adata,yerr=er_ratio,fmt='none',color=color_from_split)

    er_ratio = np.sqrt(erroptimal2*1/adata**2 + errdata2 * (aoptimal**2/adata**4))
    ax2.errorbar(x=xvals,y=aoptimal/adata,yerr=er_ratio,fmt='none',color=color_optimal)



    ax1.legend(loc='best',fontsize=12,framealpha=0.1)
    ax2.legend(loc='best',fontsize=12,framealpha=0.8)
    plt.tight_layout()
    plt.savefig(results_dir+'high_level_obs_'+str(_)+'.pdf')

    ax1.set_yscale('log')
    plt.savefig(results_dir+'high_level_obs_'+str(_)+'_log_scale.pdf')
    plt.clf()

plt.close('all')


##### plot all zvals and then slice on mT2


fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
ax1.set_ylabel(r'$\langle f(z)\rangle$')
ax2.set_ylabel('Ratio')
ax2.set_ylim(0.8,1.2)
plt.xlabel('$z$')
a,b,c = ax1.hist(X_measured_splits[:,:,0].flatten()[valid_values_measured],bins=50,histtype='step',color=color_data,label=label_data,density=True)
er = np.sqrt(a*(b[1]-b[0])/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color=color_data)

a2,b,c = ax1.hist(X_sim_splits[:,:,0].flatten()[valid_values_sim],bins=b,histtype='step',color=color_sim,label=label_sim,density=True)
er2 = np.sqrt(a2*(b[1]-b[0])/len(X_sim_splits[:,:,0].flatten()[valid_values_sim]))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_sim)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured])**2)*(1+1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim])**2)*(1+1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim]))/(b[1]-b[0])**2))
chi = np.sum(chi)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_sim,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))

a2,b,c = ax1.hist(X_sim_splits[:,:,0].flatten()[valid_values_sim],bins=b,weights=weights_per_split_new_fragmentation_function,histtype='step',color=color_new_fragmentation_function,label=label_new_fragmentation_function,density=True)
a2_aux, b = np.histogram(X_sim_splits[:,:,0].flatten()[valid_values_sim],bins=b,weights=weights_per_split_new_fragmentation_function)
er2, b = np.histogram(X_sim_splits[:,:,0].flatten()[valid_values_sim],bins=b,weights=weights_per_split_new_fragmentation_function**2)
er2=np.sqrt(er2*(1.0/np.sum(weights_per_split_new_fragmentation_function))**2+((a2_aux/(np.sum(weights_per_split_new_fragmentation_function))**2)**2) *(np.sum(weights_per_split_new_fragmentation_function**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_new_fragmentation_function)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured])**2)*(1+1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim])**2)*(1+1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim]))/(b[1]-b[0])**2))
chi = np.sum(chi)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_new_fragmentation_function,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))

if has_best_NN:
    a2,b,c = ax1.hist(X_sim_splits[:,:,0].flatten()[valid_values_sim],bins=b,weights=weights_per_split_from_split,histtype='step',color=color_from_split,label=label_from_split,density=True)
    a2_aux, b = np.histogram(X_sim_splits[:,:,0].flatten()[valid_values_sim],bins=b,weights=weights_per_split_from_split)
    er2, b = np.histogram(X_sim_splits[:,:,0].flatten()[valid_values_sim],bins=b,weights=weights_per_split_from_split**2)
    er2=np.sqrt(er2*(1.0/np.sum(weights_per_split_from_split))**2+((a2_aux/(np.sum(weights_per_split_from_split))**2)**2) *(np.sum(weights_per_split_from_split**2)))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_from_split)
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    chi = (a2-a)**2
    chi*=1/(np.where(er>0,er**2,(1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured])**2)*(1+1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim])**2)*(1+1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim]))/(b[1]-b[0])**2))
    chi = np.sum(chi)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_from_split,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))




ax1.legend(loc='best',fontsize=12,framealpha=0.1)
ax2.legend(loc='best',fontsize=12,framealpha=0.8)

ax2.axhline(1.0,linestyle='dashed',color='black')
plt.tight_layout()
plt.savefig(results_dir+'reweighted_emissions_ratio.pdf')
plt.clf()


plt.close('all')


emissions_mT2 = np.sqrt((X_sim_splits[:,:,1].flatten())**2+(X_sim_splits[:,:,2].flatten())**2)[valid_values_sim]
emissions_mT2_truth = np.sqrt((X_measured_splits[:,:,1].flatten())**2+(X_measured_splits[:,:,2].flatten())**2)[valid_values_measured]

fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
ax1.set_ylabel('Density')
ax2.set_ylabel('Ratio')
ax2.set_ylim(0.8,1.2)
plt.xlabel('$p^{kick}_{T}$')
a,b,c = ax1.hist(emissions_mT2_truth,bins=np.linspace(0.95*np.min([np.min(emissions_mT2),np.min(emissions_mT2_truth)]),1.05*np.max([np.max(emissions_mT2),np.max(emissions_mT2_truth)]),20),histtype='step',color=color_data,label=label_data,density=True)
er = np.sqrt(a*(b[1]-b[0])/len(emissions_mT2_truth))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color=color_data)

a2,b,c = ax1.hist(emissions_mT2,bins=b,histtype='step',color=color_sim,label=label_sim,density=True)
er2 = np.sqrt(a2*(b[1]-b[0])/len(emissions_mT2))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_sim)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured])**2)*(1+1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(emissions_mT2)**2)*(1+1/len(emissions_mT2))/(b[1]-b[0])**2))
chi = np.sum(chi)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_sim,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))


a2,b,c = ax1.hist(emissions_mT2,bins=b,weights=weights_per_split_new_fragmentation_function,histtype='step',color=color_new_fragmentation_function,label=label_new_fragmentation_function,density=True)
a2_aux, b = np.histogram(emissions_mT2,bins=b,weights=weights_per_split_new_fragmentation_function)
er2, b = np.histogram(emissions_mT2,bins=b,weights=weights_per_split_new_fragmentation_function**2)
er2=np.sqrt(er2*(1.0/np.sum(weights_per_split_new_fragmentation_function))**2+((a2_aux/(np.sum(weights_per_split_new_fragmentation_function))**2)**2) *(np.sum(weights_per_split_new_fragmentation_function**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_new_fragmentation_function)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured])**2)*(1+1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(emissions_mT2)**2)*(1+1/len(emissions_mT2))/(b[1]-b[0])**2))
chi = np.sum(chi)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_new_fragmentation_function,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))

if has_best_NN:
    a2,b,c = ax1.hist(emissions_mT2,bins=b,weights=weights_per_split_from_split,histtype='step',color=color_from_split,label=label_from_split,density=True)
    a2_aux, b = np.histogram(emissions_mT2,bins=b,weights=weights_per_split_from_split)
    er2, b = np.histogram(emissions_mT2,bins=b,weights=weights_per_split_from_split**2)
    er2=np.sqrt(er2*(1.0/np.sum(weights_per_split_from_split))**2+((a2_aux/(np.sum(weights_per_split_from_split))**2)**2) *(np.sum(weights_per_split_from_split**2)))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_from_split)
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    chi = (a2-a)**2
    chi*=1/(np.where(er>0,er**2,(1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured])**2)*(1+1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(emissions_mT2))*(1+1/len(emissions_mT2))/(b[1]-b[0])**2))
    chi = np.sum(chi)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_from_split,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))

ax1.legend(loc='best',fontsize=12,framealpha=0.1)
ax2.legend(loc='best',fontsize=12,framealpha=0.8)

ax2.axhline(1.0,linestyle='dashed',color='black')
plt.tight_layout()
plt.savefig(results_dir+'reweighted_emissions_ratio_transverse_added_momentum.pdf')
plt.clf()


plt.close('all')

emissions_mT2 = ((X_sim_splits[:,:,3].flatten())**2+(X_sim_splits[:,:,9].flatten())**2+(X_sim_splits[:,:,10].flatten())**2)[valid_values_sim]
emissions_mT2_truth = ((X_measured_splits[:,:,3].flatten())**2+(X_measured_splits[:,:,9].flatten())**2+(X_measured_splits[:,:,10].flatten())**2)[valid_values_measured]

fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
ax1.set_ylabel('Density')
ax2.set_ylabel('Ratio')
ax2.set_ylim(0.8,1.2)
plt.xlabel('$m^{2}_{T}$')
a,b,c = ax1.hist(emissions_mT2_truth,bins=np.linspace(0.95*np.min([np.min(emissions_mT2),np.min(emissions_mT2_truth)]),1.05*np.max([np.max(emissions_mT2),np.max(emissions_mT2_truth)]),20),histtype='step',color=color_data,label=label_data,density=True)
er = np.sqrt(a*(b[1]-b[0])/len(emissions_mT2_truth))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color=color_data)

a2,b,c = ax1.hist(emissions_mT2,bins=b,histtype='step',color=color_sim,label=label_sim,density=True)
er2 = np.sqrt(a2*(b[1]-b[0])/len(emissions_mT2))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_sim)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured])**2)*(1+1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(emissions_mT2)**2)*(1+1/len(emissions_mT2))/(b[1]-b[0])**2))
chi = np.sum(chi)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_sim,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))


a2,b,c = ax1.hist(emissions_mT2,bins=b,weights=weights_per_split_new_fragmentation_function,histtype='step',color=color_new_fragmentation_function,label=label_new_fragmentation_function,density=True)
a2_aux, b = np.histogram(emissions_mT2,bins=b,weights=weights_per_split_new_fragmentation_function)
er2, b = np.histogram(emissions_mT2,bins=b,weights=weights_per_split_new_fragmentation_function**2)
er2=np.sqrt(er2*(1.0/np.sum(weights_per_split_new_fragmentation_function))**2+((a2_aux/(np.sum(weights_per_split_new_fragmentation_function))**2)**2) *(np.sum(weights_per_split_new_fragmentation_function**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_new_fragmentation_function)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured])**2)*(1+1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(emissions_mT2)**2)*(1+1/len(emissions_mT2))/(b[1]-b[0])**2))
chi = np.sum(chi)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_new_fragmentation_function,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))

if has_best_NN:
    a2,b,c = ax1.hist(emissions_mT2,bins=b,weights=weights_per_split_from_split,histtype='step',color=color_from_split,label=label_from_split,density=True)
    a2_aux, b = np.histogram(emissions_mT2,bins=b,weights=weights_per_split_from_split)
    er2, b = np.histogram(emissions_mT2,bins=b,weights=weights_per_split_from_split**2)
    er2=np.sqrt(er2*(1.0/np.sum(weights_per_split_from_split))**2+((a2_aux/(np.sum(weights_per_split_from_split))**2)**2) *(np.sum(weights_per_split_from_split**2)))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_from_split)
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    chi = (a2-a)**2
    chi*=1/(np.where(er>0,er**2,(1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured])**2)*(1+1/len(X_measured_splits[:,:,0].flatten()[valid_values_measured]))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(emissions_mT2))*(1+1/len(emissions_mT2))/(b[1]-b[0])**2))
    chi = np.sum(chi)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_from_split,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))



ax1.legend(loc='best',fontsize=12,framealpha=0.1)
ax2.legend(loc='best',fontsize=12,framealpha=0.8)

ax2.axhline(1.0,linestyle='dashed',color='black')
plt.tight_layout()
plt.savefig(results_dir+'reweighted_emissions_ratio_transverse_mass.pdf')
plt.clf()


plt.close('all')

mT2_quantiles = np.array([np.quantile(emissions_mT2,i*10/100) for i in range(11)])
mT2_vals = 0.5*(mT2_quantiles[1:]+mT2_quantiles[:-1])
print(mT2_quantiles)

for nmT2_val, mT2_val in enumerate(mT2_vals):
    mask_mT2 = [ a and b for a,b in zip(emissions_mT2>=mT2_quantiles[nmT2_val],emissions_mT2<mT2_quantiles[nmT2_val+1])] 
    mask_mT2_truth = [ a and b for a,b in zip(emissions_mT2_truth>=mT2_quantiles[nmT2_val],emissions_mT2_truth<mT2_quantiles[nmT2_val+1])] 
    print(nmT2_val)
    fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
    ax1.set_ylabel(r'$f(z)$')
    ax2.set_ylabel('Ratio')
    ax2.set_ylim(0.8,1.2)
    ax2.set_xlabel('$z$')
    ax1.set_title('$m^{2}_{T} \in$ ['+str(np.round(mT2_quantiles[nmT2_val],3))+', '+str(np.round(mT2_quantiles[nmT2_val+1],3))+')')


    a, b, c = ax1.hist(X_measured_splits[:,:,0].flatten()[valid_values_measured][mask_mT2_truth],bins=np.linspace(0,1,20),histtype='step',color=color_data,density=True,label=label_data)
    er = np.sqrt(a*(b[1]-b[0])/len(X_measured_splits[:,:,0].flatten()[valid_values_measured][mask_mT2_truth]))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color=color_data)

    
    a2, b, c = ax1.hist(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2],bins=b,histtype='step',color=color_sim,density=True,label=label_sim)
    er2 = np.sqrt(a2*(b[1]-b[0])/len(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2]))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_sim)
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    chi = (a2-a)**2
    chi*=1/np.where(er2>0,er2**2,(1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2])**2)*(1+1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2]))/(b[1]-b[0])**2)
    chi = np.sum(chi)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_sim,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))
    

    a2, b, c = ax1.hist(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2],bins=b,histtype='step',weights=weights_per_split_new_fragmentation_function[mask_mT2],color=color_new_fragmentation_function,density=True,label=label_new_fragmentation_function)
    print(label_new_fragmentation_function)
    print(np.mean(weights_per_split_new_fragmentation_function[mask_mT2]),np.sum(weights_per_split_new_fragmentation_function[mask_mT2])/len(weights_per_split_new_fragmentation_function))
    a2_aux, b = np.histogram(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2],bins=b,weights=weights_per_split_new_fragmentation_function[mask_mT2])
    er2, b = np.histogram(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2],bins=b,weights=weights_per_split_new_fragmentation_function[mask_mT2]**2)
    er2=np.sqrt(er2*(1.0/np.sum(weights_per_split_new_fragmentation_function[mask_mT2]))**2+((a2_aux/(np.sum(weights_per_split_new_fragmentation_function[mask_mT2]))**2)**2) *(np.sum(weights_per_split_new_fragmentation_function[mask_mT2]**2)))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_new_fragmentation_function)
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    chi = (a2-a)**2
    chi*=1/np.where(er2>0,er2**2,(1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2])**2)*(1+1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2]))/(b[1]-b[0])**2)
    chi = np.sum(chi)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_new_fragmentation_function,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))


    if has_best_NN:
        a2, b, c = ax1.hist(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2],bins=b,histtype='step',weights=weights_per_split_from_split[mask_mT2],color=color_from_split,density=True,label=label_from_split)
        print(label_from_split)
        print(np.mean(weights_per_split_from_split[mask_mT2]),np.sum(weights_per_split_from_split[mask_mT2])/len(weights_per_split_from_split))
        a2_aux, b = np.histogram(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2],bins=b,weights=weights_per_split_from_split[mask_mT2])
        er2, b = np.histogram(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2],bins=b,weights=weights_per_split_from_split[mask_mT2]**2)
        er2=np.sqrt(er2*(1.0/np.sum(weights_per_split_from_split[mask_mT2]))**2+((a2_aux/(np.sum(weights_per_split_from_split[mask_mT2]))**2)**2) *(np.sum(weights_per_split_from_split[mask_mT2]**2)))/(b[1]-b[0])
        ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_from_split)
        er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
        chi = (a2-a)**2
        chi*=1/np.where(er2>0,er2**2,(1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2])**2)*(1+1/len(X_sim_splits[:,:,0].flatten()[valid_values_sim][mask_mT2]))/(b[1]-b[0])**2)
        chi = np.sum(chi)
        ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_from_split,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))

    ax1.legend(loc='best',fontsize=12,framealpha=0.1)
    ax2.legend(loc='best',fontsize=12,framealpha=0.8)

    plt.tight_layout()
    plt.savefig(results_dir+'learned_hadronization_function_density_'+str(nmT2_val)+'_th_quantile.pdf')
    ax1.set_yscale('log')
    plt.savefig(results_dir+'learned_hadronization_function_density_'+str(nmT2_val)+'_th_quantile_log_scale.pdf')
    plt.clf()
    

plt.close('all')

bins = np.linspace(np.min([np.quantile(sim_optimal_observable,0.01),np.quantile(measured_optimal_observable,0.01)]),np.max([np.quantile(sim_optimal_observable,0.99),np.quantile(measured_optimal_observable,0.99)]),50)

fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
ax1.set_ylabel('Density')
ax2.set_ylabel('Ratio')
ax2.set_ylim(0.5,1.5)
plt.xlabel(r'-2 ln $w_{\mathrm{exact}}$')

a, b, c = ax1.hist(measured_optimal_observable,bins=bins,color=color_data,label=label_data,density='True',histtype='step')
er = np.sqrt(a*(b[1]-b[0])/len(measured_optimal_observable))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color=color_data)

significance = 0.05
value = np.quantile(-measured_optimal_observable,1-significance)
print(label_data)
print(np.sum(a[bins[:-1]<-value]*(b[1]-b[0])))
print(np.mean(-measured_optimal_observable>value))

a2, b, c = ax1.hist(sim_optimal_observable,bins=bins,color=color_sim,label=label_sim,density='True',histtype='step')
print(label_sim)
power = np.mean(-sim_optimal_observable>value)
print(power)
print(np.sum(a2[bins[:-1]<-value]*(b[1]-b[0])))

er2 = np.sqrt(a2*(b[1]-b[0])/len(sim_optimal_observable))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_sim)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(measured_optimal_observable)**2)*(1+1/len(measured_optimal_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_optimal_observable)**2)*(1+1/len(sim_optimal_observable))/(b[1]-b[0])**2))
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_sim,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))


a2, b, c = ax1.hist(sim_optimal_observable,bins=bins,weights=weights_per_event_first_step,color=color_first_step,label=label_first_step,density='True',histtype='step')
print(label_first_step)
power = np.sum(a2[bins[:-1]<-value]*(b[1]-b[0]))
print(power)
print(np.sum(weights_per_event_first_step[-sim_optimal_observable>value])/np.sum(weights_per_event_first_step))
print(np.sum(weights_per_event_first_step[-sim_optimal_observable>value])/len(sim_optimal_observable),np.mean(weights_per_event_first_step))

a2_aux, b = np.histogram(sim_optimal_observable,bins=b,weights=weights_per_event_first_step)
er2, b = np.histogram(sim_optimal_observable,bins=b,weights=weights_per_event_first_step**2)
er2=np.sqrt(er2*(1.0/np.sum(weights_per_event_first_step))**2+((a2_aux/(np.sum(weights_per_event_first_step))**2)**2) *(np.sum(weights_per_event_first_step**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_first_step)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(measured_optimal_observable)**2)*(1+1/len(measured_optimal_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_optimal_observable)**2)*(1+1/len(sim_optimal_observable))/(b[1]-b[0])**2))
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_first_step,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))


a2, b, c = ax1.hist(sim_optimal_observable,bins=bins,weights=weights_per_event_second_step,color=color_second_step,label=label_second_step,density='True',histtype='step')
print(label_second_step)
power = np.sum(a2[bins[:-1]<-value]*(b[1]-b[0]))
print(power)
print(np.sum(weights_per_event_second_step[-sim_optimal_observable>value])/np.sum(weights_per_event_second_step))
print(np.sum(weights_per_event_second_step[-sim_optimal_observable>value])/len(sim_optimal_observable),np.mean(weights_per_event_second_step))

a2_aux, b = np.histogram(sim_optimal_observable,bins=b,weights=weights_per_event_second_step)
er2, b = np.histogram(sim_optimal_observable,bins=b,weights=weights_per_event_second_step**2)
er2=np.sqrt(er2*(1.0/np.sum(weights_per_event_second_step))**2+((a2_aux/(np.sum(weights_per_event_second_step))**2)**2) *(np.sum(weights_per_event_second_step**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_second_step)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(measured_optimal_observable)**2)*(1+1/len(measured_optimal_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_optimal_observable)**2)*(1+1/len(sim_optimal_observable))/(b[1]-b[0])**2))
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_second_step,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))


a2, b, c = ax1.hist(sim_optimal_observable,bins=bins,weights=weights_per_event_new_fragmentation_function,color=color_new_fragmentation_function,label=label_new_fragmentation_function,density='True',histtype='step')
print(label_new_fragmentation_function)
power = np.sum(a2[bins[:-1]<-value]*(b[1]-b[0]))
print(power)
print(np.sum(weights_per_event_new_fragmentation_function[-sim_optimal_observable>value])/np.sum(weights_per_event_new_fragmentation_function))
print(np.sum(weights_per_event_new_fragmentation_function[-sim_optimal_observable>value])/len(sim_optimal_observable),np.mean(weights_per_event_new_fragmentation_function))


a2_aux, b = np.histogram(sim_optimal_observable,bins=b,weights=weights_per_event_new_fragmentation_function)
er2, b = np.histogram(sim_optimal_observable,bins=b,weights=weights_per_event_new_fragmentation_function**2)
er2=np.sqrt(er2*(1.0/np.sum(weights_per_event_new_fragmentation_function))**2+((a2_aux/(np.sum(weights_per_event_new_fragmentation_function))**2)**2) *(np.sum(weights_per_event_new_fragmentation_function**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_new_fragmentation_function)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(measured_optimal_observable)**2)*(1+1/len(measured_optimal_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_optimal_observable)**2)*(1+1/len(sim_optimal_observable))/(b[1]-b[0])**2))
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_new_fragmentation_function,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))


if has_best_NN:
    a2, b, c = ax1.hist(sim_optimal_observable,bins=bins,weights=weights_per_event_from_split,color=color_from_split,label=label_from_split,density='True',histtype='step')
    print(label_from_split)
    power = np.sum(a2[bins[:-1]<-value]*(b[1]-b[0]))
    print(power)
    print(np.sum(weights_per_event_from_split[-sim_optimal_observable>value])/np.sum(weights_per_event_from_split))
    print(np.sum(weights_per_event_from_split[-sim_optimal_observable>value])/len(sim_optimal_observable),np.mean(weights_per_event_from_split))

    a2_aux, b = np.histogram(sim_optimal_observable,bins=b,weights=weights_per_event_from_split)
    er2, b = np.histogram(sim_optimal_observable,bins=b,weights=weights_per_event_from_split**2)
    er2=np.sqrt(er2*(1.0/np.sum(weights_per_event_from_split))**2+((a2_aux/(np.sum(weights_per_event_from_split))**2)**2) *(np.sum(weights_per_event_from_split**2)))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_from_split)
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    chi = (a2-a)**2
    chi*=1/(np.where(er>0,er**2,(1/len(measured_optimal_observable)**2)*(1+1/len(measured_optimal_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_optimal_observable)**2)*(1+1/len(sim_optimal_observable))/(b[1]-b[0])**2))
    chi = np.sum(chi)
    print(st.chi2(df=len(b)-1).sf(chi))
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_from_split,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))


a2, b, c = ax1.hist(sim_optimal_observable,bins=bins,weights=np.exp(0.5*sim_optimal_observable),color=color_optimal,label=label_optimal,linestyle='solid',density='True',histtype='step')
print(label_optimal)
power = np.sum(a2[bins[:-1]<-value]*(b[1]-b[0]))
print(power)
print(np.sum(np.exp(0.5*sim_optimal_observable)[-sim_optimal_observable>value])/np.sum(np.exp(0.5*sim_optimal_observable)))
print(np.sum(np.exp(0.5*sim_optimal_observable)[-sim_optimal_observable>value])/len(sim_optimal_observable),np.mean(np.exp(0.5*sim_optimal_observable)))

a2_aux, b = np.histogram(sim_optimal_observable,bins=b,weights=np.exp(0.5*sim_optimal_observable))
er2, b = np.histogram(sim_optimal_observable,bins=b,weights=np.exp(0.5*sim_optimal_observable)**2)
er2=np.sqrt(er2*(1.0/np.sum(np.exp(0.5*sim_optimal_observable)))**2+((a2_aux/(np.sum(np.exp(0.5*sim_optimal_observable)))**2)**2) *(np.sum(np.exp(0.5*sim_optimal_observable)**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_optimal)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
chi*=1/(np.where(er>0,er**2,(1/len(measured_optimal_observable)**2)*(1+1/len(measured_optimal_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_optimal_observable)**2)*(1+1/len(sim_optimal_observable))/(b[1]-b[0])**2))
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_optimal,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))

ax1.legend(loc='best',fontsize=12,framealpha=0.1)
ax2.legend(loc='best',fontsize=12,framealpha=0.8)
plt.tight_layout()
plt.savefig(results_dir+'optimal_observable.pdf')
plt.clf()

print("Measurable optimal observable")
bins = np.linspace(np.min([np.quantile(sim_measured_observable,0.01),np.quantile(measured_measured_observable,0.01)]),np.max([np.quantile(sim_measured_observable,0.99),np.quantile(measured_measured_observable,0.99)]),50)

fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
ax1.set_ylabel('Density')
ax2.set_ylabel('Ratio')
ax2.set_ylim(0.5,1.5)
plt.xlabel(r'-2 ln $w_{\mathrm{class}}$')


a, b, c = ax1.hist(measured_measured_observable,bins=bins,color=color_data,label=label_data,density='True',histtype='step')
er = np.sqrt(a*(b[1]-b[0])/len(measured_measured_observable))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color=color_data)
print("Data error")
print(a)
print(er)

a2, b, c = ax1.hist(sim_measured_observable,bins=bins,color=color_sim,label=label_sim,density='True',histtype='step')
er2 = np.sqrt(a2*(b[1]-b[0])/len(sim_measured_observable))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_sim)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
print("Sim")
print(chi)
chi*=1/np.where(er2>0,er2**2,(1/len(sim_measured_observable)**2)*(1+1/len(sim_measured_observable))/(b[1]-b[0])**2)
print(chi)
print(er2)
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_sim,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1))))


a2, b, c = ax1.hist(sim_measured_observable,bins=bins,weights=weights_per_event_first_step,color=color_first_step,label=label_first_step,density='True',histtype='step')
a2_aux, b = np.histogram(sim_measured_observable,bins=b,weights=weights_per_event_first_step)
er2, b = np.histogram(sim_measured_observable,bins=b,weights=weights_per_event_first_step**2)
er2=np.sqrt(er2*(1.0/np.sum(weights_per_event_first_step))**2+((a2_aux/(np.sum(weights_per_event_first_step))**2)**2) *(np.sum(weights_per_event_first_step**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_first_step)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
print("First step")
print(chi)
chi*=1/(np.where(er>0,er**2,(1/len(measured_measured_observable)**2)*(1+1/len(measured_measured_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_measured_observable)**2)*(1+1/len(sim_measured_observable))/(b[1]-b[0])**2))
print(chi)
print(er2)
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_first_step,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))


a2, b, c = ax1.hist(sim_measured_observable,bins=bins,weights=weights_per_event_second_step,color=color_second_step,label=label_second_step,density='True',histtype='step')
a2_aux, b = np.histogram(sim_measured_observable,bins=b,weights=weights_per_event_second_step)
er2, b = np.histogram(sim_measured_observable,bins=b,weights=weights_per_event_second_step**2)
er2=np.sqrt(er2*(1.0/np.sum(weights_per_event_second_step))**2+((a2_aux/(np.sum(weights_per_event_second_step))**2)**2) *(np.sum(weights_per_event_second_step**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_second_step)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
print("Second step")
print(chi)
chi*=1/(np.where(er>0,er**2,(1/len(measured_measured_observable)**2)*(1+1/len(measured_measured_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_measured_observable)**2)*(1+1/len(sim_measured_observable))/(b[1]-b[0])**2))
print(chi)
print(er2)
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_second_step,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))


a2, b, c = ax1.hist(sim_measured_observable,bins=bins,weights=weights_per_event_new_fragmentation_function,color=color_new_fragmentation_function,label=label_new_fragmentation_function,density='True',histtype='step')
a2_aux, b = np.histogram(sim_measured_observable,bins=b,weights=weights_per_event_new_fragmentation_function)
er2, b = np.histogram(sim_measured_observable,bins=b,weights=weights_per_event_new_fragmentation_function**2)
er2=np.sqrt(er2*(1.0/np.sum(weights_per_event_new_fragmentation_function))**2+((a2_aux/(np.sum(weights_per_event_new_fragmentation_function))**2)**2) *(np.sum(weights_per_event_new_fragmentation_function**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_new_fragmentation_function)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
print("Second step regen")
print(chi)
chi*=1/(np.where(er>0,er**2,(1/len(measured_measured_observable)**2)*(1+1/len(measured_measured_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_measured_observable)**2)*(1+1/len(sim_measured_observable))/(b[1]-b[0])**2))
print(chi)
print(er2)
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_new_fragmentation_function,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))


if has_best_NN:
    a2, b, c = ax1.hist(sim_measured_observable,bins=bins,weights=weights_per_event_from_split,color=color_from_split,label=label_from_split,density='True',histtype='step')
    a2_aux, b = np.histogram(sim_measured_observable,bins=b,weights=weights_per_event_from_split)
    er2=np.sqrt(er2*(1.0/np.sum(weights_per_event_from_split))**2+((a2_aux/(np.sum(weights_per_event_from_split))**2)**2) *(np.sum(weights_per_event_from_split**2)))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_from_split)
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    chi = (a2-a)**2
    print("Best NN")
    print(chi)
    chi*=1/(np.where(er>0,er**2,(1/len(measured_measured_observable)**2)*(1+1/len(measured_measured_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_measured_observable)**2)*(1+1/len(sim_measured_observable))/(b[1]-b[0])**2))
    print(chi)
    print(er2)
    chi = np.sum(chi)
    print(st.chi2(df=len(b)-1).sf(chi))
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_from_split,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))


a2, b, c = ax1.hist(sim_measured_observable,bins=bins,weights=np.exp(0.5*sim_optimal_observable),color=color_optimal,label=label_optimal,linestyle='solid',density='True',histtype='step')
a2_aux, b = np.histogram(sim_measured_observable,bins=b,weights=np.exp(0.5*sim_optimal_observable))
er2, b = np.histogram(sim_measured_observable,bins=b,weights=np.exp(0.5*sim_optimal_observable)**2)
er2=np.sqrt(er2*(1.0/np.sum(np.exp(0.5*sim_optimal_observable)))**2+((a2_aux/(np.sum(np.exp(0.5*sim_optimal_observable)))**2)**2)*(np.sum(np.exp(0.5*sim_optimal_observable)**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color=color_optimal)
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
chi = (a2-a)**2
print("Optimal")
print(chi)
chi*=1/(np.where(er>0,er**2,(1/len(measured_measured_observable)**2)*(1+1/len(measured_measured_observable))/(b[1]-b[0])**2)+np.where(er2>0,er2**2,(1/len(sim_measured_observable)**2)*(1+1/len(sim_measured_observable))/(b[1]-b[0])**2))
print(chi)
print(er2)
chi = np.sum(chi)
print(st.chi2(df=len(b)-1).sf(chi))
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color=color_optimal,label='$\chi^2$ / $N_{\mathrm{bins}}$ = '+str(round(chi/(len(b)-1),2)))

ax1.legend(loc='best',fontsize=12,framealpha=0.1)
ax2.legend(loc='best',fontsize=12,framealpha=0.8)
plt.tight_layout()
plt.savefig(results_dir+'measured_observable.pdf')
plt.clf()
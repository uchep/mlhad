import os
os.environ['OMP_NUM_THREADS'] = "1"

import numpy as np
import sys
import scipy.stats as st
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'size'   : 25}
rc('font', **font)
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
rc('text', usetex=True)
plt.rcParams['font.family']='Computer Modern'



from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import cross_val_predict, train_test_split, KFold

from sklearn.metrics import accuracy_score, precision_score, roc_auc_score
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.calibration import calibration_curve,CalibratedClassifierCV

import torch
torch.set_num_threads(1)

from torch import nn
from tqdm import tqdm
from torch.nn.modules import Module
from torch.nn import Sequential as Seq, Linear, ReLU, Parameter,LeakyReLU
from torch.utils.data import DataLoader, TensorDataset


###############################################################################
def bin_assigner(bins,val):

    # This function simply outputs the bin assignment of a given value val for a given choice of binning bins

    number_of_bins = len(bins)
    bin_number=0
    if val >= bins[-1]:
        bin_number = number_of_bins - 1
    if val < bins[0]:
        bin_number = 0
    else:
        for bin_number_aux in range(number_of_bins-1):
            if bins[bin_number_aux]<= val < bins[bin_number_aux+1]:
                bin_number = bin_number_aux
    return bin_number
###############################################################################

class sim_Dataset(torch.utils.data.Dataset):
    """
    Joins the x and y into a dataset, so that it can be used by the pythorch syntax.
    """

    def __init__(self, event,measurement):
        self.event = event
        self.measurement = measurement

    def __len__(self):
        return len(self.event)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        sample = [self.event[idx],self.measurement[idx]]
        return sample

###############################################################################
    
# Define model


class NeuralNetwork(nn.Module):
    def __init__(self,dim_input=13,dim_output=1,layers_data=[(64, nn.ReLU()),(64, nn.ReLU()),(64, nn.ReLU())]): # example of layers_data=[(layer1, nn.ReLU()), (layer2, nn.ReLU()), (output_size, nn.Sigmoid())]
        super(NeuralNetwork, self).__init__()
        self.flatten = nn.Flatten()
        self.layers = nn.ModuleList()
        self.input_size = dim_input  # Can be useful later ...
        self.output_size = dim_output  # Can be useful later ...
        input_size = dim_input
        for size, activation in layers_data:
            self.layers.append(nn.Linear(input_size, size))
            input_size = size  # For the next layer
            if activation is not None:
                assert isinstance(activation, Module), \
                    "Each tuples should contain a size (int) and a torch.nn.modules.Module."
                self.layers.append(activation)
        self.layers.append(nn.Linear(input_size, dim_output))
        self.layers.append(nn.Sigmoid())

    def forward(self, x):
        output = self.flatten(x)
        for layer in self.layers:
            output = layer(output)
            #print(output.shape)
        return output

    def loss_function(self,measured,predicted,predicted_weight_sqr,bin_lengths,size):
        loss = 0
        for nobs in range(len(bin_lengths)):
            t=measured[int(torch.sum(bin_lengths[:nobs])):int(torch.sum(bin_lengths[:nobs+1]))]
            pt = t*(size/torch.sum(t))
            sigmatsquared = torch.pow(torch.ones(1)*batch_size,2)*(t/torch.pow(torch.sum(t),2.0))*(1+t/torch.sum(t))

            y=predicted[int(torch.sum(bin_lengths[:nobs])):int(torch.sum(bin_lengths[:nobs+1]))]
            py = y

            loss+=(1/bin_lengths[nobs])*torch.sum(torch.pow((pt-py),2)/(1e-5+sigmatsquared))
        return loss

    def reset_weights(self):
        for m in self.layers:
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
                m.reset_parameters()

def train(model,optimizer,train_dataset,measured_events,bin_lengths):
    model.train()
    total_loss = 0
    for batch, (X, E) in enumerate(train_dataset):
        X, E  = X.to(device), E.to(device)
        optimizer.zero_grad()  # Clear gradients.
        pred = model(X)[:,0]  # Forward pass.
        pred = pred/(1-pred)
        weighted_measurements = torch.einsum('i,ik->k',pred,E)
        weighted_measurements_sqr = torch.einsum('i,ik->k',pred**2,E)
        loss = model.loss_function(measured_events,weighted_measurements,weighted_measurements_sqr,bin_lengths,len(pred))  # Loss computation.
        loss.backward()  # Backward pass.
        optimizer.step()  # Update model parameters.
        total_loss += loss.item()

    return total_loss / len(train_dataset.dataset)

def test(model, test_dataset,measured_events,bin_lengths):
    model.eval()
    total_loss = 0
    for batch, (X, E) in enumerate(test_dataset):
        X, E  = X.to(device), E.to(device)
        pred = model(X)[:,0]  # Forward pass.
        pred = pred/(1-pred)
        weighted_measurements = torch.einsum('i,ik->k',pred,E)
        weighted_measurements_sqr = torch.einsum('i,ik->k',pred**2,E)
        loss = model.loss_function(measured_events,weighted_measurements,weighted_measurements_sqr,bin_lengths,len(pred))  # Loss computation.
        total_loss += loss.item()

    return total_loss / len(test_dataset.dataset)


results_dir = str(sys.argv[1]) # should be dir/

sim_observables = np.load(str(sys.argv[2]))
measured_observables = np.load(str(sys.argv[3]))

X_event = np.vstack([sim_observables,measured_observables])
y_event = np.hstack([np.zeros(len(sim_observables)),np.ones(len(measured_observables))])

bins = [[] for _ in range(X_event.shape[1])]
bin_lengths = np.zeros(X_event.shape[1])

for nobs in range(X_event.shape[1]):
    bins[nobs]=np.linspace(0.95*np.min(X_event[:,nobs]),1.05*np.max(X_event[:,nobs]),10)
    #bins[nobs]=[np.quantile(X_event[:,nobs],i/10) for i in range(11)]
    #bins[nobs][0]*=0.95
    #bins[nobs][-1]*=1.05
    bin_lengths[nobs]=len(bins[nobs])

## Re-define bins for discrete multiplicities
bins[5]=np.arange(np.min(X_event[:,5])-.5,np.max(X_event[:,5])+1.5,1)
bin_lengths[5]=len(bins[5])
bins[6]=np.arange(np.min(X_event[:,6])-1,np.max(X_event[:,6])+3,2)
bin_lengths[6]=len(bins[6])

print(np.min(X_event[:,5]),np.max(X_event[:,5]),bins[5])
print(np.min(X_event[:,6]),np.max(X_event[:,6]),bins[6])

total_bins = int(np.sum(bin_lengths))
print(total_bins)
expression_matrix = np.zeros((len(X_event),total_bins))
## this is with torch einsum

for nevent in range(len(X_event)):
    for nobs in range(X_event.shape[1]):
        expressed_bin = int(np.sum(bin_lengths[:nobs])+bin_assigner(bins[nobs],X_event[nevent,nobs]))
        expression_matrix[nevent,expressed_bin]=1

event_counts_measured = np.dot((np.sum(y_event==0)/np.sum(y_event==1))*np.ones(int(np.sum(y_event==1))),expression_matrix[y_event==1])
print(event_counts_measured.shape,np.sum(y_event==0),np.sum(y_event==1),np.sum(event_counts_measured)/(X_event.shape[1]))


event_counts_measured_torch = torch.tensor(event_counts_measured).to(torch.float)
bin_lengths_torch = torch.tensor(bin_lengths).to(torch.float)

scaler = StandardScaler()
sim_events_torch = torch.tensor(scaler.fit_transform(X_event[y_event==0])).to(torch.float)
sim_measurements_torch = torch.tensor(expression_matrix[y_event==0]).to(torch.float)

batch_size=10000
sim_dataset = DataLoader(sim_Dataset(sim_events_torch,sim_measurements_torch),batch_size=batch_size)


try: nepochs = int(sys.argv[4])
except: nepochs = 10
try: learning_rate=float(sys.argv[5])
except: learning_rate = 0.01
try: npatience = int(sys.argv[6])
except: npatience = 5
try: model_path = results_dir+sys.argv[9]
except: model_path = results_dir+'default_model.pth'
print(nepochs,learning_rate,npatience)

layers_data=[(X_event.shape[1],nn.ReLU()),(int(2*X_event.shape[1]),nn.ReLU())]#,(X_event.shape[1],nn.ReLU())]#[(64,nn.ReLU()),(64,nn.ReLU()),(64,nn.ReLU())]
model = NeuralNetwork(dim_input=X_event.shape[1],dim_output=1,layers_data=layers_data)


try: model.load_state_dict(torch.load(model_path))
except: print("New model")
device = torch.device("cpu")#torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model.to(device)
event_counts_measured_torch.to(device)
bin_lengths_torch.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

val_loss_aux = 1000
times = 0
for epoch in range(1, nepochs):
    loss = train(model, optimizer,sim_dataset,event_counts_measured_torch,bin_lengths_torch)
    val_loss = test(model,sim_dataset,event_counts_measured_torch,bin_lengths_torch)
    print(f'Epoch: {epoch:02d}, Loss: {loss:.4f},Val Loss: {val_loss:.4f}, Learning Rate: {learning_rate:.6f}')
    if epoch == 0:
        val_loss_aux = val_loss
        times = 0
        continue

    if loss < val_loss_aux:
        val_loss_aux = val_loss
        times = 0
    else:
        times+=1

    if times == npatience:
        learning_rate*=0.1
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)    
    if times == 2*npatience:
        break  
    torch.save(model.state_dict(),model_path)


probas = model(torch.tensor(scaler.transform(X_event)).to(torch.float))[:,0].detach().cpu().numpy()

accuracy_xgboost = accuracy_score(y_event,np.round(probas))
auc_xgboost = roc_auc_score(y_event,probas)
mse_xgboost = np.mean((y_event-probas)**2)
print(accuracy_xgboost,auc_xgboost,mse_xgboost)

plt.hist(probas[y_event==0],histtype='step',bins=100,color='blue',label='Sim.');
plt.hist(probas[y_event==1],histtype='step',bins=100,color='red',label='Data');
plt.legend(loc='upper right')
plt.xlabel('Predicted probability')
plt.ylabel('Events')
plt.title('High Level features, AUC = '+str(round(auc_xgboost,3))+', Acc. = '+str(round(accuracy_xgboost,3)))
plt.tight_layout()
plt.savefig(results_dir+'probability_histogram.pdf')
plt.clf()

probs_true, probs_pred = calibration_curve(y_event,probas,n_bins=50)

plt.plot(probs_true, probs_pred)
plt.plot(probs_true, probs_true,'k')
plt.xlabel('True frequency')
plt.ylabel('Predicted frequency')
plt.title('High level features')
plt.tight_layout()
plt.savefig(results_dir+'calibration_curve_high_level_features.pdf')
plt.clf()

plt.hist(probas[y_event==0]/(1-probas[y_event==0]),histtype='step',bins=100,color='blue',density=True,label='Learned');
plt.axvline(1)
plt.yscale('log')
plt.xscale('log')
plt.xlabel('w')
plt.ylabel('Density')
plt.title('High level features')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'classifier_high_level_features.pdf')
plt.clf()

plt.hist(np.log(probas[y_event==0]/(1-probas[y_event==0])),histtype='step',bins=100,color='blue',density=True,label='Learned');
# plt.hist(probas[y_event==1]/(1-probas[y_event==1]),histtype='step',bins=100,color='red');
plt.axvline(0)
plt.yscale('log')
plt.xlabel('Log w')
plt.ylabel('Density')
plt.title('High level features')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'log_classifier_high_level_features.pdf')
plt.clf()

weights_per_event_classifier_xgboost_high_level_features = probas[y_event==0]/(1.0-probas[y_event==0])
print("Average weight")
print(np.mean(weights_per_event_classifier_xgboost_high_level_features))
print("Effective number of events")
print(len(weights_per_event_classifier_xgboost_high_level_features),(np.sum(weights_per_event_classifier_xgboost_high_level_features)**2)/(np.sum(weights_per_event_classifier_xgboost_high_level_features**2)))


np.save(results_dir+'learned_event_weights_first_step_train.npy',weights_per_event_classifier_xgboost_high_level_features)
np.save(results_dir+'learned_event_weights_first_step_train_data.npy',probas[y_event==1]/(1.0-probas[y_event==1]))

obs_names=[str(i) for i in range(X_event.shape[1])]#['1-T','C','D','$B_{W}$','$B_{T}$','$n_{final}$','$n_{ch.}$','$<|Ln x_{p}|>$','$<(|Ln x_{p}|-<|Ln x_{p}|>)^2>$','$<(|Ln x_{p}|-<|Ln x_{p}|>)^3>$','$<|Ln x_{p}|>$','$<(|Ln x_{p}|-<|Ln x_{p}|>)^2>$','$<(|Ln x_{p}|-<|Ln x_{p}|>)^3>$']
for _ in range(X_event.shape[1]):
    if _ == 5:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=np.arange(0.5,57.5,1.0),histtype='step',color='blue',label='Simulation')
    elif _ == 6:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=np.arange(1.0,57.0,2.0),histtype='step',color='blue',label='Simulation')
    elif _ == 0:
        a,b,c = plt.hist(1-X_event[y_event==0,_],bins=100,histtype='step',color='blue',label='Simulation')
    else:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=100,histtype='step',color='blue',label='Simulation')
    if _ == 0:
        plt.hist(1.0-X_event[y_event==1,_],bins=b,histtype='step',color='red',label='Data')
        plt.hist(1.0-X_event[y_event==0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')
    else:
        plt.hist(X_event[y_event==1,_],bins=b,histtype='step',color='red',label='Data')
        plt.hist(X_event[y_event==0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')

    plt.xlabel(obs_names[_])
    plt.ylabel('Events')
    plt.legend(loc='upper right')
    plt.tight_layout()
    plt.savefig(results_dir+'high_level_obs_'+str(_)+'.pdf')
    plt.clf()
    #'''
    if _ == 5:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=np.arange(0.5,57.5,1.0),histtype='step',color='blue',label='Simulation')
    elif _ == 6:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=np.arange(1.0,57.0,2.0),histtype='step',color='blue',label='Simulation')
    elif _ == 0:
        a,b,c = plt.hist(1-X_event[y_event==0,_],bins=100,histtype='step',color='blue',label='Simulation')
    else:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=100,histtype='step',color='blue',label='Simulation')
    if _ == 0:
        plt.hist(1.0-X_event[y_event==1,_],bins=b,histtype='step',color='red',label='Data')
        plt.hist(1.0-X_event[y_event==0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')
    else:
        plt.hist(X_event[y_event==1,_],bins=b,histtype='step',color='red',label='Data')
        plt.hist(X_event[y_event==0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')

    plt.xlabel(obs_names[_])
    plt.ylabel('Events')
    plt.legend(loc='upper right')
    plt.yscale('log')
    plt.tight_layout()
    plt.savefig(results_dir+'high_level_obs_'+str(_)+'_log_scale.pdf')
    plt.clf()
    #'''


X_event_test = np.load(str(sys.argv[7]))#[:11000]
probas_test = model(torch.tensor(scaler.transform(X_event_test)).to(torch.float))[:,0].detach().cpu().numpy()

X_meas_event_test = np.load(str(sys.argv[8]))#[:11000]
probas_meas_test = model(torch.tensor(scaler.transform(X_meas_event_test)).to(torch.float))[:,0].detach().cpu().numpy()#lr.predict_proba(scaler.transform(X_meas_event_test))[:,1]


plt.hist(probas[y_event==0],histtype='step',bins=100,color='blue',label='Sim.');
plt.hist(probas[y_event==1],histtype='step',bins=100,color='red',label='Data');
plt.hist(probas_test,histtype='step',bins=100,color='violet',label='Sim. test');
plt.hist(probas_meas_test,histtype='step',bins=100,color='brown',label='Data test');
plt.legend(loc='upper right')
plt.xlabel('Predicted probability')
plt.ylabel('Events')
plt.title('High Level features, AUC = '+str(round(auc_xgboost,3))+', Acc. = '+str(round(accuracy_xgboost,3)))
plt.tight_layout()
plt.savefig(results_dir+'probability_histogram_with_test.pdf')
plt.clf()

a,b,c = plt.hist(probas[y_event==0]/(1-probas[y_event==0]),histtype='step',bins=100,color='blue',density=True,label='Learned');
plt.hist(probas_test/(1-probas_test),histtype='step',bins=b,color='violet',density=True,label='Test');
plt.axvline(1)
plt.yscale('log')
plt.xscale('log')
plt.xlabel('w')
plt.ylabel('Density')
plt.title('High level features')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'classifier_high_level_features_with_test.pdf')
plt.clf()

a,b,c = plt.hist(np.log(probas[y_event==0]/(1-probas[y_event==0])),histtype='step',bins=100,color='blue',density=True,label='Learned');
plt.hist(np.log(probas_test/(1-probas_test)),histtype='step',bins=b,color='violet',density=True,label='Test');
plt.axvline(0)
plt.yscale('log')
plt.xlabel('Log w')
plt.ylabel('Density')
plt.title('High level features')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'log_classifier_high_level_features_with_test.pdf')
plt.clf()

weights_test = probas_test/(1.0-probas_test)
print("Average weight")
print(np.mean(weights_test))

np.save(results_dir+'learned_event_weights_first_step_test.npy',weights_test)

weights_meas_test = probas_meas_test/(1.0-probas_meas_test)
print("Average weight")
print(np.mean(weights_meas_test))

np.save(results_dir+'learned_event_weights_first_step_test_data.npy',weights_meas_test)
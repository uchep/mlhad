import os
#os.environ['OMP_NUM_THREADS'] = "24"

import numpy as np
import sys
import scipy.stats as st
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'size'   : 25}
rc('font', **font)
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
rc('text', usetex=True)
plt.rcParams['font.family']='Computer Modern'


from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import cross_val_predict, train_test_split, KFold

from sklearn.metrics import accuracy_score, precision_score, roc_auc_score
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.calibration import calibration_curve,CalibratedClassifierCV

import torch
torch.set_num_threads(24)

from torch import nn
from tqdm import tqdm
from torch.nn.modules import Module

from torch_geometric.data import Data
from torch_geometric.loader import DataLoader

from torch_geometric.utils import add_self_loops, degree
from torch.nn import Sequential as Seq, Linear, ReLU, Parameter,LeakyReLU
from torch_geometric.nn import MessagePassing, global_max_pool, global_add_pool
from torch_cluster import knn_graph

###############################################################################
def bin_assigner(bins,val):

    # This function simply outputs the bin assignment of a given value val for a given choice of binning bins

    number_of_bins = len(bins)
    bin_number=0
    if val >= bins[-1]:
        bin_number = number_of_bins - 1
    if val < bins[0]:
        bin_number = 0
    else:
        for bin_number_aux in range(number_of_bins-1):
            if bins[bin_number_aux]<= val < bins[bin_number_aux+1]:
                bin_number = bin_number_aux
    return bin_number
###############################################################################

### general framework for training and testing
def train(model, optimizer, loader,criterion,len_dataset):
    model.train()
    #device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    #print(device)
    
    total_loss = 0
    for data in loader:
        data = data.to(device)
        optimizer.zero_grad()  # Clear gradients.
        max_histories_per_event = torch.tensor([torch.max(data.x[data.batch==event_index][:,12]) for event_index in range(len(data.y))])
        total_accepted_fragmentations_per_event = torch.hstack([torch.where(data.x[data.batch==event_index][:,12]==max_histories_per_event[event_index],1,0) for event_index in range(len(data.y))])

        pred = model(data.x[torch.where(total_accepted_fragmentations_per_event)], data.edge_index,data.batch[torch.where(total_accepted_fragmentations_per_event)])[:,0]  # Forward pass.
        loss = criterion(pred, data.y)  # Loss computation.
        loss.backward()  # Backward pass.
        ### let's add a penalty term that enforces proper normalization
        #internal_weights = model.internal_representation(data.x, data.edge_index,data.batch)
        #loss = criterion(pred, data.y) + 0/(100)*(torch.pow(torch.mean(internal_weights)-1.0,2)+torch.sum(internal_weights**2)-torch.sum(internal_weights)**2/torch.sum(torch.ones(internal_weights.shape))) # Loss computation.
        #loss.backward()  # Backward pass. 
        optimizer.step()  # Update model parameters.
        total_loss += loss.item() * data.num_graphs

    return total_loss / len_dataset

def test(model,loader,len_dataset):
    model.eval()
    #device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    #print(device)
    predictions = np.zeros(len_dataset)
    values = np.zeros(len_dataset)
    initial_index = 0
    for ndata, data in enumerate(loader):
        data = data.to(device)
        max_histories_per_event = torch.tensor([torch.max(data.x[data.batch==event_index][:,12]) for event_index in range(len(data.y))])
        total_accepted_fragmentations_per_event = torch.hstack([torch.where(data.x[data.batch==event_index][:,12]==max_histories_per_event[event_index],1,0) for event_index in range(len(data.y))])

        pred = model(data.x[torch.where(total_accepted_fragmentations_per_event)], data.edge_index,data.batch[torch.where(total_accepted_fragmentations_per_event)])[:,0].detach().cpu().numpy()  # Forward pass.
    #     print(pred)
        predictions[initial_index:initial_index+len(pred)]=pred
        values[initial_index:initial_index+len(pred)]=data.y.detach().cpu().numpy()
        initial_index+=len(pred)
    return predictions, values


#'''
###  A model with no connections, just using z, pT, mTHad, pxOld, pyOld and no end points

# PointCloud with no connections classifier
class PointCloud(MessagePassing):
    def __init__(self, in_channels, inner_channels, out_channels):
        super().__init__(aggr='sum') #  "Sum" aggregation.
        self.mlp = Seq(Linear(in_channels, inner_channels),
                       ReLU(),
                       Linear(inner_channels, inner_channels),
                       ReLU(),
                       Linear(inner_channels, inner_channels),
                       ReLU(),
                       #Linear(inner_channels, inner_channels),
                       #ReLU(),                                              
                       Linear(inner_channels, out_channels))
        

    def forward(self, x, edge_index):
        # x has shape [N, in_channels]
        # edge_index has shape [2, E]
        edge_index, _ = add_self_loops(edge_index, num_nodes=x.size(0))
        #print(edge_index)
        out = self.propagate(edge_index, x=x)
        #print(out.shape)

        return out

    def message(self, x_i,x_j):
#         print(x_i.shape)
        # x_i has shape [E, in_channels]

        tmp = torch.cat([x_i, x_j - x_i], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.mlp(tmp)


class PointNetLayer(MessagePassing):
    def __init__(self, in_channels, out_channels):
        # Message passing with "max" aggregation.
        super().__init__(aggr='sum')

        # Initialization of the MLP:
        # Here, the number of input features correspond to the hidden node
        # dimensionality plus point dimensionality (=3).
        self.mlp = Seq(Linear(in_channels + 4, out_channels),
                              ReLU(),
                              Linear(out_channels, out_channels),
                              ReLU(),
                              Linear(out_channels, out_channels))

    def forward(self, h, pos, edge_index):
        # Start propagating messages.
        return self.propagate(edge_index, h=h, pos=pos)

    def message(self, h_j, pos_j, pos_i):
        # h_j defines the features of neighboring nodes as shape [num_edges, in_channels]
        # pos_j defines the position of neighboring nodes as shape [num_edges, 3]
        # pos_i defines the position of central nodes as shape [num_edges, 3]
        #print(h_j.shape,pos_j.shape,pos_i.shape)
        input = pos_j - pos_i  # Compute spatial relation.
        #print(h_j,pos_j,pos_i)
        if h_j is not None:
            # In the first layer, we may not have any hidden node features,
            # so we only combine them in case they are present.
            input = torch.cat([h_j, input], dim=-1)

        return self.mlp(input)  # Apply our final MLP.

# Particle Net

class PointNet(torch.nn.Module):
    def __init__(self,in_channels,inner_dimension):
        super().__init__()

        torch.manual_seed(12345)
        #self.mp = PointCloud(len([8,9,10,11]), inner_dimension,inner_dimension)
        #self.mp1 = PointCloud(inner_dimension, inner_dimension,inner_dimension)
        #self.mp2 = PointCloud(inner_dimension, inner_dimension,1)
        self.mp1 = PointNetLayer(len([8,9,10,11]), inner_dimension)
        self.mp2 = PointNetLayer(inner_dimension, inner_dimension)
        self.classifier = nn.Sequential(
            nn.Linear(inner_dimension, inner_dimension),
            nn.ReLU(),
            #nn.Linear(inner_dimension, inner_dimension),
            #nn.ReLU(),
            nn.Linear(inner_dimension, 1),
            nn.Sigmoid())
        
    def forward(self, x, edge_index,batch):
        # Compute the kNN graph:
        # Here, we need to pass the batch vector to the function call in order
        # to prevent creating edges between points of different examples.
        # We also add `loop=True` which will add self-loops to the graph in
        # order to preserve central point information.
        edge_index = knn_graph(x[:,[8,9,10,11]], k=8, batch=batch, loop=True)
        #edge_index, _ = add_self_loops(edge_index, num_nodes=x.size(0))
        # 3. Start message passing.
        #h = self.mp(x[:,[8,9,10,11]], edge_index=edge_index)
        
        h = self.mp1(h=x[:,[8,9,10,11]],pos=x[:,[8,9,10,11]], edge_index=edge_index)
        h = self.mp2(h=h,pos=x[:,[8,9,10,11]], edge_index=edge_index)
        #print(h.shape)
        #h = self.mp2(h, edge_index=edge_index)
        #print(h.shape)
#         print(x[:,-1].shape)
        # 4. Global Pooling.
        h = global_add_pool(h, batch)  # [num_examples, hidden_channels]
        h = self.classifier(h)#torch.exp(h)
        #h = h/(1+h)
        #h = torch.sigmoid(h)
        #print(h.shape)
        return h
    def internal_representation(self,x,edge_index,batch):
        #h = self.mp(x[:,[8,9,10,11]], edge_index=edge_index)
        edge_index = knn_graph(x[:,[8,9,10,11]], k=16, batch=batch, loop=True)
        h = self.mp1(h=x[:,[8,9,10,11]],pos=x[:,[8,9,10,11]], edge_index=edge_index)
        h = self.mp2(h=h,pos=x[:,[8,9,10,11]], edge_index=edge_index)
        return h
    
###############################################################################
results_dir = str(sys.argv[1])
X_event_sim = np.load(sys.argv[2])
X_event_meas = np.load(sys.argv[3])
history_indexes_sim = np.load(sys.argv[4])
history_indexes_meas = np.load(sys.argv[5])
print(X_event_sim.shape,X_event_meas.shape,history_indexes_sim.shape,history_indexes_meas.shape)
X_event_sim = np.concatenate((X_event_sim,history_indexes_sim.reshape((X_event_sim.shape[0],X_event_sim.shape[1],1))),axis=2)
X_event_meas = np.concatenate((X_event_meas,history_indexes_meas.reshape((X_event_meas.shape[0],X_event_meas.shape[1],1))),axis=2)

print(X_event_sim.shape,X_event_meas.shape)



#print(X_event.shape,len(X_event))
Nevents=int(np.min([float(sys.argv[6]),len(X_event_sim),len(X_event_meas)]))
X_event_sim=X_event_sim[:Nevents]
X_event_meas=X_event_meas[:Nevents]
#y_event = np.where(np.log(y_event)>5.0,y_event,np.exp(5.0))
#y_event = np.where(np.log(y_event)<-5.0,y_event,np.exp(-5.0))
print(X_event_sim.shape,X_event_meas.shape)

### Hadronization scaling
X_event_sim[:,:,3]=X_event_sim[:,:,3]/(0.140)
#X_event[:,:,3]=X_event[:,3]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event_sim[:,:,4]=np.where(X_event_sim[:,:,4]==0,-1,X_event_sim[:,:,4])
#X_event[:,:,4]=X_event[:,:,4]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event_sim[:,:,7]=X_event_sim[:,:,7]/5.0
### Four momenta scaling

X_event_sim[:,:,5]=X_event_sim[:,:,5]/1.0
X_event_sim[:,:,6]=X_event_sim[:,:,6]/1.0

X_event_sim[:,:,9]=X_event_sim[:,:,9]/1.0
X_event_sim[:,:,10]=X_event_sim[:,:,10]/1.0
X_event_sim[:,:,11]=X_event_sim[:,:,11]/25.0
X_event_sim[:,:,12]=X_event_sim[:,:,12]/25.0

### Hadronization scaling
X_event_meas[:,:,3]=X_event_meas[:,:,3]/(0.140)
#X_event[:,:,3]=X_event[:,3]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event_meas[:,:,4]=np.where(X_event_meas[:,:,4]==0,-1,X_event_meas[:,:,4])
#X_event[:,:,4]=X_event[:,:,4]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event_meas[:,:,7]=X_event_meas[:,:,7]/5.0
### Four momenta scaling

X_event_meas[:,:,5]=X_event_meas[:,:,5]/1.0
X_event_meas[:,:,6]=X_event_meas[:,:,6]/1.0

X_event_meas[:,:,9]=X_event_meas[:,:,9]/1.0
X_event_meas[:,:,10]=X_event_meas[:,:,10]/1.0
X_event_meas[:,:,11]=X_event_meas[:,:,11]/25.0
X_event_meas[:,:,12]=X_event_meas[:,:,12]/25.0


try: nepochs = int(sys.argv[7])
except: nepochs = 10
try: learning_rate=float(sys.argv[8])
except: learning_rate = 0.01
try: npatience = int(sys.argv[9])
except: npatience = 5
try: model_path = results_dir+str(sys.argv[14])
except: model_path = results_dir+'default_model_first_step_point_cloud.pth'
print(Nevents,nepochs,learning_rate,npatience)

device = torch.device("cpu")#cuda:0" if torch.cuda.is_available() else "cpu")
print(device)


dataset = []
multiplicities=np.zeros(2*Nevents)
stringends = True
for n in range(Nevents):
    if stringends == False:
        if np.sum(X_event_sim[n,:int(np.sum(X_event_sim[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        multiplicities[n]=np.sum([a and b for a,b in zip(X_event_sim[n,:,12]!=0.0,X_event_sim[n,:,8]==0.0)])
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)   
        hadrons = X_event_sim[n,:int(np.sum(X_event_sim[n,:,12]!=0.0))]
        hadrons = hadrons[X_event_sim[n,:int(np.sum(X_event_sim[n,:,12]!=0.0)),8]==0.0]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    else:
        if np.sum(X_event_sim[n,:,12]!=0.0) == 0.0:
            continue
        multiplicities[n]=np.sum(X_event_sim[n,:,12]!=0.0)
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)    
        hadrons = X_event_sim[n,:int(np.sum(X_event_sim[n,:,12]!=0.0))]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    #print(x.shape)
    y = torch.tensor(0,dtype=torch.float)

    data = Data(x=x, y=y,edge_index=edge_index)
    dataset.append(data)

for n in range(Nevents):
    if stringends == False:
        if np.sum(X_event_meas[n,:int(np.sum(X_event_meas[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        multiplicities[Nevents+n]=np.sum([a and b for a,b in zip(X_event_meas[n,:,12]!=0.0,X_event_meas[n,:,8]==0.0)])
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)   
        hadrons = X_event_meas[n,:int(np.sum(X_event_meas[n,:,12]!=0.0))]
        hadrons = hadrons[X_event_meas[n,:int(np.sum(X_event_meas[n,:,12]!=0.0)),8]==0.0]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    else:
        if np.sum(X_event_meas[n,:,12]!=0.0) == 0.0:
            continue
        multiplicities[Nevents+n]=np.sum(X_event_meas[n,:,12]!=0.0)
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)    
        hadrons = X_event_meas[n,:int(np.sum(X_event_meas[n,:,12]!=0.0))]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    #print(x.shape)
    y = torch.tensor(1,dtype=torch.float)

    data = Data(x=x, y=y,edge_index=edge_index)
    dataset.append(data)
print(len(dataset),len(X_event_sim)+len(X_event_meas),2*Nevents)

### training
batch_size=10000
loader = DataLoader(dataset,batch_size=batch_size, shuffle=True)
model = PointNet(X_event_sim.shape[2]-1,64)
try: model.load_state_dict(torch.load(model_path))
except: print("New model")

model.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
criterion = torch.nn.BCELoss()  # Define loss criterion.

#'''

train_loss_aux = 1000
times = 0
for epoch in range(1, nepochs):
    loss = train(model, optimizer, loader,criterion,2*Nevents)
    print(f'Epoch: {epoch:02d}, Loss: {loss:.4f}, Learning Rate: {learning_rate:.6f}')
    if epoch == 0:
        train_loss_aux = loss
        times = 0
        continue
    if loss < train_loss_aux:
        train_loss_aux = loss
        times = 0
    else:
        times+=1

    if times == npatience:
        learning_rate*=0.1
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)    
    if times == 2*npatience:
        break  
    torch.save(model.state_dict(),model_path)  


dataset = []
for n in range(len(X_event_sim)):
    if stringends == False:
        if np.sum(X_event_sim[n,:int(np.sum(X_event_sim[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)   
        hadrons = X_event_sim[n,:int(np.sum(X_event_sim[n,:,12]!=0.0))]
        hadrons = hadrons[X_event_sim[n,:int(np.sum(X_event_sim[n,:,12]!=0.0)),8]==0.0]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    else:
        if np.sum(X_event_sim[n,:,12]!=0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)    
        hadrons = X_event_sim[n,:int(np.sum(X_event_sim[n,:,12]!=0.0))]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    #print(x.shape)
    y = torch.tensor(0,dtype=torch.float)

    data = Data(x=x, y=y,edge_index=edge_index)
    dataset.append(data)

for n in range(len(X_event_meas)):
    if stringends == False:
        if np.sum(X_event_meas[n,:int(np.sum(X_event_meas[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)   
        hadrons = X_event_meas[n,:int(np.sum(X_event_meas[n,:,12]!=0.0))]
        hadrons = hadrons[X_event_meas[n,:int(np.sum(X_event_meas[n,:,12]!=0.0)),8]==0.0]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    else:
        if np.sum(X_event_meas[n,:,12]!=0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)    
        hadrons = X_event_meas[n,:int(np.sum(X_event_meas[n,:,12]!=0.0))]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    #print(x.shape)
    y = torch.tensor(1,dtype=torch.float)

    data = Data(x=x, y=y,edge_index=edge_index)
    dataset.append(data)
print(len(dataset),len(X_event_sim)+len(X_event_meas),2*Nevents)

loader_eval = DataLoader(dataset,batch_size=batch_size, shuffle=False)
#[int(np.min([Nevents,len(X_event_sim)]))]
probas, y_event = test(model,loader_eval,len(X_event_sim)+len(X_event_meas))

print(np.min(probas),np.max(probas))
probas = np.where(probas<1e-5,1e-5,probas)
probas = np.where(probas>1-1e-5,1-1e-5,probas)

accuracy_xgboost = accuracy_score(y_event,np.round(probas))
auc_xgboost = roc_auc_score(y_event,probas)
mse_xgboost = np.mean((y_event-probas)**2)
print(accuracy_xgboost,auc_xgboost,mse_xgboost)

plt.hist(probas[y_event==0],histtype='step',bins=100,color='blue',label='Sim.');
plt.hist(probas[y_event==1],histtype='step',bins=100,color='red',label='Data');
plt.legend(loc='upper right')
plt.xlabel('Predicted probability')
plt.ylabel('Events')
plt.title('Point Cloud, AUC = '+str(round(auc_xgboost,3))+', Acc. = '+str(round(accuracy_xgboost,3)))
plt.tight_layout()
plt.savefig(results_dir+'probability_histogram.pdf')
plt.clf()

probs_true, probs_pred = calibration_curve(y_event,probas,n_bins=50)

plt.plot(probs_true, probs_pred)
plt.plot(probs_true, probs_true,'k')
plt.xlabel('True frequency')
plt.ylabel('Predicted frequency')
plt.title('Point Cloud')
plt.tight_layout()
plt.savefig(results_dir+'calibration_curve_point_cloud.pdf')
plt.clf()

plt.hist(probas[y_event==0]/(1-probas[y_event==0]),histtype='step',bins=100,color='blue',density=True,label='Learned');
plt.axvline(1)
plt.yscale('log')
plt.xscale('log')
plt.xlabel('w')
plt.ylabel('Density')
plt.title('Point Cloud')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'classifier_point_cloud.pdf')
plt.clf()

plt.hist(np.log(probas[y_event==0]/(1-probas[y_event==0])),histtype='step',bins=100,color='blue',density=True,label='Learned');
# plt.hist(probas[y_event==1]/(1-probas[y_event==1]),histtype='step',bins=100,color='red');
plt.axvline(0)
plt.yscale('log')
plt.xlabel('Log w')
plt.ylabel('Density')
plt.title('Point Cloud')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'log_classifier_point_cloud.pdf')
plt.clf()

weights_per_event_classifier_xgboost_high_level_features = probas[y_event==0]/(1.0-probas[y_event==0])
print("Average weight")
print(np.mean(weights_per_event_classifier_xgboost_high_level_features))
print("Effective number of events")
print(len(weights_per_event_classifier_xgboost_high_level_features),(np.sum(weights_per_event_classifier_xgboost_high_level_features)**2)/(np.sum(weights_per_event_classifier_xgboost_high_level_features**2)))
print(X_event_sim.shape,weights_per_event_classifier_xgboost_high_level_features.shape)

np.save(results_dir+'learned_event_weights_first_step_train.npy',weights_per_event_classifier_xgboost_high_level_features)#/np.mean(weights_per_event_classifier_xgboost_high_level_features))
np.save(results_dir+'learned_event_weights_first_step_train_data.npy',probas[y_event==1]/(1.0-probas[y_event==1]))#/np.mean(weights_per_event_classifier_xgboost_high_level_features))



obs_names=[str(i) for i in range(X_event_sim.shape[2])]#['1-T','C','D','$B_{W}$','$B_{T}$','$n_{final}$','$n_{ch.}$','$<|Ln x_{p}|>$','$<(|Ln x_{p}|-<|Ln x_{p}|>)^2>$','$<(|Ln x_{p}|-<|Ln x_{p}|>)^3>$','$<|Ln x_{p}|>$','$<(|Ln x_{p}|-<|Ln x_{p}|>)^2>$','$<(|Ln x_{p}|-<|Ln x_{p}|>)^3>$']
for _ in [0,1,2,9,10,11,12]:#len(obs_names)):
    a,b,c = plt.hist(X_event_sim[:,0,_],bins=100,histtype='step',color='blue',label='Simulation')
    plt.hist(X_event_meas[:,0,_],bins=b,histtype='step',color='red',label='Data')
    plt.hist(X_event_sim[:,0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')

    plt.xlabel(obs_names[_])
    plt.ylabel('Events')
    plt.legend(loc='upper right')
    plt.tight_layout()
    plt.savefig(results_dir+'first_emission_obs_'+str(_)+'.pdf')
    plt.clf()
    #'''
    a,b,c = plt.hist(X_event_sim[:,0,_],bins=100,histtype='step',color='blue',label='Simulation')
    plt.hist(X_event_meas[:,0,_],bins=b,histtype='step',color='red',label='Data')
    plt.hist(X_event_sim[:,0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')

    plt.xlabel(obs_names[_])
    plt.ylabel('Events')
    plt.legend(loc='upper right')
    plt.yscale('log')
    plt.tight_layout()
    plt.savefig(results_dir+'first_emission_obs_'+str(_)+'_log_scale.pdf')
    plt.clf()

    a,b,c = plt.hist(X_event_sim[:,1,_],bins=100,histtype='step',color='blue',label='Simulation')
    plt.hist(X_event_meas[:,1,_],bins=b,histtype='step',color='red',label='Data')
    plt.hist(X_event_sim[:,1,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')

    plt.xlabel(obs_names[_])
    plt.ylabel('Events')
    plt.legend(loc='upper right')
    plt.tight_layout()
    plt.savefig(results_dir+'second_emission_obs_'+str(_)+'.pdf')
    plt.clf()
    #'''
    a,b,c = plt.hist(X_event_sim[:,1,_],bins=100,histtype='step',color='blue',label='Simulation')
    plt.hist(X_event_meas[:,1,_],bins=b,histtype='step',color='red',label='Data')
    plt.hist(X_event_sim[:,1,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')

    plt.xlabel(obs_names[_])
    plt.ylabel('Events')
    plt.legend(loc='upper right')
    plt.yscale('log')
    plt.tight_layout()
    plt.savefig(results_dir+'second_emission_obs_'+str(_)+'_log_scale.pdf')
    plt.clf()
    #'''


print("Test set")

X_event_test = np.load(sys.argv[10])
history_indexes_test = np.load(sys.argv[12])
print(X_event_test.shape,history_indexes_test.shape)
X_event_test = np.concatenate((X_event_test,history_indexes_test.reshape((X_event_test.shape[0],X_event_test.shape[1],1))),axis=2)

print(X_event_test.shape)


### Hadronization scaling
X_event_test[:,:,3]=X_event_test[:,:,3]/(0.140)
X_event_test[:,:,4]=np.where(X_event_test[:,:,4]==0,-1,X_event_test[:,:,4])
X_event_test[:,:,7]=X_event_test[:,:,7]/5.0
### Four momenta scaling

X_event_test[:,:,5]=X_event_test[:,:,5]/1.0
X_event_test[:,:,6]=X_event_test[:,:,6]/1.0

X_event_test[:,:,9]=X_event_test[:,:,9]/1.0
X_event_test[:,:,10]=X_event_test[:,:,10]/1.0
X_event_test[:,:,11]=X_event_test[:,:,11]/25.0
X_event_test[:,:,12]=X_event_test[:,:,12]/25.0


dataset_test = []
for n in range(len(X_event_test)):
    if stringends == False:
        if np.sum(X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)   
        hadrons = X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0))]
        hadrons = hadrons[X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0)),8]==0.0]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    else:
        if np.sum(X_event_test[n,:,12]!=0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)    
        hadrons = X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0))]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    #print(x.shape)
    y = torch.tensor(0,dtype=torch.float)

    data = Data(x=x, y=y,edge_index=edge_index)
    dataset_test.append(data)

### training
loader_test = DataLoader(dataset_test,batch_size=batch_size, shuffle=False)

probas_test, y_event_test = test(model,loader_test,len(X_event_test))

print(np.min(probas_test),np.max(probas_test))
probas_test = np.where(probas_test<1e-5,1e-5,probas_test)
probas_test = np.where(probas_test>1-1e-5,1-1e-5,probas_test)

weights_test = probas_test/(1.0-probas_test)
print("Average weight")
print(np.mean(weights_test))

np.save(results_dir+'learned_event_weights_first_step_test.npy',weights_test)


X_event_test = np.load(sys.argv[11])
history_indexes_test = np.load(sys.argv[13])
print(X_event_test.shape,history_indexes_test.shape)
X_event_test = np.concatenate((X_event_test,history_indexes_test.reshape((X_event_test.shape[0],X_event_test.shape[1],1))),axis=2)

print(X_event_test.shape)


### Hadronization scaling
X_event_test[:,:,3]=X_event_test[:,:,3]/(0.140)
X_event_test[:,:,4]=np.where(X_event_test[:,:,4]==0,-1,X_event_test[:,:,4])
X_event_test[:,:,7]=X_event_test[:,:,7]/5.0
### Four momenta scaling

X_event_test[:,:,5]=X_event_test[:,:,5]/1.0
X_event_test[:,:,6]=X_event_test[:,:,6]/1.0

X_event_test[:,:,9]=X_event_test[:,:,9]/1.0
X_event_test[:,:,10]=X_event_test[:,:,10]/1.0
X_event_test[:,:,11]=X_event_test[:,:,11]/25.0
X_event_test[:,:,12]=X_event_test[:,:,12]/25.0


dataset_test = []
for n in range(len(X_event_test)):
    if stringends == False:
        if np.sum(X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)   
        hadrons = X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0))]
        hadrons = hadrons[X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0)),8]==0.0]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    else:
        if np.sum(X_event_test[n,:,12]!=0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)    
        hadrons = X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0))]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    #print(x.shape)
    y = torch.tensor(0,dtype=torch.float)

    data = Data(x=x, y=y,edge_index=edge_index)
    dataset_test.append(data)

### training
loader_test = DataLoader(dataset_test,batch_size=batch_size, shuffle=False)

probas_test, y_event_test = test(model,loader_test,len(X_event_test))

print(np.min(probas_test),np.max(probas_test))
probas_test = np.where(probas_test<1e-5,1e-5,probas_test)
probas_test = np.where(probas_test>1-1e-5,1-1e-5,probas_test)

weights_test = probas_test/(1.0-probas_test)
print("Average weight")
print(np.mean(weights_test))

np.save(results_dir+'learned_event_weights_first_step_test_data.npy',weights_test)

import os
os.environ['OMP_NUM_THREADS'] = "24"

import numpy as np
import sys
import scipy.stats as st
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'size'   : 25}
rc('font', **font)
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
rc('text', usetex=True)
plt.rcParams['font.family']='Computer Modern'



from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import cross_val_predict, train_test_split, KFold

from sklearn.metrics import accuracy_score, precision_score, roc_auc_score
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.calibration import calibration_curve,CalibratedClassifierCV

from xgboost import XGBClassifier, XGBRegressor

###############################################################################
def bin_assigner(bins,val):

    # This function simply outputs the bin assignment of a given value val for a given choice of binning bins

    number_of_bins = len(bins)
    bin_number=0
    if val >= bins[-1]:
        bin_number = number_of_bins - 1
    if val < bins[0]:
        bin_number = 0
    else:
        for bin_number_aux in range(number_of_bins-1):
            if bins[bin_number_aux]<= val < bins[bin_number_aux+1]:
                bin_number = bin_number_aux
    return bin_number
###############################################################################

results_dir = str(sys.argv[1]) # should be dir/

sim_observables = np.load(str(sys.argv[2]))
measured_observables = np.load(str(sys.argv[3]))

X_event = np.vstack([sim_observables,measured_observables])
y_event = np.hstack([np.zeros(len(sim_observables)),np.ones(len(measured_observables))])
weights = np.ones(len(y_event))
weights[y_event==0] = np.sum(y_event==1)/np.sum(y_event==0)
print(X_event.shape,y_event.shape,np.sum(weights)/(2*len(measured_observables)))


parameters = {'reg_lambda':0,'learning_rate':1,'max_depth' : 10,'min_child_weight':1000,'colsample_bytree':0.5, 'colsample_bylevel':0.5, 'colsample_bynode':0.5}

lr = XGBClassifier(**parameters)
scaler = StandardScaler()
try:
    lr.load_model(results_dir+'classifier.json')
    scaler.fit(X_event)
except:
    lr.fit(scaler.fit_transform(X_event),y_event,sample_weight=weights)
    lr.save_model(results_dir+'classifier.json')
probas = lr.predict_proba(scaler.transform(X_event))[:,1]

accuracy_xgboost = accuracy_score(y_event,np.round(probas))
auc_xgboost = roc_auc_score(y_event,probas)
mse_xgboost = np.mean((y_event-probas)**2)
print(accuracy_xgboost,auc_xgboost,mse_xgboost)

plt.hist(probas[y_event==0],histtype='step',bins=100,color='blue',label='Sim.');
plt.hist(probas[y_event==1],histtype='step',bins=100,color='red',label='Data');
plt.legend(loc='upper right')
plt.xlabel('Predicted probability')
plt.ylabel('Events')
plt.title('High Level features, AUC = '+str(round(auc_xgboost,3))+', Acc. = '+str(round(accuracy_xgboost,3)))
plt.tight_layout()
plt.savefig(results_dir+'probability_histogram.pdf')
plt.clf()

probs_true, probs_pred = calibration_curve(y_event,probas,n_bins=50)

plt.plot(probs_true, probs_pred)
plt.plot(probs_true, probs_true,'k')
plt.xlabel('True frequency')
plt.ylabel('Predicted frequency')
plt.title('High level features')
plt.tight_layout()
plt.savefig(results_dir+'calibration_curve_high_level_features.pdf')
plt.clf()

plt.hist(probas[y_event==0]/(1-probas[y_event==0]),histtype='step',bins=100,color='blue',density=True,label='Learned');
plt.axvline(1)
plt.yscale('log')
plt.xscale('log')
plt.xlabel('w')
plt.ylabel('Density')
plt.title('High level features')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'classifier_high_level_features.pdf')
plt.clf()

plt.hist(np.log(probas[y_event==0]/(1-probas[y_event==0])),histtype='step',bins=100,color='blue',density=True,label='Learned');
# plt.hist(probas[y_event==1]/(1-probas[y_event==1]),histtype='step',bins=100,color='red');
plt.axvline(0)
plt.yscale('log')
plt.xlabel('Log w')
plt.ylabel('Density')
plt.title('High level features')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'log_classifier_high_level_features.pdf')
plt.clf()

weights_per_event_classifier_xgboost_high_level_features = probas[y_event==0]/(1.0-probas[y_event==0])
print("Average weight")
print(np.mean(weights_per_event_classifier_xgboost_high_level_features))
print("Effective number of events")
print(len(weights_per_event_classifier_xgboost_high_level_features),(np.sum(weights_per_event_classifier_xgboost_high_level_features)**2)/(np.sum(weights_per_event_classifier_xgboost_high_level_features**2)))


np.save(results_dir+'learned_event_weights_first_step_train.npy',weights_per_event_classifier_xgboost_high_level_features)
np.save(results_dir+'learned_event_weights_first_step_train_data.npy',probas[y_event==1]/(1.0-probas[y_event==1]))

obs_names=[str(i) for i in range(X_event.shape[1])]
for _ in range(X_event.shape[1]):
    if _ == 5:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=np.arange(0.5,57.5,1.0),histtype='step',color='blue',label='Simulation')
    elif _ == 6:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=np.arange(1.0,57.0,2.0),histtype='step',color='blue',label='Simulation')
    elif _ == 0:
        a,b,c = plt.hist(1-X_event[y_event==0,_],bins=100,histtype='step',color='blue',label='Simulation')
    else:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=100,histtype='step',color='blue',label='Simulation')
    if _ == 0:
        plt.hist(1.0-X_event[y_event==1,_],bins=b,histtype='step',color='red',label='Data')
        plt.hist(1.0-X_event[y_event==0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')
    else:
        plt.hist(X_event[y_event==1,_],bins=b,histtype='step',color='red',label='Data')
        plt.hist(X_event[y_event==0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')

    plt.xlabel(obs_names[_])
    plt.ylabel('Events')
    plt.legend(loc='upper right')
    plt.tight_layout()
    plt.savefig(results_dir+'high_level_obs_'+str(_)+'.pdf')
    plt.clf()
    #'''
    if _ == 5:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=np.arange(0.5,57.5,1.0),histtype='step',color='blue',label='Simulation')
    elif _ == 6:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=np.arange(1.0,57.0,2.0),histtype='step',color='blue',label='Simulation')
    elif _ == 0:
        a,b,c = plt.hist(1-X_event[y_event==0,_],bins=100,histtype='step',color='blue',label='Simulation')
    else:
        a,b,c = plt.hist(X_event[y_event==0,_],bins=100,histtype='step',color='blue',label='Simulation')
    if _ == 0:
        plt.hist(1.0-X_event[y_event==1,_],bins=b,histtype='step',color='red',label='Data')
        plt.hist(1.0-X_event[y_event==0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')
    else:
        plt.hist(X_event[y_event==1,_],bins=b,histtype='step',color='red',label='Data')
        plt.hist(X_event[y_event==0,_],bins=b,weights=weights_per_event_classifier_xgboost_high_level_features,histtype='step',color='magenta',label='Reweighted Sim.')

    plt.xlabel(obs_names[_])
    plt.ylabel('Events')
    plt.legend(loc='upper right')
    plt.yscale('log')
    plt.tight_layout()
    plt.savefig(results_dir+'high_level_obs_'+str(_)+'_log_scale.pdf')
    plt.clf()
    #'''


print("Test events")
X_event_test = np.load(str(sys.argv[4]))
probas_test = lr.predict_proba(scaler.transform(X_event_test))[:,1]

X_meas_event_test = np.load(str(sys.argv[5]))
probas_meas_test = lr.predict_proba(scaler.transform(X_meas_event_test))[:,1]


plt.hist(probas[y_event==0],histtype='step',bins=100,color='blue',label='Sim.');
plt.hist(probas[y_event==1],histtype='step',bins=100,color='red',label='Data');
plt.hist(probas_test,histtype='step',bins=100,color='violet',label='Sim. test');
plt.hist(probas_meas_test,histtype='step',bins=100,color='brown',label='Data test');
plt.legend(loc='upper right')
plt.xlabel('Predicted probability')
plt.ylabel('Events')
plt.title('High Level features, AUC = '+str(round(auc_xgboost,3))+', Acc. = '+str(round(accuracy_xgboost,3)))
plt.tight_layout()
plt.savefig(results_dir+'probability_histogram_with_test.pdf')
plt.clf()

a,b,c = plt.hist(probas[y_event==0]/(1-probas[y_event==0]),histtype='step',bins=100,color='blue',density=True,label='Learned');
plt.hist(probas_test/(1-probas_test),histtype='step',bins=b,color='violet',density=True,label='Test');
plt.axvline(1)
plt.yscale('log')
plt.xscale('log')
plt.xlabel('w')
plt.ylabel('Density')
plt.title('High level features')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'classifier_high_level_features_with_test.pdf')
plt.clf()

a,b,c = plt.hist(np.log(probas[y_event==0]/(1-probas[y_event==0])),histtype='step',bins=100,color='blue',density=True,label='Learned');
plt.hist(np.log(probas_test/(1-probas_test)),histtype='step',bins=b,color='violet',density=True,label='Test');
# plt.hist(probas[y_event==1]/(1-probas[y_event==1]),histtype='step',bins=100,color='red');
plt.axvline(0)
plt.yscale('log')
plt.xlabel('Log w')
plt.ylabel('Density')
plt.title('High level features')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'log_classifier_high_level_features_with_test.pdf')
plt.clf()

weights_test = probas_test/(1.0-probas_test)
print("Average weight")
print(np.mean(weights_test))

np.save(results_dir+'learned_event_weights_first_step_test.npy',weights_test)

weights_meas_test = probas_meas_test/(1.0-probas_meas_test)
print("Average weight")
print(np.mean(weights_meas_test))

np.save(results_dir+'learned_event_weights_first_step_test_data.npy',weights_meas_test)


import shap
import pandas as pd

obs_names=['1-T','C','D',r'$B_{W}$',r'$B_{T}$',r'$n_{f}$',r'$n_{\mathrm{ch}}$',r'$\langle |\ln x_f|\rangle$',r'$\langle (|\ln x_f|-\langle |\ln x_f|\rangle)^2\rangle$',r'$\langle (|\ln x_f|-\langle |\ln x_f|\rangle)^3\rangle$',r'$\langle |\ln x_{\rm ch}|\rangle$',r'$\langle (|\ln x_{\rm ch}|-\langle |\ln x_{\rm ch}|\rangle)^2\rangle$',r'$\langle (|\ln x_{\rm ch}|-\langle |\ln x_{\rm ch}|\rangle)^3\rangle$']

X_event_test_df = pd.DataFrame(scaler.transform(np.vstack([X_event_test,X_meas_event_test])), columns=obs_names)


explainer = shap.TreeExplainer(lr)#,X_event_test_df[:1000])
shap_values = explainer(X_event_test_df)
print(shap_values.feature_names)
print(shap_values.shape)
print(shap_values.values.shape)
shap_values_to_store = np.zeros(X_event_test.shape[1])
for i in range(X_event_test.shape[1]):
    shap_values_to_store[i]=np.mean(np.abs(shap_values.values[:,i]))
    print(i,shap_values_to_store[i])
np.save(results_dir+'shap_values_to_store.npy',shap_values_to_store)

shap.summary_plot(shap_values, X_event_test_df,feature_names=obs_names)
#plt.ytick
plt.tight_layout()
plt.savefig(results_dir+'shap_summary_plot.pdf')
plt.clf()


for nname, name in enumerate(X_event_test_df.columns):
    shap.dependence_plot(name, shap_values.values, X_event_test_df,feature_names=obs_names)
    plt.tight_layout()
    plt.savefig(results_dir+'shap_dependence_plot'+str(nname)+'.pdf')
    plt.clf()

import os
#os.environ['OMP_NUM_THREADS'] = "24"

import numpy as np
import sys
import scipy.stats as st
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'size'   : 25}
rc('font', **font)
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
rc('text', usetex=True)
plt.rcParams['font.family']='Computer Modern'

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import cross_val_predict, train_test_split, KFold

from sklearn.metrics import accuracy_score, precision_score, roc_auc_score
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.calibration import calibration_curve,CalibratedClassifierCV

import torch
torch.set_num_threads(24)

from torch import nn
from tqdm import tqdm
from torch.nn.modules import Module
from torch.nn import Sequential as Seq, Linear, ReLU, Parameter,LeakyReLU
from torch.utils.data import DataLoader 

###############################################################################
def bin_assigner(bins,val):

    # This function simply outputs the bin assignment of a given value val for a given choice of binning bins

    number_of_bins = len(bins)
    bin_number=0
    if val >= bins[-1]:
        bin_number = number_of_bins - 1
    if val < bins[0]:
        bin_number = 0
    else:
        for bin_number_aux in range(number_of_bins-1):
            if bins[bin_number_aux]<= val < bins[bin_number_aux+1]:
                bin_number = bin_number_aux
    return bin_number
###############################################################################
    
def pxpypzEtoptphiyE(hadron):
    E = hadron[-1]
    pT = np.sqrt(hadron[0]**2+hadron[1]**2)
    phi = np.arctan2(hadron[1],hadron[0])
    y = 0.5*np.log((E+hadron[2])/(E-hadron[2]))
    #return [pT, phi, y, E]
    return [pT,hadron[-2],E]
###############################################################################

results_dir = str(sys.argv[1])
X_event = np.load(sys.argv[2])
y_event = np.load(sys.argv[3])
X_event=X_event[:len(y_event)]
print(X_event.shape,y_event.shape)
Nevents=int(np.min([float(sys.argv[4]),len(X_event)]))
X_event = X_event[:Nevents]
y_event = y_event[:Nevents]

### Hadronization scaling
X_event[:,:,3]=X_event[:,:,3]/(0.140)
#X_event[:,:,3]=X_event[:,3]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event[:,:,4]=np.where(X_event[:,:,4]==0,-1,X_event[:,:,4])
#X_event[:,:,4]=X_event[:,:,4]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event[:,:,7]=X_event[:,:,7]/5.0
### Four momenta scaling

X_event[:,:,5]=X_event[:,:,5]/1.0
X_event[:,:,6]=X_event[:,:,6]/1.0

X_event[:,:,9]=X_event[:,:,9]/1.0
X_event[:,:,10]=X_event[:,:,10]/1.0
X_event[:,:,11]=X_event[:,:,11]/25.0
X_event[:,:,12]=X_event[:,:,12]/25.0



stringends = False
print("first 10 first emissions")
if stringends == False:
    valid_values = [a and b for a,b in zip(X_event[:,:,8].flatten()==0.0,X_event[:,:,12].flatten()!=0.0)]
else:
    valid_values = [a and b for a,b in zip(X_event[:,:,8].flatten()>=0.0,X_event[:,:,12].flatten()!=0.0)]
y_event = y_event.flatten()[valid_values]
dataset = np.vstack([X_event[:,:,i].flatten()[valid_values] for i in [0,1,2,3,4,5,6,7,9,10,11,12]]).T
print(dataset.shape,y_event.shape)

try: nepochs = int(sys.argv[5])
except: nepochs = 10
try: learning_rate=float(sys.argv[6])
except: learning_rate = 0.01
try: npatience = int(sys.argv[7])
except: npatience = 5
try: model_path = results_dir+sys.argv[9]
except: model_path = results_dir+'default_forward_model.pth'
print(Nevents,nepochs,learning_rate,npatience)


class xyDataset(torch.utils.data.Dataset):
    """
    Joins the x and y into a dataset, so that it can be used by the pythorch syntax.
    """

    def __init__(self, x, y):
        self.x = torch.tensor(x).to(torch.float)
        self.y = torch.tensor(y).to(torch.float)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        sample = [self.x[idx], self.y[idx]]
        return sample


# Define model


class NeuralNetwork(nn.Module):
    def __init__(self): 
        super(NeuralNetwork, self).__init__()
        self.flatten = nn.Flatten()
        self.mpJoint = Seq(Linear(len([0,1,2,3,4,5,6]), 64),
                       ReLU(),
                       Linear(64, 64),
                       ReLU(),
                       Linear(64, 64),
                       ReLU(),                                          
                       Linear(64, 1))
        self.mpMarginal = Seq(Linear(len([5,6]), 64),
                       ReLU(),
                       Linear(64, 64),
                       ReLU(),
                       Linear(64, 64),
                       ReLU(),                                          
                       Linear(64, 1))

    def forward(self, x):
        output = self.flatten(x)
        joint = self.mpJoint(output[:,[0,1,2,3,4,5,6]])
        marginal = self.mpMarginal(output[:,[5,6]])
        output = joint-marginal
        return output, joint, marginal

    def loss_function(self,t,y):
        loss_fn = nn.MSELoss()
        return loss_fn(t,y)

def train(model,optimizer,train_dataset,batch_size=1024):
    model.train()
    train_dataset_batched = DataLoader(train_dataset,batch_size=batch_size,shuffle=True)
    total_loss = 0
    for batch, (X, y) in enumerate(train_dataset_batched):
        X, y  = X.to(device), y.to(device)
        optimizer.zero_grad()  # Clear gradients.
        pred, predJoint, predMarginal = model(X)  # Forward pass.
        loss = model.loss_function(pred[:,0],y)  # Loss computation.
        loss.backward()  # Backward pass.
        optimizer.step()  # Update model parameters.
        total_loss += loss.item()

    return total_loss / len(train_dataset_batched.dataset)

def test(model, test_dataset,batch_size=1024):
    model.eval()
    test_dataset_batched = DataLoader(test_dataset,batch_size=batch_size,shuffle=False)
    total_loss = 0
    for batch, (X, y) in enumerate(test_dataset_batched):
        X, y  = X.to(device), y.to(device)
        pred, predJoint, predMarginal = model(X)  # Forward pass.
        loss = model.loss_function(pred[:,0],y)  # Loss computation.
        total_loss += loss.item()

    return total_loss / len(test_dataset_batched.dataset)

def pred(model,test_dataset,batch_size=1024):
    model.eval()
    test_dataset_batched = DataLoader(test_dataset,batch_size=batch_size,shuffle=False)
    truths = np.zeros(len(test_dataset))
    preds = np.zeros(len(test_dataset))
    initial_index = 0
    final_index = 0
    for batch, (X, y) in enumerate(test_dataset_batched):
        final_index+=len(y)
        truths[initial_index:final_index]=y.detach().cpu().numpy()
        X, y  = X.to(device), y.to(device)
        pred, predJoint, predMarginal = model(X)  # Forward pass.
        preds[initial_index:final_index] = pred[:,0].detach().cpu().numpy()
        initial_index+=len(y)
    return truths, preds

X_train, X_val, y_train, y_val = train_test_split(dataset,y_event,test_size=0.25, random_state=42,shuffle=False)
train_dataset=xyDataset(X_train,y_train)
val_dataset=xyDataset(X_val,y_val)
full_dataset=xyDataset(dataset,y_event)


model = NeuralNetwork()
try: model.load_state_dict(torch.load(model_path))
except: print("New model")
device = torch.device("cuda:1" if torch.cuda.is_available() else "cpu")
model.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

val_loss_aux = 1000
times = 0
for epoch in range(1, nepochs):
    loss = train(model, optimizer,train_dataset,batch_size=10000)
    val_loss = test(model,val_dataset)
    print(f'Epoch: {epoch:02d}, Loss: {loss:.4f},Val Loss: {val_loss:.4f}, Learning Rate: {learning_rate:.6f}')
    if epoch == 0:
        val_loss_aux = val_loss
        times = 0
        continue

    if loss < val_loss_aux:
        val_loss_aux = val_loss
        times = 0
    else:
        times+=1

    if times == npatience:
        learning_rate*=0.1
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)    
    if times == 2*npatience:
        break  
    torch.save(model.state_dict(),model_path)


y_truths, y_preds = pred(model,full_dataset)
np.save(results_dir+'all_emission_weights_best_NN_train.npy',np.exp(y_preds))
print(y_truths.shape,y_preds.shape)
print(np.mean(np.exp(y_truths)),np.mean(np.exp(y_preds)))

a,b,c = plt.hist(y_truths,bins=100,label='Truth',histtype='step')
plt.hist(y_preds,bins=b,label='Reconstructed',histtype='step')
plt.xlabel('Log Weights')
plt.legend(loc='upper right')
plt.ylabel('Events')
plt.yscale('log')
plt.tight_layout()
plt.savefig(results_dir+'log_weight_reconstruction_histogram.pdf')
plt.clf()

plt.hist2d(y_truths,y_preds)
plt.colorbar()
plt.xlabel('Truth')
plt.ylabel('Reconstructed')
plt.title('Log Weights')
plt.tight_layout()
plt.savefig(results_dir+'log_weight_reconstruction_truth_vs_pred.pdf')
plt.clf()

a,b,c = plt.hist(np.exp(y_truths),bins=100,label='Truth',histtype='step')
plt.hist(np.exp(y_preds),bins=b,label='Reconstructed',histtype='step')
plt.xlabel('Weights')
plt.legend(loc='upper right')
plt.ylabel('Events')
plt.yscale('log')
plt.tight_layout()
plt.savefig(results_dir+'weight_reconstruction_histogram.pdf')
plt.clf()

plt.hist2d(np.exp(y_truths),np.exp(y_preds))
plt.colorbar()
plt.xlabel('Truth')
plt.ylabel('Reconstructed')
plt.title('Weights')
plt.tight_layout()
plt.savefig(results_dir+'weight_reconstruction_truth_vs_pred.pdf')
plt.clf()


fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
ax1.set_ylabel('Density')
ax2.set_ylabel('New/Sim')
ax2.set_ylim(0.5,1.5)
plt.xlabel('$z$')
a,b,c = ax1.hist(dataset[:,0],bins=50,histtype='step',color='blue',label='Sim',density=True)
er = np.sqrt(a*(b[1]-b[0])/len(dataset[:,0]))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color='blue')


a2,b,c = ax1.hist(dataset[:,0].flatten(),bins=b,weights=np.exp(y_truths),histtype='step',color='red',label='Analytical',density=True)
a2_aux, b = np.histogram(dataset[:,0].flatten(),bins=b,weights=np.exp(y_truths))
er2, b = np.histogram(dataset[:,0].flatten(),bins=b,weights=np.exp(y_truths)**2)
er2=np.sqrt(er2*(1.0/np.sum(np.exp(y_truths)))**2+((a2_aux/(np.sum(np.exp(y_truths)))**2)**2) *(np.sum(np.exp(y_truths)**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='red')
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='red')

a2,b,c = ax1.hist(dataset[:,0].flatten(),bins=b,weights=np.exp(y_preds),histtype='step',color='magenta',label='NN',density=True)
a2_aux, b = np.histogram(dataset[:,0].flatten(),bins=b,weights=np.exp(y_preds))
er2, b = np.histogram(dataset[:,0].flatten(),bins=b,weights=np.exp(y_preds)**2)
er2=np.sqrt(er2*(1.0/np.sum(np.exp(y_preds)))**2+((a2_aux/(np.sum(np.exp(y_preds)))**2)**2) *(np.sum(np.exp(y_preds)**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='magenta')
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='magenta')


ax1.legend(loc='upper right')
ax2.axhline(1.0,linestyle='dashed',color='black')
plt.savefig(results_dir+'reweighted_emissions_gnn_high_level_ratio.pdf')
plt.clf()

plt.close("all")

### save weights for whole event
print("Event weights")

event_weights_pred = np.zeros(Nevents)
event_weights_true = np.zeros(Nevents)

event_weights_first_pred = np.zeros(Nevents)
event_weights_first_true = np.zeros(Nevents)

event_weights_second_pred = np.zeros(Nevents)
event_weights_second_true = np.zeros(Nevents)

index = 0 
for n in range(Nevents):
    if stringends == False:
        if np.sum(X_event[n,:int(np.sum(X_event[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        multiplicity=np.sum([a and b for a,b in zip(X_event[n,:,12]!=0.0,X_event[n,:,8]==0.0)])
    else:
        if np.sum(X_event[n,:,12]!=0.0) == 0.0:
            continue
        multiplicity=np.sum(X_event[n,:,12]!=0.0)

    event_weights_pred[n]=np.exp(np.sum(y_preds[index:index+multiplicity]))
    event_weights_true[n]=np.exp(np.sum(y_truths[index:index+multiplicity]))
    event_weights_first_pred[n]=np.exp(np.sum(y_preds[index:index+1]))
    event_weights_first_true[n]=np.exp(np.sum(y_truths[index:index+1]))
    if multiplicity >=2:
        event_weights_second_pred[n]=np.exp(np.sum(y_preds[index+1:index+2]))
        event_weights_second_true[n]=np.exp(np.sum(y_truths[index+1:index+2]))        

    index+=multiplicity
print(len(dataset),Nevents,index,len(y_event))
print(np.mean(event_weights_true),np.mean(event_weights_pred))
print(np.mean(event_weights_first_true),np.mean(event_weights_first_pred))
print(np.mean(event_weights_second_true[event_weights_second_true>0]),np.mean(event_weights_second_pred[event_weights_second_pred>0]))

print("Pearson r")
print(str(np.round(st.pearsonr(event_weights_true,event_weights_pred),4)[0]))
print("Hist 1")
plt.hist2d(event_weights_true,event_weights_pred,bins=100)
plt.colorbar()
#print("Colorbar")
plt.plot(np.linspace(np.min(event_weights_true),np.max(event_weights_true),10),np.linspace(np.min(event_weights_true),np.max(event_weights_true),10),'k')
#print("Linspace")
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$w_{\mathrm{Target}}$')
plt.ylabel('$w_{\mathrm{NN}}$')
plt.title(str(np.round(st.pearsonr(event_weights_true,event_weights_pred),4)[0]))
#print("Pearson")
plt.tight_layout()
plt.savefig(results_dir+'event_weights_from_split.pdf')
plt.clf()

a,b,c = plt.hist(event_weights_true,histtype='step',color='blue',bins=100,label='Target')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color='blue')
a2,b,c = plt.hist(event_weights_pred,histtype='step',color='magenta',bins=b,label='NN')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='magenta')
#print("Linspace")
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Weight')
plt.ylabel('Events')
plt.title(str(np.round(st.pearsonr(event_weights_true,event_weights_pred),4)[0]))
#print("Pearson")
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'event_weights_from_split_bis.pdf')
plt.clf()

print("Hist 2")
plt.hist2d(np.log(event_weights_true),np.log(event_weights_pred),bins=100)
plt.colorbar()
plt.plot(np.linspace(np.min(np.log(event_weights_true)),np.max(np.log(event_weights_true)),10),np.linspace(np.min(np.log(event_weights_true)),np.max(np.log(event_weights_true)),10),'k')
#plt.xscale('log')
#plt.yscale('log')
plt.xlabel('Log $w_{\mathrm{Target}}$')
plt.ylabel('Log $w_{\mathrm{NN}}$')
plt.title(str(np.round(st.pearsonr(np.log(event_weights_true),np.log(event_weights_pred)),4)[0]))
plt.tight_layout()
plt.savefig(results_dir+'log_event_weights_from_split.pdf')
plt.clf()

a,b,c = plt.hist(np.log(event_weights_true),histtype='step',color='blue',bins=100,label='Target')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color='blue')
a2,b,c = plt.hist(np.log(event_weights_pred),histtype='step',color='magenta',bins=b,label='NN')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='magenta')
#plt.xscale('log')
plt.yscale('log')
plt.xlabel('Log Weight')
plt.ylabel('Events')
plt.title(str(np.round(st.pearsonr(np.log(event_weights_true),np.log(event_weights_pred)),4)[0]))
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'log_event_weights_from_split_bis.pdf')
plt.clf()


np.save(results_dir+'event_weights_from_split_info_train.npy',event_weights_pred)

print("Test set")
X_event_test = np.load(sys.argv[8])


### Hadronization scaling
X_event_test[:,:,3]=X_event_test[:,:,3]/(0.140)
#X_event[:,:,3]=X_event[:,3]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event_test[:,:,4]=np.where(X_event_test[:,:,4]==0,-1,X_event_test[:,:,4])
#X_event[:,:,4]=X_event[:,:,4]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event_test[:,:,7]=X_event_test[:,:,7]/5.0
### Four momenta scaling

X_event_test[:,:,5]=X_event_test[:,:,5]/1.0
X_event_test[:,:,6]=X_event_test[:,:,6]/1.0

X_event_test[:,:,9]=X_event_test[:,:,9]/1.0
X_event_test[:,:,10]=X_event_test[:,:,10]/1.0
X_event_test[:,:,11]=X_event_test[:,:,11]/25.0
X_event_test[:,:,12]=X_event_test[:,:,12]/25.0



if stringends == False:
    valid_values_test = [a and b for a,b in zip(X_event_test[:,:,8].flatten()==0.0,X_event_test[:,:,12].flatten()!=0.0)]
else:
    valid_values_test = [a and b for a,b in zip(X_event_test[:,:,8].flatten()>=0.0,X_event_test[:,:,12].flatten()!=0.0)]
y_event_test = np.zeros(int(np.sum(valid_values_test)))
dataset_test = np.vstack([X_event_test[:,:,i].flatten()[valid_values_test] for i in [0,1,2,3,4,5,6,7,9,10,11,12]]).T
print(dataset_test.shape,y_event_test.shape)

full_dataset_test=xyDataset(dataset_test,y_event_test)
y_truths_test, y_preds_test = pred(model,full_dataset_test)
print(np.mean(np.exp(y_preds_test)))
np.save(results_dir+'all_emission_weights_best_NN_test.npy',np.exp(y_preds_test))

event_weights_pred_test = np.zeros(len(X_event_test))

index = 0 
for n in range(len(X_event_test)):
    if stringends == False:
        if np.sum(X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        multiplicity=np.sum([a and b for a,b in zip(X_event_test[n,:,12]!=0.0,X_event_test[n,:,8]==0.0)])
    else:
        if np.sum(X_event_test[n,:,12]!=0.0) == 0.0:
            continue
        multiplicity=np.sum(X_event_test[n,:,12]!=0.0)

    event_weights_pred_test[n]=np.exp(np.sum(y_preds_test[index:index+multiplicity]))

    index+=multiplicity
print(index,len(y_preds_test))
print(np.mean(event_weights_pred_test))

np.save(results_dir+'event_weights_from_split_info_test.npy',event_weights_pred_test)

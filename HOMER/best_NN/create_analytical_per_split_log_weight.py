import os
os.environ['OMP_NUM_THREADS'] = "24"

import numpy as np
import sys
import scipy.stats as st
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'size'   : 22}
rc('font', **font)

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import cross_val_predict, train_test_split, KFold

from sklearn.metrics import accuracy_score, precision_score, roc_auc_score
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.calibration import calibration_curve,CalibratedClassifierCV


###############################################################################
def bin_assigner(bins,val):

    # This function simply outputs the bin assignment of a given value val for a given choice of binning bins

    number_of_bins = len(bins)
    bin_number=0
    if val >= bins[-1]:
        bin_number = number_of_bins - 1
    if val < bins[0]:
        bin_number = 0
    else:
        for bin_number_aux in range(number_of_bins-1):
            if bins[bin_number_aux]<= val < bins[bin_number_aux+1]:
                bin_number = bin_number_aux
    return bin_number
###############################################################################

def lund(a,b,sample):
        ### this function mimicks Pythia's zLund. It takes as input two parameters, a and b, and a given sample [mT^2,z] and inputs the Lund function value normalized such that f(zMax) = 1

        z = sample[1]
        beff = b*sample[0]

        ### I initialize two ints to asses whether zMax is too low or too high
        peakednearzero = 0
        peakednearunity = 0
        
        ### zMax determination
        if a == 0.0:
            if 1.0 > beff:
                zMax = beff
            else:
                zMax = 1.0
        elif a == 1.0:
            zMax = beff/(beff+1.0)
        else:
            zMax = 0.5 * (beff + 1.0 - np.sqrt( (beff - 1.0)**2 + 4* a * beff)) / (1.0 - a)
        if (zMax > 0.9999 and beff > 100):
             zMax = np.min([zMax, 1.0 - a/beff])

    # get current regime
        if zMax < 0.1:
            peakednearzero = 1
        if zMax > 0.85 and beff > 1.0:
            peakednearunity = 1

        # right now I'll ignore whether it is too small or too large but I'll print it just to keep in mind that it does happen

    # if zMax too small
        if peakednearzero == 1:
            # print("Near Zero")
#             pass
            zDiv = 2.75 * zMax
            fIntLow = zDiv
            fIntHigh = -zDiv * np.log(zDiv)
            fInt = fIntLow + fIntHigh
            fZ = 1.0 if z < zDiv else zDiv/z
            #if z > 0.0 and z < 1.0:
            #    z = zDiv * z if z < zDiv else zDiv/z
        #return 0.0

    # if zMax too large 
        elif peakednearunity == 1:
            # print("Near Unity")
        #return 0.0
#             pass
            rcb = np.sqrt(4. + (1.0 / beff)**2)
            zDiv = rcb - 1.0/zMax - (1.0 / beff) *np.log( zMax * 0.5 * (rcb + 1.0 / beff) )+(a/beff) * np.log(1. - zMax)
            zDiv = np.min([zMax, np.max([0., zDiv])])
            fIntLow = 1.0 / beff
            fIntHigh = 1. - zDiv
            fInt = fIntLow + fIntHigh
            fZ = np.exp( beff * (z - zDiv) ) if z < zDiv else 1.0
            #if z > 0.0 and z < 1.0:
            #    z =  zDiv + np.log(z) / beff if z < zDiv else zDiv + (1. - zDiv) * z

        # Lund function itself 
        if z > 0.0 and z<1.0:
            #fExp = beff * (0.0 - 1. / z)+ 1.0 * np.log(1.0 / z) + a * np.log( (1. -z) )
            fExp = beff * (1. / zMax - 1. / z)+ 1.0*np.log(zMax / z) + a * np.log( (1. - z) / (1. - zMax) )
            fZ = np.exp( np.max( [-50, np.min( [50, fExp]) ]) ) # 50 is chosen to avoid too large exponents
        else:
            fZ = 0.0
                
        return fZ
    
def lund_unnorm(a,b,sample):
        ### this function mimicks Pythia's zLund. It takes as input two parameters, a and b, and a given sample [mT^2,z] and inputs the Lund function value normalized such that f(zMax) = 1

        z = sample[1]
        beff = b*sample[0]

        ### I initialize two ints to asses whether zMax is too low or too high
        peakednearzero = 0
        peakednearunity = 0
        
        ### zMax determination
        if a == 0.0:
            if 1.0 > beff:
                zMax = beff
            else:
                zMax = 1.0
        elif a == 1.0:
            zMax = beff/(beff+1.0)
        else:
            zMax = 0.5 * (beff + 1.0 - np.sqrt( (beff - 1.0)**2 + 4* a * beff)) / (1.0 - a)
        if (zMax > 0.9999 and beff > 100):
             zMax = np.min([zMax, 1.0 - a/beff])

    # get current regime
        if zMax < 0.1:
            peakednearzero = 1
        if zMax > 0.85 and beff > 1.0:
            peakednearunity = 1

        # right now I'll ignore whether it is too small or too large but I'll print it just to keep in mind that it does happen

    # if zMax too small
#         if peakednearzero == 1:
#             print("Near Zero")
#             pass
#             zDiv = 2.75 * zMax
#             fIntLow = zDiv
#             fIntHigh = -zDiv * np.log(zDiv)
#             fInt = fIntLow + fIntHigh
#             fZ = 1.0 if z < zDiv else zDiv/z
            #if z > 0.0 and z < 1.0:
            #    z = zDiv * z if z < zDiv else zDiv/z
        #return 0.0

    # if zMax too large 
#         elif peakednearunity == 1:
            # print("Near Unity")
        #return 0.0
#             pass
#             rcb = np.sqrt(4. + (1.0 / beff)**2)
#             zDiv = rcb - 1.0/zMax - (1.0 / beff) *np.log( zMax * 0.5 * (rcb + 1.0 / beff) )+(a/beff) * np.log(1. - zMax)
#             zDiv = np.min([zMax, np.max([0., zDiv])])
#             fIntLow = 1.0 / beff
#             fIntHigh = 1. - zDiv
#             fInt = fIntLow + fIntHigh
#             fZ = np.exp( beff * (z - zDiv) ) if z < zDiv else 1.0
            #if z > 0.0 and z < 1.0:
            #    z =  zDiv + np.log(z) / beff if z < zDiv else zDiv + (1. - zDiv) * z

        # Lund function itself 
        if 1==1:
            if z > 0.0 and z<1.0:
                #fExp = beff * (0.0 - 1. / z)+ 1.0 * np.log(1.0 / z) + a * np.log( (1. -z) )
                fExp = beff * (- 1. / z)+ 1.0*np.log(1.0 / z) + a * np.log( (1. - z))
                fZ = np.exp(fExp)#np.exp( np.max( [-50, np.min( [50, fExp]) ]) ) # 50 is chosen to avoid too large exponents
            else:
                fZ = 0.0
                
        return fZ

    
def lund_unnorm_max(a,b,sample):
        ### this function mimicks Pythia's zLund. It takes as input two parameters, a and b, and a given sample [mT^2,z] and inputs the Lund function value normalized such that f(zMax) = 1

        z = sample[1]
        beff = b*sample[0]

        
        ### zMax determination
        if a == 0.0:
            if 1.0 > beff:
                zMax = beff
            else:
                zMax = 1.0
        elif a == 1.0:
            zMax = beff/(beff+1.0)
        else:
            zMax = 0.5 * (beff + 1.0 - np.sqrt( (beff - 1.0)**2 + 4* a * beff)) / (1.0 - a)
        if (zMax > 0.9999 and beff > 100):
             zMax = np.min([zMax, 1.0 - a/beff])

        # Lund function itself 
        if zMax > 0.0 and zMax<1.0:
            #fExp = beff * (0.0 - 1. / z)+ 1.0 * np.log(1.0 / z) + a * np.log( (1. -z) )
            fExp = beff * (- 1. / zMax)+ 1.0*np.log(1.0 / zMax) + a * np.log( (1. - zMax))
            fZ = np.exp( np.max( [-50, np.min( [50, fExp]) ]) ) # 50 is chosen to avoid too large exponents
        else:
            fZ = 0.0
                
        return fZ

###############################################################################
results_dir = str(sys.argv[1])
X_event = np.load(sys.argv[2])
print(X_event.shape)
Nevents=int(np.min([float(sys.argv[3]),len(X_event)]))
alund_sim=float(sys.argv[4])
alund_data=float(sys.argv[5])

X_event = X_event[:Nevents]
print(X_event.shape)

y_event = np.zeros((X_event.shape[0],X_event.shape[1]))


all_mT2_vals = X_event[:,:,3].flatten()**2+X_event[:,:,9].flatten()**2+X_event[:,:,10].flatten()**2
valid_values = [a and b for a,b in zip(X_event[:,:,8].flatten()==0.0,X_event[:,:,-1].flatten()>0.0)]
all_mT2_vals=all_mT2_vals[valid_values]

nmT2_bins = 1000
mT2_bins=np.linspace(0.95*np.min(all_mT2_vals),7,nmT2_bins)
nz_bins = 1000
z_bins=np.linspace(0,1,nz_bins)
normalization_ratio = np.zeros(nmT2_bins-1)
print(np.min(all_mT2_vals),np.max(all_mT2_vals),mT2_bins[0],mT2_bins[-1],(mT2_bins[1]-mT2_bins[0]),nmT2_bins)
print(np.quantile(all_mT2_vals,0.95),np.quantile(all_mT2_vals,0.99))

for nmT2_bin in range(nmT2_bins-1):
    mT2_val = 0.5*(mT2_bins[nmT2_bin]+mT2_bins[nmT2_bin+1])
    function_vals_base=np.zeros(nz_bins)
    function_vals_nom=np.zeros(nz_bins)
    for nz_bin in range(nz_bins):
        z_val = z_bins[nz_bin]
        function_vals_base[nz_bin]=lund(alund_sim,0.98,[mT2_val,z_val])
        function_vals_nom[nz_bin]=lund(alund_data,0.98,[mT2_val,z_val])
    normalization_ratio[nmT2_bin]=np.sum(function_vals_base)/np.sum(function_vals_nom)
for nevent in range(len(X_event)):

    for nhadron, hadron in enumerate(X_event[nevent]):
        if hadron[-1] == 0.0: # zero energy means end of string
            break
        elif hadron[8]==0.0: ## Let's NOT use stringends
            mT2 = hadron[9]**2+hadron[10]**2+hadron[3]**2
            z = hadron[0]
            function_val_base=lund(alund_sim,0.98,[mT2,z])
            function_val_nom=lund(alund_data,0.98,[mT2,z])
            if mT2 < 7:
                nmT2_bin=bin_assigner(mT2_bins,mT2)
                y_event[nevent,nhadron]=-np.log(function_val_base/function_val_nom)+np.log(normalization_ratio[nmT2_bin])
            else:
                nz_bins = 10000
                z_bins=np.linspace(0,1,nz_bins)
                function_vals_est_base=np.zeros(nz_bins)
                function_vals_est_nom=np.zeros(nz_bins)
                for nz_bin in range(nz_bins):
                    z_val = z_bins[nz_bin]
                    function_vals_est_base[nz_bin]=lund(alund_sim,0.98,[mT2,z_val])
                    function_vals_est_nom[nz_bin]=lund(alund_data,0.98,[mT2,z_val])
                normalization_ratio_est = np.sum(function_vals_est_base)/np.sum(function_vals_est_nom)
                y_event[nevent,nhadron]=-np.log(function_val_base/function_val_nom)+np.log(normalization_ratio_est)
print(X_event.shape,y_event.shape,np.mean(np.exp(y_event.flatten()[valid_values])),np.mean(np.exp(-y_event.flatten()[valid_values])))
#print(np.max(y_event,1))

np.save(results_dir+'analytical_per_split_log_weight.npy',y_event)

import os
#os.environ['OMP_NUM_THREADS'] = "24"

import numpy as np
import sys
import scipy.stats as st
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'size'   : 25}
rc('font', **font)
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
rc('text', usetex=True)
plt.rcParams['font.family']='Computer Modern'

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import cross_val_predict, train_test_split, KFold

from sklearn.metrics import accuracy_score, precision_score, roc_auc_score
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.calibration import calibration_curve,CalibratedClassifierCV

import torch
torch.set_num_threads(40)

from torch import nn
from tqdm import tqdm
from torch.nn.modules import Module

from torch_geometric.data import Data
from torch_geometric.loader import DataLoader

from torch_geometric.utils import add_self_loops, degree
from torch.nn import Sequential as Seq, Linear, ReLU, Parameter,LeakyReLU
from torch_geometric.nn import MessagePassing, global_max_pool, global_add_pool

###############################################################################
def bin_assigner(bins,val):

    # This function simply outputs the bin assignment of a given value val for a given choice of binning bins

    number_of_bins = len(bins)
    bin_number=0
    if val >= bins[-1]:
        bin_number = number_of_bins - 1
    if val < bins[0]:
        bin_number = 0
    else:
        for bin_number_aux in range(number_of_bins-1):
            if bins[bin_number_aux]<= val < bins[bin_number_aux+1]:
                bin_number = bin_number_aux
    return bin_number
###############################################################################

### general framework for training and testing
def train(model, optimizer, loader,len_dataset):
    model.train()
    
    total_loss = 0
    for data in loader:
        data = data.to(device)
        optimizer.zero_grad()  # Clear gradients.
        #print("Starting")
        max_histories_per_event = torch.tensor([torch.max(data.x[data.batch==event_index][:,12]) for event_index in range(len(data.y))])
        lengths_histories_all_strings = torch.tensor([0]+[ max_histories_per_event[event_index]+1 for event_index in range(len(data.y)-1)])
        
        total_accepted_fragmentations_per_event = torch.hstack([torch.where(data.x[data.batch==event_index][:,12]==max_histories_per_event[event_index],1,0) for event_index in range(len(data.y))])

        nominal_factor = 1/(1+torch.mean(max_histories_per_event))

        new_batch = torch.hstack([torch.tensor([int(element) for element in data.x[data.batch==event_index][:,12]+torch.sum(lengths_histories_all_strings[:event_index+1])]) for event_index in range(len(data.y))])
        

        pred_Acc, predJoint_Acc, predMarginal_Acc = model(data.x[torch.where(total_accepted_fragmentations_per_event)], data.edge_index,data.batch[torch.where(total_accepted_fragmentations_per_event)])  # Forward pass.
        pred_Acc = pred_Acc[:,0]
        predJoint_Acc = predJoint_Acc[:,0]
        predMarginal_Acc = predMarginal_Acc[:,0]

        pred_byHistory, predJoint_byHistory, predMarginal_byHistory = model(data.x, data.edge_index,new_batch)  # Forward pass.
        pred_byHistory = pred_byHistory[:,0]
        predJoint_byHistory = predJoint_byHistory[:,0]
        predMarginal_byHistory = predMarginal_byHistory[:,0]

        reweighted_factor=torch.sum(torch.exp(pred_Acc))/torch.sum(torch.exp(pred_byHistory))

        loss1 = -torch.mean(torch.exp(data.y)*torch.log((nominal_factor/reweighted_factor)*torch.exp(pred_Acc)/(1+(nominal_factor/reweighted_factor)*torch.exp(pred_Acc)))+torch.log(1/(1+(nominal_factor/reweighted_factor)*torch.exp(pred_Acc)))) 
        
        pred, predJoint, predMarginal = model(data.x, data.edge_index,data.batch)  # Forward pass.
        pred = pred[:,0]
        predJoint = predJoint[:,0]
        predMarginal = predMarginal[:,0]
        loss2 = -torch.mean(torch.exp(predJoint)*torch.log(torch.exp(predMarginal)/(1+torch.exp(predMarginal)))+torch.log(1/(1+torch.exp(predMarginal))))
        

        loss = loss1+loss2
        loss.backward()
        optimizer.step()
        total_loss +=loss.item() * data.num_graphs
    print(nominal_factor,reweighted_factor,(torch.sum(lengths_histories_all_strings)+max_histories_per_event[-1]+1))
    return total_loss / len_dataset

def test(model,loader,len_dataset):
    model.eval()
    predictions = np.zeros(len_dataset)
    values = np.zeros(len_dataset)
    initial_index = 0
    for ndata, data in enumerate(loader):
        data = data.to(device)

        max_histories_per_event = torch.tensor([torch.max(data.x[data.batch==event_index][:,12]) for event_index in range(len(data.y))])
        lengths_histories_all_strings = torch.tensor([0]+[ max_histories_per_event[event_index]+1 for event_index in range(len(data.y)-1)])

        total_accepted_fragmentations_per_event = torch.hstack([torch.where(data.x[data.batch==event_index][:,12]==max_histories_per_event[event_index],1,0) for event_index in range(len(data.y))])

        nominal_factor = 1/(1+torch.mean(max_histories_per_event))

        new_batch = torch.hstack([torch.tensor([int(element) for element in data.x[data.batch==event_index][:,12]+torch.sum(lengths_histories_all_strings[:event_index+1])]) for event_index in range(len(data.y))])        

        pred_Acc, predJoint_Acc, predMarginal_Acc = model(data.x[torch.where(total_accepted_fragmentations_per_event)], data.edge_index,data.batch[torch.where(total_accepted_fragmentations_per_event)])  # Forward pass.
        pred_Acc = pred_Acc[:,0]
        predJoint_Acc = predJoint_Acc[:,0]
        predMarginal_Acc = predMarginal_Acc[:,0]

        pred_byHistory, predJoint_byHistory, predMarginal_byHistory = model(data.x, data.edge_index,new_batch)  # Forward pass.
        pred_byHistory = pred_byHistory[:,0]
        predJoint_byHistory = predJoint_byHistory[:,0]
        predMarginal_byHistory = predMarginal_byHistory[:,0]

        reweighted_factor=torch.sum(torch.exp(pred_Acc))/torch.sum(torch.exp(pred_byHistory))
        pred =(torch.log((nominal_factor/reweighted_factor))+pred_Acc).detach().cpu().numpy()        

        predictions[initial_index:initial_index+len(pred)]=pred
        values[initial_index:initial_index+len(pred)]=data.y.detach().cpu().numpy()
        initial_index+=len(pred)
    return predictions, values

def optimal_weight(model,loader,len_dataset):
    model.eval()

    predictions = np.zeros(len_dataset)
    values = np.zeros(len_dataset)
    initial_index = 0
    for ndata, data in enumerate(loader):
        data = data.to(device)

        pred = model(data.x, data.edge_index,data.batch)[0][:,0].detach().cpu().numpy()  # Forward pass.
        predictions[initial_index:initial_index+len(pred)]=pred
        values[initial_index:initial_index+len(pred)]=data.y.detach().cpu().numpy()
        initial_index+=len(pred)
    return predictions, values

def weight_getter(model,loader):
    model.eval()
    predictions = []
    values = []
    for ndata, data in enumerate(loader):
        data = data.to(device)
        internal_weights = model.internal_representation(data.x, data.edge_index,data.batch).detach().cpu().numpy()[:,0]
        emissions = data.x.detach().cpu().numpy()
        for nemission, emission in enumerate(emissions):
            predictions.append(internal_weights[nemission])  # Forward pass.
            values.append(emission)
    return np.exp(np.array(predictions)), np.array(values)



#'''
###  A model with no connections, just using z, pT, mTHad, pxOld, pyOld and no end points

# PointCloud with no connections classifier
class PointCloud(MessagePassing):
    def __init__(self, in_channels, inner_channels, out_channels):
        super().__init__(aggr='sum') #  "Sum" aggregation.
        self.mlp = Seq(Linear(in_channels, inner_channels),
                       ReLU(),
                       Linear(inner_channels, inner_channels),
                       ReLU(),
                       Linear(inner_channels, inner_channels),
                       ReLU(),
                       #Linear(inner_channels, inner_channels),
                       #ReLU(),                                              
                       Linear(inner_channels, out_channels))
        

    def forward(self, x, edge_index):
        # x has shape [N, in_channels]
        # edge_index has shape [2, E]
        edge_index, _ = add_self_loops(edge_index, num_nodes=x.size(0))
        #print(edge_index)
        out = self.propagate(edge_index, x=x)
        #print(out.shape)

        return out

    def message(self, x_i):
#         print(x_i.shape)
        # x_i has shape [E, in_channels]

#         tmp = torch.cat([x_i, x_j - x_i], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.mlp(x_i)



# Particle Net

class PointNet(torch.nn.Module):
    def __init__(self,inner_dimension):
        super().__init__()

        torch.manual_seed(12345)
        self.mpJoint = PointCloud(len([0,1,2,3,4,5,6]), inner_dimension,1)
        self.mpMarginal = PointCloud(len([5,6]), inner_dimension,1)
        
    def forward(self, x, edge_index,batch):
        # Compute the kNN graph:
        # Here, we need to pass the batch vector to the function call in order
        # to prevent creating edges between points of different examples.
        # We also add `loop=True` which will add self-loops to the graph in
        # order to preserve central point information.
        
        # Start message passing.
        hJoint = self.mpJoint(x[:,[0,1,2,3,4,5,6]], edge_index=edge_index)
        hMarginal = self.mpMarginal(x[:,[5,6]], edge_index=edge_index)
        # Global Pooling.
        h = global_add_pool(hJoint-hMarginal, batch)  # [num_examples, hidden_channels]

        return h, hJoint, hMarginal
    def internal_representation(self,x,edge_index,batch):
        h = self.mpJoint(x[:,[0,1,2,3,4,5,6]], edge_index=edge_index)-self.mpMarginal(x[:,[5,6]], edge_index=edge_index)

        return h
    def base_likelihood(self,x,edge_index,batch):
        h = self.base_weight(x, edge_index=edge_index)
        return h
    def learned_likelihood(self,x,edge_index,batch):
        h = self.learned_weight(x, edge_index=edge_index)
        return h
    
###############################################################################
results_dir = str(sys.argv[1])
X_event = np.load(sys.argv[2])


y_event = np.load(sys.argv[3])
y_event = y_event/np.mean(y_event)
history_indexes = np.load(sys.argv[4])
X_event=X_event[:len(y_event)]
history_indexes=history_indexes[:len(y_event)]

print(X_event.shape,history_indexes.shape,y_event.shape,np.mean(y_event))
emissions_truth = np.load(sys.argv[5])
Nevents=int(np.min([float(sys.argv[6]),len(X_event)]))
X_event = X_event[:Nevents]
history_indexes = history_indexes[:Nevents]
y_event = y_event[:Nevents]


X_event = np.concatenate((X_event,history_indexes.reshape((X_event.shape[0],X_event.shape[1],1))),axis=2)
print(X_event.shape,y_event.shape,np.mean(y_event))
### Here we do some specific preprocessing that doesn't alter stuff
# obs = zHad,pxNew,pyNew,mHad,fromPos,pxOld,pyOld,GammaOld,StringEnd,px(),py(),pz(),e(), pxRem,pyRem,pzRem,eRem

### Hadronization scaling
X_event[:,:,3]=X_event[:,:,3]/(0.140)
#X_event[:,:,3]=X_event[:,3]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event[:,:,4]=np.where(X_event[:,:,4]==0,-1,X_event[:,:,4])
#X_event[:,:,4]=X_event[:,:,4]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event[:,:,7]=X_event[:,:,7]/5.0
### Four momenta scaling

X_event[:,:,5]=X_event[:,:,5]/1.0
X_event[:,:,6]=X_event[:,:,6]/1.0

X_event[:,:,9]=X_event[:,:,9]/1.0
X_event[:,:,10]=X_event[:,:,10]/1.0
X_event[:,:,11]=X_event[:,:,11]/25.0
X_event[:,:,12]=X_event[:,:,12]/25.0



try: nepochs = int(sys.argv[7])
except: nepochs = 10
try: learning_rate=float(sys.argv[8])
except: learning_rate = 0.01
try: npatience = int(sys.argv[9])
except: npatience = 5
try: model_path = results_dir+str(sys.argv[12])
except: model_path = results_dir+'default_model_second_step_regression_two_losses.pth'
print(Nevents,nepochs,learning_rate,npatience)

device = "cpu"#torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)


dataset = []
multiplicities=np.zeros(Nevents)
stringends = False
print("first 10 first emissions")

for n in range(Nevents):
    if stringends == False:
        if np.sum(X_event[n,:int(np.sum(X_event[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        multiplicities[n]=np.sum([a and b for a,b in zip(X_event[n,:,12]!=0.0,X_event[n,:,8]==0.0)])
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)   
        hadrons = X_event[n,:int(np.sum(X_event[n,:,12]!=0.0))]
        hadrons = hadrons[X_event[n,:int(np.sum(X_event[n,:,12]!=0.0)),8]==0.0]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
        if n < 10:
            print(hadrons[0])
    else:
        if np.sum(X_event[n,:,12]!=0.0) == 0.0:
            continue
        multiplicities[n]=np.sum(X_event[n,:,12]!=0.0)
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)    
        hadrons = X_event[n,:int(np.sum(X_event[n,:,12]!=0.0))]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
        if n < 10:
            print(hadrons[0])
    #print(x.shape)
    y = torch.tensor(np.log(y_event[n]),dtype=torch.float)

    data = Data(x=x, y=y,edge_index=edge_index)
    dataset.append(data)
print(len(dataset),Nevents)
print("Min multiplicity")
print(np.min(multiplicities))
### training
batch_size=10000
loader = DataLoader(dataset,batch_size=batch_size, shuffle=False)
model = PointNet(64)
try: model.load_state_dict(torch.load(model_path))
except: print("New model")

model.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

print("MSE with E[weights]")
print(np.mean((y_event-np.mean(y_event))**2),np.mean((np.log(y_event)-np.mean(np.log(y_event)))**2))

train_loss_aux = 1000
times = 0
for epoch in range(1, nepochs):

    loss = train(model, optimizer, loader,Nevents)
    print(f'Epoch: {epoch:02d}, Loss: {loss:.4f}, Learning Rate: {learning_rate:.6f}')
    if epoch == 0:
        train_loss_aux = loss
        times = 0
        continue
    if loss < train_loss_aux:
        train_loss_aux = loss
        times = 0
    else:
        times+=1

    if times == npatience:
        learning_rate*=0.1
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)    
    if times == 2*npatience:
        break  
    torch.save(model.state_dict(),model_path)

log_event_weights_pred, log_event_weights_true = test(model,loader,Nevents)
event_weights_pred, event_weights_true = np.exp(log_event_weights_pred), np.exp(log_event_weights_true)

log_optimal_event_weights_pred, log_optimal_event_weights_true = optimal_weight(model,loader,Nevents)
optimal_event_weights_pred, optimal_event_weights_true = np.exp(log_optimal_event_weights_pred), np.exp(log_optimal_event_weights_true)


print("Event weights")
print(event_weights_true.shape)
print(event_weights_pred.shape)
print(np.mean(event_weights_pred))
print("Pearson r")
print(str(np.round(st.pearsonr(event_weights_true,event_weights_pred),4)[0]))
np.save(results_dir+'learned_event_weights_second_step_train.npy',event_weights_pred)
np.save(results_dir+'learned_optimal_event_weights_train.npy',optimal_event_weights_pred)
print("Hist 1")
plt.hist2d(event_weights_true,event_weights_pred,bins=100)
plt.colorbar()
plt.plot(np.linspace(np.min(event_weights_true),np.max(event_weights_true),10),np.linspace(np.min(event_weights_true),np.max(event_weights_true),10),'k')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$w_{\mathrm{Target}}$')
plt.ylabel('$w_{\mathrm{NN}}$')
plt.title(str(np.round(st.pearsonr(event_weights_true,event_weights_pred),4)[0]))
plt.tight_layout()
plt.savefig(results_dir+'gnn_event_weights.pdf')
plt.clf()

a,b,c = plt.hist(event_weights_true,histtype='step',color='blue',bins=100,label='Target')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color='blue')
a2,b,c = plt.hist(event_weights_pred,histtype='step',color='magenta',bins=b,label='NN')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='magenta')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Weight')
plt.ylabel('Events')
plt.title(str(np.round(st.pearsonr(event_weights_true,event_weights_pred),4)[0]))
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'gnn_event_weights_bis.pdf')
plt.clf()

print("Hist 2")
plt.hist2d(np.log(event_weights_true),np.log(event_weights_pred),bins=100)
plt.colorbar()
plt.plot(np.linspace(np.min(np.log(event_weights_true)),np.max(np.log(event_weights_true)),10),np.linspace(np.min(np.log(event_weights_true)),np.max(np.log(event_weights_true)),10),'k')
plt.xlabel('Log $w_{\mathrm{Target}}$')
plt.ylabel('Log $w_{\mathrm{NN}}$')
plt.title(str(np.round(st.pearsonr(np.log(event_weights_true),np.log(event_weights_pred)),4)[0]))
plt.tight_layout()
plt.savefig(results_dir+'log_gnn_event_weights_high_level.pdf')
plt.clf()

a,b,c = plt.hist(np.log(event_weights_true),histtype='step',color='blue',bins=100,label='Target')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color='blue')
a2,b,c = plt.hist(np.log(event_weights_pred),histtype='step',color='magenta',bins=b,label='NN')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='magenta')
plt.yscale('log')
plt.xlabel('Log Weight')
plt.ylabel('Events')
plt.title(str(np.round(st.pearsonr(np.log(event_weights_true),np.log(event_weights_pred)),4)[0]))
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'log_gnn_event_weights_bis.pdf')
plt.clf()

emission_weights_unscaled, emissions = weight_getter(model,loader)

emission_weights = emission_weights_unscaled
np.save(results_dir+'all_emission_weights_full_frag_train.npy',emission_weights)


ind_px = 5
ind_py = 6


px_min = 1.2*np.min(emissions[:,ind_px])
px_max = 1.2*np.max(emissions[:,ind_px])

py_min = 1.2*np.min(emissions[:,ind_py])
py_max = 1.2*np.max(emissions[:,ind_py])

first_stretch = np.linspace(px_min,np.percentile(emissions[:,ind_px],5),2)
second_stretch = np.array([np.percentile(emissions[:,ind_px],10*i) for i in range(1,9)])
third_stretch = np.linspace(np.percentile(emissions[:,ind_px],10*9),px_max,2)

px_bins = np.append(np.append(first_stretch,second_stretch),third_stretch)

first_stretch = np.linspace(py_min,np.percentile(emissions[:,ind_py],5),2)
second_stretch = np.array([np.percentile(emissions[:,ind_py],10*i) for i in range(1,9)])
third_stretch = np.linspace(np.percentile(emissions[:,ind_py],10*9),py_max,2)

py_bins = np.append(np.append(first_stretch,second_stretch),third_stretch)

sim_px_py_density, _, _, _ = plt.hist2d(emissions[:,ind_px],emissions[:,ind_py],bins=[px_bins, py_bins],density=True);
plt.clf()

sim_resampled_px_py_density, _, _, _ = plt.hist2d(emissions[:,ind_px],emissions[:,ind_py],bins=[px_bins, py_bins],weights=emission_weights,density=True);
plt.clf()

npx_bin = bin_assigner(px_bins,0.0)
npy_bin = bin_assigner(py_bins,0.0)
wpxold_pyold_initial = sim_resampled_px_py_density[npx_bin,npy_bin]/sim_px_py_density[npx_bin,npy_bin]
print("Conditioning weight")
print(wpxold_pyold_initial)

print("All emisions")
print(emission_weights.shape)
print(emissions.shape)
print(np.mean(emission_weights))
print(np.min(emission_weights),np.max(emission_weights))
print("Neff")
print((np.sum(emission_weights))**2/np.sum(emission_weights**2))
print(((np.sum(emission_weights))**2/np.sum(emission_weights**2))/len(emission_weights))

emissions_truth = emissions_truth[:Nevents]

print("Hist 3")
a,b,c = plt.hist(emissions[:,0].flatten(),bins=50,histtype='step',color='blue',label='Simulation')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color='blue')

if stringends == False:
    a2,b,c = plt.hist(emissions_truth[:,:,0].flatten()[[a and b for a,b in zip(emissions_truth[:,:,12].flatten()!=0.0,emissions_truth[:,:,8].flatten()==0.0)]],bins=b,histtype='step',color='red',label='Data')
    plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='red')

else:
    a2,b,c = plt.hist(emissions_truth[:,:,0].flatten()[emissions_truth[:,:,12].flatten()!=0.0],bins=b,histtype='step',color='red',label='Data')
    plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='red')


resampled_events=X_event, y_event
from numpy.random import choice
resampled_events = X_event[choice([i for i in range(0,len(X_event))], Nevents,p=y_event[0:len(X_event)]/np.sum(y_event[0:len(X_event)]))]
if stringends == False:
    resampled_emissions=resampled_events[:,:,0].flatten()[[a and b for a,b in zip(resampled_events[:,:,12].flatten()!=0.0,resampled_events[:,:,8].flatten()==0.0)]]
else:
    resampled_emissions=resampled_events[:,:,0].flatten()[resampled_events[:,:,12].flatten()!=0.0]


a2,b,c= plt.hist(resampled_emissions,bins=b,histtype='step',color='green',label='1st Step')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='green')


a2,b,c= plt.hist(emissions[:,0].flatten(),bins=b,weights=emission_weights,histtype='step',color='magenta',label='Reweighted Sim.')
er2, b = np.histogram(emissions[:,0].flatten(),bins=b,weights=emission_weights**2)
er2 = np.sqrt(er2)
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='magenta')


plt.xlabel('$z$')
plt.ylabel('Events')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'reweighted_emissions_gnn_high_level.pdf')
plt.clf()



if stringends == False:
    nemissions_min = np.min([len(emissions[:,0]),np.sum([a and b for a,b in zip(emissions_truth[:,:,12].flatten()!=0.0,emissions_truth[:,:,8].flatten()==0.0)])])
else:
    nemissions_min = np.min([len(emissions[:,0]),np.sum(emissions_truth[:,:,12].flatten()!=0.0)])

print(nemissions_min,emissions[:,0].shape[0])
a,b,c = plt.hist(emissions[:,0].flatten(),weights=np.ones(len(emissions[:,0]))*nemissions_min/(emissions[:,0].shape[0]),bins=50,histtype='step',color='blue',label='Simulation')

if stringends == False:
    a2,b,c = plt.hist(emissions_truth[:,:,0].flatten()[[a and b for a,b in zip(emissions_truth[:,:,12].flatten()!=0.0,emissions_truth[:,:,8].flatten()==0.0)]],weights=np.ones(int(np.sum([a and b for a,b in zip(emissions_truth[:,:,12].flatten()!=0.0,emissions_truth[:,:,8].flatten()==0.0)])))*nemissions_min/np.sum([a and b for a,b in zip(emissions_truth[:,:,12].flatten()!=0.0,emissions_truth[:,:,8].flatten()==0.0)]),bins=b,histtype='step',color='red',label='Data')

else:
    a2,b,c = plt.hist(emissions_truth[:,:,0].flatten()[emissions_truth[:,:,12].flatten()!=0.0],weights=np.ones(int(np.sum(emissions_truth[:,:,12].flatten()!=0.0)))*nemissions_min/np.sum(emissions_truth[:,:,12].flatten()!=0.0),bins=b,histtype='step',color='red',label='Data')

a2,b,c= plt.hist(resampled_emissions,weights=np.ones(len(resampled_emissions))*nemissions_min/len(resampled_emissions),bins=b,histtype='step',color='green',label='1st Step')


a2,b,c= plt.hist(emissions[:,0].flatten(),bins=b,weights=emission_weights*nemissions_min/(emissions[:,0].shape[0]),histtype='step',color='magenta',label='Reweighted Sim.')

plt.xlabel('$z$')
plt.ylabel('Events')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'reweighted_emissions_gnn_same_number_of_emissions.pdf')
plt.clf()

fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
ax1.set_ylabel('Density')
ax2.set_ylabel('New/Sim')
ax2.set_ylim(0.5,1.5)
plt.xlabel('$z$')
a,b,c = ax1.hist(emissions[:,0].flatten(),bins=50,histtype='step',color='blue',label='Simulation',density=True)
er = np.sqrt(a*(b[1]-b[0])/len(emissions[:,0].flatten()))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color='blue')

if stringends == False:
    a2,b,c = ax1.hist(emissions_truth[:,:,0].flatten()[[a and b for a,b in zip(emissions_truth[:,:,12].flatten()!=0.0,emissions_truth[:,:,8].flatten()==0.0)]],bins=b,histtype='step',color='red',label='Data',density=True)
    er2 = np.sqrt(a2*(b[1]-b[0])/np.sum([a and b for a,b in zip(emissions_truth[:,:,12].flatten()!=0.0,emissions_truth[:,:,8].flatten()==0.0)]))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='red')
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='red')    

else:
    a2,b,c = ax1.hist(emissions_truth[:,:,0].flatten()[emissions_truth[:,:,12].flatten()!=0.0],bins=b,histtype='step',color='red',label='Data',density=True)
    er2 = np.sqrt(a2*(b[1]-b[0])/np.sum(emissions_truth[:,:,12].flatten()!=0.0))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='red')
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='red')

a2,b,c = ax1.hist(resampled_emissions,bins=b,histtype='step',color='green',label='1st Step',density=True)
er2 = np.sqrt(a2*(b[1]-b[0])/len(resampled_emissions))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='green')
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='green')    


a2,b,c = ax1.hist(emissions[:,0].flatten(),bins=b,weights=emission_weights,histtype='step',color='magenta',label='Reweighted Sim.',density=True)
a2_aux, b = np.histogram(emissions[:,0].flatten(),bins=b,weights=emission_weights)
er2, b = np.histogram(emissions[:,0].flatten(),bins=b,weights=emission_weights**2)
er2=np.sqrt(er2*(1.0/np.sum(emission_weights))**2+((a2_aux/(np.sum(emission_weights))**2)**2) *(np.sum(emission_weights**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='magenta')
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='magenta')
ax1.legend(loc='upper right')
ax2.axhline(1.0,linestyle='dashed',color='black')
plt.savefig(results_dir+'reweighted_emissions_gnn_ratio.pdf')
plt.clf()



nconst=0
first_emission_weights=np.zeros(Nevents)
first_emissions=np.zeros((Nevents,emissions.shape[1]))
for n in range(Nevents):
    first_emission_weights[n]=emission_weights[nconst]#/wpxold_pyold_initial
    first_emissions[n]=emissions[nconst]
    nconst+=int(multiplicities[n])
print(nconst,len(emissions))
print("First emissions")
print(first_emission_weights.shape)
print(np.mean(first_emission_weights))
print("Neff")
print((np.sum(first_emission_weights))**2/np.sum(first_emission_weights**2))
print(((np.sum(first_emission_weights))**2/np.sum(first_emission_weights**2))/len(first_emission_weights))
print("Hist 4")
a,b,c = plt.hist(first_emissions[:,0].flatten(),bins=np.linspace(0,1,50),histtype='step',color='blue',label='Simulation')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color='blue')

if stringends == False:
    a2,b,c=plt.hist(emissions_truth[:,:1,0].flatten()[emissions_truth[:,:1,8].flatten()==0.0],bins=b,histtype='step',color='red',label='Data')
    print("I have these many first emissions in data")
    print(np.sum(emissions_truth[:,:1,8].flatten()==0.0))
    print(np.sum(a2))
    plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='red')

else:
    a2,b,c=plt.hist(emissions_truth[:,:1,0].flatten(),bins=b,histtype='step',color='red',label='Data')
    print("I have these many first emissions in data")
    print(len(emissions_truth[:,:1,0]))
    print(np.sum(a2))
    plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='red')

a2,b,c=plt.hist(first_emissions[:,0].flatten(),bins=b,weights=y_event,histtype='step',color='green',label='Reweighted Sim. 1st Step')
er2, b = np.histogram(first_emissions[:,0].flatten(),bins=b,weights=y_event**2)
er2 = np.sqrt(er2)
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='green')

a2,b,c=plt.hist(first_emissions[:,0].flatten(),bins=b,weights=first_emission_weights,histtype='step',color='magenta',label='Reweighted Sim.')
er2, b = np.histogram(first_emissions[:,0].flatten(),bins=b,weights=first_emission_weights**2)
er2 = np.sqrt(er2)
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='magenta')


plt.xlabel('$z$')
plt.ylabel('Events')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'reweighted_first_emissions_gnn_high_level.pdf')
plt.clf()

fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
ax1.set_ylabel('Density')
ax2.set_ylabel('New/Sim')
ax2.set_ylim(0.5,1.5)
plt.xlabel('$z$')
a,b,c = ax1.hist(first_emissions[:,0].flatten(),bins=np.linspace(0,1,50),histtype='step',color='blue',label='Simulation',density=True)
er = np.sqrt(a*(b[1]-b[0])/len(first_emissions[:,0].flatten()))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color='blue')

if stringends == False:
    a2,b,c = ax1.hist(emissions_truth[:,:1,0].flatten()[emissions_truth[:,:1,8].flatten()==0.0],bins=b,histtype='step',color='red',label='Data',density=True)
    er2 = np.sqrt(a2*(b[1]-b[0])/np.sum(emissions_truth[:,:1,8].flatten()==0.0))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='red')
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='red')

else:
    a2,b,c = ax1.hist(emissions_truth[:,:1,0].flatten(),bins=b,histtype='step',color='red',label='Data',density=True)
    er2 = np.sqrt(a2*(b[1]-b[0])/len(emissions_truth[:,:1,8].flatten()))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='red')
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='red')


a2,b,c = ax1.hist(first_emissions[:,0].flatten(),bins=b,weights=y_event,histtype='step',color='green',label='1st Step',density=True)
a2_aux, b = np.histogram(first_emissions[:,0].flatten(),bins=b,weights=y_event)
er2, b = np.histogram(first_emissions[:,0].flatten(),bins=b,weights=y_event**2)
er2=np.sqrt(er2*(1.0/np.sum(y_event))**2+((a2_aux/(np.sum(y_event))**2)**2) *(np.sum(y_event**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='green')
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='green')

a2,b,c = ax1.hist(first_emissions[:,0].flatten(),bins=b,weights=first_emission_weights,histtype='step',color='magenta',label='Reweighted Sim.',density=True)
a2_aux, b = np.histogram(first_emissions[:,0].flatten(),bins=b,weights=first_emission_weights)
er2, b = np.histogram(first_emissions[:,0].flatten(),bins=b,weights=first_emission_weights**2)
er2=np.sqrt(er2*(1.0/np.sum(first_emission_weights))**2+((a2_aux/(np.sum(first_emission_weights))**2)**2) *(np.sum(first_emission_weights**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='magenta')
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='magenta')

ax1.legend(loc='upper right')
ax2.axhline(1.0,linestyle='dashed',color='black')
plt.savefig(results_dir+'reweighted_first_emissions_gnn_ratio.pdf')
plt.clf()

nconst=0
second_emission_weights=np.zeros(Nevents)
second_emissions=np.zeros((Nevents,emissions.shape[1]))
for n in range(Nevents):
    if multiplicities[n]==1:
        print("Single emission event")
        nconst+=int(multiplicities[n])
        continue
    second_emission_weights[n]=emission_weights[nconst+1]
    second_emissions[n]=emissions[nconst+1]
    nconst+=int(multiplicities[n])
print(nconst,len(emissions))                            
print("Second emissions")
print(second_emissions.shape)
print(np.mean(second_emission_weights))
print("Neff")
print((np.sum(second_emission_weights))**2/np.sum(second_emission_weights**2))
print(((np.sum(second_emission_weights))**2/np.sum(second_emission_weights**2))/len(first_emission_weights))
print("Hist 4")
a,b,c = plt.hist(second_emissions[:,0].flatten(),bins=np.linspace(0,1,50),histtype='step',color='blue',label='Simulation')
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=np.sqrt(a),fmt='none',color='blue')

if stringends == False:
    a2,b,c=plt.hist(emissions_truth[:,1:2,0].flatten()[[a and b for a,b in zip(emissions_truth[:,1:2,12].flatten()!=0.0,emissions_truth[:,1:2,8].flatten()==0.0)]],bins=b,histtype='step',color='red',label='Data')
    plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='red')

else:
    a2,b,c=plt.hist(emissions_truth[:,1:2,0].flatten()[emissions_truth[:,1:2,12].flatten()!=0.0],bins=b,histtype='step',color='red',label='Data')
    plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=np.sqrt(a2),fmt='none',color='red')


a2,b,c=plt.hist(second_emissions[:,0].flatten(),bins=b,weights=y_event,histtype='step',color='green',label='1st Step')
er2, b = np.histogram(second_emissions[:,0].flatten(),bins=b,weights=y_event**2)
er2 = np.sqrt(er2)
plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='green')

a2,b,c=plt.hist(second_emissions[:,0].flatten(),bins=b,weights=second_emission_weights,histtype='step',color='magenta',label='Reweighted Sim.')
er2, b = np.histogram(second_emissions[:,0].flatten(),bins=b,weights=second_emission_weights**2)
er2 = np.sqrt(er2)

plt.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='magenta')
plt.xlabel('$z$')
plt.ylabel('Events')
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig(results_dir+'reweighted_second_emissions_gnn_high_level.pdf')
plt.clf()

fig, (ax1, ax2) = plt.subplots(2,figsize=(2*4,2*3),gridspec_kw=dict(height_ratios=[3, 1]),sharex=True,layout="constrained")
ax1.set_ylabel('Density')
ax2.set_ylabel('New/Sim')
ax2.set_ylim(0.5,1.5)
plt.xlabel('$z$')
a,b,c = ax1.hist(second_emissions[:,0].flatten(),bins=np.linspace(0,1,50),histtype='step',color='blue',label='Simulation',density=True)
er = np.sqrt(a*(b[1]-b[0])/len(second_emissions[:,0].flatten()))*np.sqrt(1+a*(b[1]-b[0]))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a,yerr=er,fmt='none',color='blue')

if stringends == False:
    a2,b,c = ax1.hist(emissions_truth[:,1:2,0].flatten()[[a and b for a,b in zip(emissions_truth[:,1:2,12].flatten()!=0.0,emissions_truth[:,1:2,8].flatten()==0.0)]],bins=b,histtype='step',color='red',label='Data',density=True)
    er2 = np.sqrt(a2*(b[1]-b[0])/np.sum([a and b for a,b in zip(emissions_truth[:,1:2,12].flatten()!=0.0,emissions_truth[:,1:2,8].flatten()==0.0)]))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='red')
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='red')

else:
    a2,b,c = ax1.hist(emissions_truth[:,1:2,0].flatten()[emissions_truth[:,1:2,12].flatten()!=0.0],bins=b,histtype='step',color='red',label='Data',density=True)
    er2 = np.sqrt(a2*(b[1]-b[0])/np.sum(emissions_truth[:,1:2,12].flatten()!=0.0))*np.sqrt(1+a2*(b[1]-b[0]))/(b[1]-b[0])
    ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='red')
    er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
    ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='red')

a2,b,c = ax1.hist(second_emissions[:,0].flatten(),bins=b,weights=y_event,histtype='step',color='green',label='1st Step',density=True)
a2_aux, b = np.histogram(second_emissions[:,0].flatten(),bins=b,weights=y_event)
er2, b = np.histogram(second_emissions[:,0].flatten(),bins=b,weights=y_event**2)
er2=np.sqrt(er2*(1.0/np.sum(y_event))**2+((a2_aux/(np.sum(y_event))**2)**2) *(np.sum(y_event**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='green')
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='green')

a2,b,c = ax1.hist(second_emissions[:,0].flatten(),bins=b,weights=second_emission_weights,histtype='step',color='magenta',label='Reweighted Sim.',density=True)
a2_aux, b = np.histogram(second_emissions[:,0].flatten(),bins=b,weights=second_emission_weights)
er2, b = np.histogram(second_emissions[:,0].flatten(),bins=b,weights=second_emission_weights**2)
er2=np.sqrt(er2*(1.0/np.sum(second_emission_weights))**2+((a2_aux/(np.sum(second_emission_weights))**2)**2) *(np.sum(second_emission_weights**2)))/(b[1]-b[0])
ax1.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2,yerr=er2,fmt='none',color='magenta')
er_ratio = np.sqrt((er2/a)**2+((a2/a)*(er/a))**2)
ax2.errorbar(x=0.5*(b[1:]+b[:-1]),y=a2/a,yerr=er_ratio,color='magenta')

ax1.legend(loc='upper right')
ax2.axhline(1.0,linestyle='dashed',color='black')
plt.savefig(results_dir+'reweighted_second_emissions_gnn_ratio.pdf')
plt.clf()
#'''

print("Test normalization of emisions")
pxOld_value = emissions[1,5]
indexes_pxOld_value = np.abs(emissions[:,5]-pxOld_value)<0.1
pyOld_value = emissions[1,6]
indexes_pyOld_value = np.abs(emissions[:,6]-pyOld_value)<0.1
print("pxOld")
print(pxOld_value,np.sum(indexes_pxOld_value),np.mean(indexes_pxOld_value))
print("pyOld")
print(pyOld_value,np.sum(indexes_pyOld_value),np.mean(indexes_pyOld_value))
Old_indexes = [a and b for a,b in zip(indexes_pxOld_value,indexes_pyOld_value)]
print(np.sum(Old_indexes),np.mean(Old_indexes))



print(emission_weights[Old_indexes].shape)
print(emissions[Old_indexes].shape)
print(np.mean(emission_weights[Old_indexes]))
print(np.min(emission_weights[Old_indexes]),np.max(emission_weights[Old_indexes]))
print("Neff")
print((np.sum(emission_weights[Old_indexes]))**2/np.sum(emission_weights[Old_indexes]**2))
print(((np.sum(emission_weights[Old_indexes]))**2/np.sum(emission_weights[Old_indexes]**2))/len(emission_weights[Old_indexes]))


print("Test set")
X_event_test = np.load(sys.argv[10])
history_indexes_test = np.load(sys.argv[11])
X_event_test = np.concatenate((X_event_test,history_indexes_test.reshape((X_event_test.shape[0],X_event_test.shape[1],1))),axis=2)

### Hadronization scaling
X_event_test[:,:,3]=X_event_test[:,:,3]/(0.140)
#X_event[:,:,3]=X_event[:,3]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event_test[:,:,4]=np.where(X_event_test[:,:,4]==0,-1,X_event_test[:,:,4])
#X_event[:,:,4]=X_event[:,:,4]+0.1*np.random.randn((X_event.shape[0],X_event.shape[1]))
X_event_test[:,:,7]=X_event_test[:,:,7]/5.0
### Four momenta scaling

X_event_test[:,:,5]=X_event_test[:,:,5]/1.0
X_event_test[:,:,6]=X_event_test[:,:,6]/1.0

X_event_test[:,:,9]=X_event_test[:,:,9]/1.0
X_event_test[:,:,10]=X_event_test[:,:,10]/1.0
X_event_test[:,:,11]=X_event_test[:,:,11]/25.0
X_event_test[:,:,12]=X_event_test[:,:,12]/25.0


dataset_test = []

for n in range(len(X_event_test)):
    if stringends == False:
        if np.sum(X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0)),8]==0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)   
        hadrons = X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0))]
        hadrons = hadrons[X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0)),8]==0.0]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
    else:
        if np.sum(X_event_test[n,:,12]!=0.0) == 0.0:
            continue
        edge_index = torch.tensor([[],
                                []], dtype=torch.long)    
        hadrons = X_event_test[n,:int(np.sum(X_event_test[n,:,12]!=0.0))]
        hadrons = np.array([np.delete(hadron,8) for hadron in hadrons])
        x = torch.tensor(hadrons, dtype=torch.float)
        if n < 10:
            print(hadrons[0])
    #print(x.shape)
    y = torch.tensor(0.0,dtype=torch.float)

    data = Data(x=x, y=y,edge_index=edge_index)
    dataset_test.append(data)

loader_test = DataLoader(dataset_test,batch_size=batch_size, shuffle=False)


log_event_weights_pred_test, log_event_weights_true_test = test(model,loader_test,len(X_event_test))
event_weights_pred_test, event_weights_true_test = np.exp(log_event_weights_pred_test), np.exp(log_event_weights_true_test)

log_optimal_event_weights_pred_test, log_optimal_event_weights_true_test = optimal_weight(model,loader_test,len(X_event_test))
optimal_event_weights_pred_test, optimal_event_weights_true_test = np.exp(log_optimal_event_weights_pred_test), np.exp(log_optimal_event_weights_true_test)


print(np.mean(event_weights_pred_test))
print(np.mean(optimal_event_weights_pred_test))
np.save(results_dir+'learned_event_weights_second_step_test.npy',event_weights_pred_test)
np.save(results_dir+'learned_optimal_event_weights_test.npy',optimal_event_weights_pred_test)

emission_weights_unscaled_test, emissions_test = weight_getter(model,loader_test)

emission_weights_test = emission_weights_unscaled_test
print(np.mean(emission_weights_test))
np.save(results_dir+'all_emission_weights_full_frag_test.npy',emission_weights_test)
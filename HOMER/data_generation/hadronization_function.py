import numpy as np
import scipy.stats as st
import sys

import pythia8
#from pythia8 import Pythia, Vec4, Event, StringZ, StringEnd, ColSinglet, HadronLevel
from pythia8 import Pythia, Vec4, Event
from math import pi, sqrt, cos, sin


###############################################################################

def sqrtPos(val):
    """
    Returns 0 if val is negative, otherwiswe the sqrt.
    """
    return sqrt(val) if val > 0 else 0.



# Write own derived UserHooks class.

class MyUserHooks(pythia8.UserHooks):

    # Constructor creates anti-kT jet finder with (-1, R, pTmin, etaMax).
    def __init__(self,sigmaQ, aLund, bLund,aExtraSQuark,aExtraDiquark,rFactC,rFactB):
        #print("Initializing User Hooks")
        pythia8.UserHooks.__init__(self)
        self.counter = 0
        self.counterPos = 0
        self.counterNeg = 0
        self.abs_counter = 0
        self.supremum = 11.0#self.supremum_pT * self.supremum_z
        self.sigmaQ = sigmaQ
        self.aLund = aLund
        self.bLund = bLund
        self.aExtraSQuark = aExtraSQuark
        self.aExtraDiquark = aExtraDiquark
        self.rFactC = rFactC
        self.rFactB = rFactB
        self.splits = []

    # Allow to change Fragmentation parameters...
    def canVetoProcessLevel(self):
        #print("Calling canVetoProcessLevel")
        return True

    # This changes the parameters of the Pythia models
    def doVetoProcessLevel(self, process):
        #print("Calling doProcessLevel")
        last_particle_id = np.abs(process.back().id())
        #print(last_particle_id)
        if last_particle_id >= 3:
            return True
        return False
        
    # Allow to change Fragmentation parameters...
    def canChangeFragPar(self):
        #print("Calling canChangeFragPar")
        return True

    # This changes the parameters of the Pythia models
    def doChangeFragPar(self, flavPtr, zPtr, pTPtr, idEnd, m2Had,iParton, sEnd):
        return True

    # This vetos a given hadron on a given string end
    def doVetoFragmentation(self, *args):#had, sEnd):
        #print("Calling doVetoFragmentation")
        # check whether we are in a final hadronization or not
        if(len(args)==2):# and self.counter < 1):
            # we have Hadron, StringEnd
            zHad = args[1].zHad
            mT2Had = args[1].mT2Had
            pTHad = np.sqrt((args[1].pxHad-args[1].pxOld)**2+(args[1].pyHad-args[1].pyOld)**2)

            self.abs_counter += 1
            #print(pTHad,np.sqrt(args[1].pxHad**2+args[1].pyHad**2),np.sqrt(args[0].px()**2+args[0].py()**2))
            if np.allclose(args[1].mHad**2+args[1].pxHad**2+args[1].pyHad**2,mT2Had) == False:
                print("Wrong mT2 definition")
                print(args[1].mHad**2+args[1].pxHad**2+args[1].pyHad**2,mT2Had)
                
            self.splits.append([args[1].zHad,args[1].pxNew,args[1].pyNew,args[1].mHad,args[1].fromPos,args[1].pxOld,args[1].pyOld,args[1].GammaOld,0.0,args[0].px(),args[0].py(),args[0].pz(),args[0].e()])
            self.counter+=1
            if args[1].fromPos:
                self.counterPos+=1
            else:
                self.counterNeg+=1
            pass
        else:
            # we have had1, had2, s1, s2
            for nHad in range(2):
                zHad = args[nHad+2].zHad 
                mT2Had = args[nHad+2].mT2Had
                pTHad = np.sqrt((args[nHad+2].pxHad-args[nHad+2].pxOld)**2+(args[nHad+2].pyHad-args[nHad+2].pyOld)**2)
                self.abs_counter += 1
                self.splits.append([args[nHad+2].zHad,args[nHad+2].pxNew,args[nHad+2].pyNew,args[nHad+2].mHad,args[nHad+2].fromPos,args[nHad+2].pxOld,args[nHad+2].pyOld,args[nHad+2].GammaOld,1.0,args[nHad].px(),args[nHad].py(),args[nHad].pz(),args[nHad].e()])
                self.counter+=1

        return False




###############################################################################
class Reweighted_Hadronization:
    """
    Provides a simple Reweighted Hadronization chains for hadronic systems using Pythia.

    pythia:  internal Pythia object.
    event:   Pythia event.
    process: Pythia process (the initial setup).
    strings: Pythia event containing the strings, if available.
    pdb:     particle information database.
    rng:     random number generator.
    """
    ###########################################################################
    def __init__(self, sigmaQ=0.335/np.sqrt(2), aLund = 0.68, bLund = 0.98,seed = 1, aLund_alt=0.30,cmds = None):
        """
        Configure and initialize the internal Pythia instance, particle
        data table, and random number generator.

        seed: random number generator seed.
        cmds: optionally pass commands for configuration.
        """
        self.pythia = Pythia("", False)
        if cmds == None: cmds = []
        cfg = ["ProcessLevel:all = off", "HadronLevel:Decay = off",
               "Next:numberShowInfo = 0", "Next:numberShowProcess = 0",
               "Next:numberShowEvent = 0", "Print:quiet = off",
               "StringFragmentation:TraceColours = on","Fragmentation:setVertices = off", "Check:event = false",
               "111:mayDecay = false","211:mayDecay = false",
               "TimeShower:nGluonToQuark = 0",
               "TimeShower:nGammaToQuark = 0",
               "TimeShower:nGammaToLepton = 0",
               "TimeShower:QEDshowerByQ = off",
               "TimeShower:QEDshowerByL = off",
               "TimeShower:QEDshowerByOther = off",
               "TimeShower:QEDshowerByGamma = off",
               "113:m0 = 100.",
               "115:m0 = 100.",
               "213:m0 = 100.",
               "215:m0 = 100.",
               "Random:setSeed = true",  "Random:seed = %i" % seed ,
               "StringFlav:probStoUD=0.",
               "StringFlav:probQQtoQ = 0.0",
               "StringFlav:mesonUDvector=0.",
               "StringFlav:etaSup=0.0",
               "StringFlav:etaPrimeSup=0.0",
               "StringPT:sigma="+str(sigmaQ*np.sqrt(2)),
               "StringZ:aLund = "+str(aLund),
               "StringZ:bLund = "+str(bLund),
               "StringZ:aExtraSQuark = 0.0",
               "StringZ:aExtraDiquark = 0.0",
               "StringZ:rFactC = 0.0",
               "StringZ:rFactB = 0.0",
               "VariationFrag:List = {a=alt frag:aLund="+str(aLund_alt)+"}"]

        for cmd in cfg + cmds: self.pythia.readString(cmd)
        self.sigmaQ = sigmaQ
        self.aLund = aLund
        self.bLund = bLund
        self.myUserHooks = MyUserHooks(sigmaQ, aLund, bLund,0,0,0,0)
        self.pythia.setUserHooksPtr(self.myUserHooks)
        self.pythia.init()
        self.event   = self.pythia.event
        self.process = self.pythia.process
        self.pdb     = self.pythia.particleData
        self.rng     = self.pythia.rndm
        try: self.strings = self.pythia.strings
        except: pass
    
    ###########################################################################
    def next(self):
        """
        Simple method to do the filling of partons into the event record.

        pe:    parton energy in GeV.
        pid:   particle ID to generate if mode is 0.
        """
        # Reset event record to allow for new event.
        self.event.reset()
        
        try: self.strings.reset()
        except: pass
        try: self.myUserHooks.counter = 0
        except: pass
        try: self.myUserHooks.counterPos = 0
        except: pass
        try: self.myUserHooks.counterNeg = 0
        except: pass
        try: self.myUserHooks.abs_counter = 0
        except: pass
        try: self.myUserHooks.splits = []
        except: pass

        # ID of q
        pid = 2

        # energy of each quark
        pe = 45.0

        # x-momentum of each quark
        px = 0.0

        # mass of each quark
        pm  = self.pythia.particleData.m0(pid)

        # resulting pz
        pp  = sqrtPos(pe*pe - pm*pm -px*px)
        '''
        # add particles to event record
        eFrac = 0.15
        eGluon = eFrac * 2 * pe
        eQuark = pe * (1.0 - eFrac )
        pZQsq = eQuark**2 - 0.25*(eGluon**2) 
        pZQ   = sqrtPos(pZQsq)
        self.pythia.event.append(  pid, 23, 101,   0, -0.5*eGluon, 0.0 , pZQ, eQuark )
        self.pythia.event.append( 21, 23, 102, 101,  1.0*eGluon, 0.0 , 0.0, eGluon )
        self.pythia.event.append( -pid, 23,   0, 102, -0.5*eGluon, 0.0 ,-pZQ, eQuark )
        '''

        self.pythia.event.append(  pid, 23, 101,   0, -px, 0,  -pp, pe, pm,pe)
        self.pythia.event.append( -pid, 23,   0, 101, px, 0., pp, pe, pm,pe)
        # Copy the event to the process.
        self.pythia.process = Event(self.pythia.event)
        #self.pythia.forceTimeShower(1,2,pe)#,1)
        #self.pythia.forceHadronLevel()

        # Hadronize the event.
        self.pythia.next()
        #return self.pythia.next()

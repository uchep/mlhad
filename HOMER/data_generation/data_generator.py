import numpy as np
import os
import matplotlib.pyplot as plt
import scipy.stats as st
from hadronization_function import *
import pythia8
import time
from pythia8 import Pythia, Vec4, Event, Sphericity, Thrust
sph = Sphericity(powerIn=1)# powerIn = 1 to get linear version for C and D parameter, powerIn = 2 to get sphericity
thr = Thrust()

try: seed = int(sys.argv[6])
except: seed = int(42)
print(seed)

def event_info_unbinned(Event):
    
    list_of_info = np.zeros((100,5))
    nparticle = 0
    for prt in Event:
        ### check that particle is produced from string fragmentation
        if (prt.status() == 83.0 or prt.status() ==84.0):
            list_of_info[int(nparticle)]=[prt.px(),prt.py(),prt.pz(),prt.e(),np.sign(prt.id())]
            nparticle+=1.0
        ### at most 100 particles
        if nparticle >= 100:
            break
    return list_of_info


def event_info(Event):
    # Thrust, C, D, BW, BT = 5
    # particle multiplicity + charged particle multiplicty = 2
    # first three momentums of the momentum fraction for all and for charged particles = 3 + 3
    
    list_of_info = np.zeros(5+2+6)
    sph.analyze(Event)
    thr.analyze(Event)
    sph_eigenvalues = [sph.eigenValue(i) for i in range(1,4)]
    list_of_info[0] = thr.thrust()
    nThrust = np.array([thr.eventAxis(1).px(),thr.eventAxis(1).py(),thr.eventAxis(1).pz()])
    
    list_of_info[1] = 3 * (sph_eigenvalues[0]*sph_eigenvalues[1]+sph_eigenvalues[1]*sph_eigenvalues[2]+sph_eigenvalues[0]*sph_eigenvalues[2])
    list_of_info[2] = 27 * sph_eigenvalues[0]*sph_eigenvalues[1]*sph_eigenvalues[2]
    nFinal = 0
    nCharged = 0
    Bplus = 0.0
    Bminus = 0.0
    totalmom = 0.0
    final_list = np.zeros(100)
    charged_list = np.zeros(100)

    for prt in Event:

        if prt.isFinal():
            final_list[int(nFinal)] = np.abs(np.log(2.0*np.sqrt(prt.pz()**2+prt.pT()**2)/(91.2)))
            nFinal+=1.0
             
            if prt.isVisible():
                pParticle =  np.array([prt.px(),prt.py(),prt.pz()])
                hemisphere = np.dot(pParticle,nThrust)
                modP = np.sqrt(np.sum(pParticle**2))
                totalmom += modP
                cross_product = np.cross(pParticle,nThrust)
                if hemisphere > 0:
                    Bplus += np.sqrt(np.sum(cross_product**2))
                else:
                    Bminus += np.sqrt(np.sum(cross_product**2))
            if prt.isCharged():
                charged_list[int(nCharged)]=np.abs(np.log(2.0*np.sqrt(prt.pz()**2+prt.pT()**2)/(91.2)))
                nCharged+=1.0
    Bplus = Bplus/(2*totalmom)
    Bminus = Bminus/(2*totalmom)
    list_of_info[3] = np.max([Bplus,Bminus])
    list_of_info[4] = Bplus+Bminus
    list_of_info[5] = nFinal
    list_of_info[6] = nCharged
    list_of_info[6+1] = np.mean(final_list[:int(nFinal)])
    list_of_info[6+3+1] = np.mean(charged_list[:int(nCharged)])
    for nmoment in range(2,1+3):
        list_of_info[6+nmoment] = st.moment(final_list[:int(nFinal)],moment=nmoment)
        list_of_info[6+3+nmoment] = st.moment(charged_list[:int(nCharged)],moment=nmoment)
    
    return list_of_info


### Run a given model
results_dir = str(sys.argv[1])
nevents = int(sys.argv[2])

# this is for lund string model closure test, params are aLund, bLund, sigmaQ, aExtraSQuark, aExtraDiquark, rFactC, rFactB

is_data = int(sys.argv[5]) # 0 for sim, 1 for data

if is_data == 0:
    default_params = np.array([float(sys.argv[3]),0.98,0.335/np.sqrt(2),float(sys.argv[4])])
else:
    default_params = np.array([float(sys.argv[4]),0.98,0.335/np.sqrt(2),float(sys.argv[3])])

start = time.time()

default_run = Reweighted_Hadronization(aLund = default_params[0],bLund = default_params[1],sigmaQ=default_params[2],aLund_alt=default_params[3],seed=seed)

obs_dim = 5+2+6
all_obs = np.zeros((int(nevents),int(obs_dim)))


splits_dim = 100
individual_split_dim = 9 + 4
all_splits = np.zeros((int(nevents),int(splits_dim),individual_split_dim))

all_weights=np.zeros(int(nevents))

history_indexes = -np.ones((int(nevents),int(splits_dim)))

nevent = 0
nevent_true = 0
for nevent in range(int(nevents)):
    default_run.next()
    splits_aux = default_run.myUserHooks.splits 
    if len(splits_aux) > 0 and len(splits_aux)<=splits_dim:
        info_per_event = event_info_unbinned(default_run.pythia.event)

        all_splits[nevent_true,:len(splits_aux)]=np.array(splits_aux)

        info_per_event = event_info(default_run.pythia.event)
        all_obs[nevent_true]=info_per_event

        info = default_run.pythia.infoPython()
        all_weights[nevent_true] = info.getGroupWeight(0)

        history_index = -1
        fromPos = 0
        fromNeg = 0
        for nhadron, hadron in enumerate(all_splits[nevent_true]):
            if hadron[-1] == 0.0: # zero energy means end of string
                break
            elif hadron[8]>=0.0: ## Let's use stringends

                if hadron[5]==0 and hadron[6]==0 and hadron[7]==0:
                    if (fromPos > 0 and hadron[4]==1) or (fromNeg > 0 and hadron[4] == 0): ### this doesn't work if all emissions come from a single end and finalTwo is called and fails
                        fromPos = 0
                        fromNeg = 0 
                if fromPos+fromNeg == 0:
                    history_index+=1
                if hadron[4]==1:
                    fromPos+=1
                else:
                    fromNeg+=1
                history_indexes[nevent_true,nhadron]=history_index

        nevent_true+=1
print(nevents,nevent_true)
all_splits = all_splits[:nevent_true]
all_obs = all_obs[:nevent_true]
all_weights=all_weights[:nevent_true]
print("Mean weight should be close to 1")
print(np.mean(all_weights))
history_indexes=history_indexes[:nevent_true]
print("Acceptance probability")
print(len(history_indexes)/(np.sum(np.max(history_indexes,1)+1)))
print(1/(1+np.mean(np.max(history_indexes,1))))
print("Reject probability")
print(1-len(history_indexes)/(np.sum(np.max(history_indexes,1)+1)))

np.save(results_dir+'splits_for_full_hadron_info.npy',all_splits)
np.save(results_dir+'hadrons_obs_only.npy',all_obs)
if is_data == 0:
    np.save(results_dir+'analytical_log_weight.npy',np.log(all_weights))
else:
    np.save(results_dir+'analytical_log_weight.npy',-np.log(all_weights))
np.save(results_dir+'history_indexes.npy',history_indexes)
print('It took', time.time()-start, 'seconds.')

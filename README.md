<p align="center">
  <img src="MLHAD_logo.svg" alt="drawing" width="300" />
</p>

This repository contains the public codes associated with "__Modeling hadronization using machine learning__" ([2203.04983](https://arxiv.org/abs/2203.04983)), "__Towards a data-driven model of hadronization with normalizing flows__" ([2311.09296](https://arxiv.org/pdf/2311.09296.pdf)) and "__Describing Hadronization via Histories and Observables for Monte-Carlo Event Reweighting__" ([2410.06342](https://arxiv.org/abs/2410.06342)) written by the [MLHad collaboration](https://uchep.gitlab.io/mlhad-docs/). Each public code may be found in ```cSWAE/```, ```BNF/``` and ```HOMER/``` respectively. For more details please see the associated papers, documentation, and examples provided in each subdirectory.

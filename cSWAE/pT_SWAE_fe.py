"""
# pT_SWAE_fe.py is a part of the MLHAD package.
# Copyright (C) 2022 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import torch
from torch.utils.data import DataLoader
from torch import nn
from cSWAE import cSWAE
import matplotlib.pyplot as plt
from math import atan, asinh, sqrt
import numpy as np
from scipy.stats import skewnorm
from sklearn.model_selection import train_test_split
import time
import shutil
import seaborn as sns
import os
import pickle
import csv
sns.set()
sns.set_style("ticks")
sns.set_context("paper", font_scale = 3.0)
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 25

def load_pT_data(file_name = 'data/pT_data.csv'):
    """
    Load in Pythia pz data from file_name and generate labels

    Returns pz and label lists
    """
    pT,labels = [],[]
    with open(file_name, 'r') as csvfile:
        # Create csv reader object
        csvreader = csv.reader(csvfile, quoting = csv.QUOTE_NONNUMERIC)

        # Extract row by row
        for row in csvreader:
            pT.append(row)

    # Create labels, in this case to a value which will not effect the training
    vec_len = len(pT[0])
    N = len(pT)*vec_len
    for i in range(int(N/vec_len)):
        c = np.full((1,2),[1.0, 1.0])
        labels.append(c)
    
    # Reshape labels 
    labels = np.array(labels).reshape(int(N/vec_len),2)

    return pT, labels

def skew_gauss(batch_size, lat_dim=2, c_label = None):
    """
    Produces (batch_size, lat_dim)-dimensional vectors sampled from a skewed-gaussian probability distribution used
    to compute the Sliced Wasserstein distance.

    Returns a torch array of size (batch_size, lat_dim) 
    """
    # a = skewness, loc = mean, scale = standard deviation: parameter chosen based on fit to pT distribution
    z = np.array(skewnorm.rvs(a=4.2592,loc=0.0988,scale=0.2570, size=(batch_size,lat_dim)))
    return torch.from_numpy(z).type(torch.FloatTensor)

def triangular(batch_size, lat_dim, c_label = None):
    """
    Produces (batch_size, lat_dim)-dimensional vectors sampled from a triangular probability distribution used
    to compute the Sliced Wasserstein distance.

    a: lower bound
    b: peak
    d: upper bound

    Returns a torch array of size (batch_size, lat_dim) 
    """
    z = np.random.triangular(0, 0.3, 1, (batch_size, lat_dim))
    return torch.from_numpy(z).type(torch.FloatTensor)

def save_checkp(state, filename):
    f_path = 'saved_models/'+filename
    torch.save(state, f_path)

class cd:
    """
    Context manager for changing the current working directory
    """
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

class xyDataset(torch.utils.data.Dataset):
    """
    Joins the x and y into a dataset, so that it can be used by the pythorch syntax.
    """

    def __init__(self, x, y):
        self.x = torch.tensor(x).to(torch.float)
        self.y = torch.tensor(y).to(torch.float)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        sample = [self.x[idx], self.y[idx]]
        return sample

def results(model_name, p_orig, p_gen):
    """
    This function takes in orginal and generated data for plotting and analysis. The function will create a model
    folder and within all plots will be saved along with the trained model .pth file which is used to generate kinemtaic
    data in frag_chain.py.

    model_name: Path to and name of folder to place produced figures and model file
    p_orig:     List of Pythia-generated data to be compared with model generated data
    p_gen:      List of model-generated data to be compared with Pythia

    Returns None
    """
    # Create a directory name to hold all auxiliary files
    procfolder = model_name
    
    # Create the directory
    os.makedirs(procfolder, exist_ok = True)
    # cd into the directory and stay there until all of the diagrams have been created
    with cd(procfolder):
        bins_p = np.histogram(np.hstack((p_orig.flatten(), p_gen.flatten())), bins=60)[1]
        bins_lat = np.histogram(np.hstack((z_restruc.flatten(),lat_all)), bins=60)[1]
        
        # Summary plot showing model generated pz vs Pythia and target vs generated latent space
        fig, (ax1,ax2) = plt.subplots(1,2,figsize=(16,8))
        ax1.hist(p_orig, bins_p, label = r'$\mathrm{Pythia}$', density=True, histtype='step', linewidth=1.5)
        ax1.hist(p_gen, bins_p, label = r'$\mathrm{Trained}$ $\mathrm{model}$', density=True, histtype='step')
        ax1.set_xlabel(r"$p_T$")
        ax1.set_ylabel(r"$\mathrm{PDF}$")
        ax1.legend(fontsize='small',frameon=False)
        ax2.hist(z_restruc, bins_lat, density=True, histtype='step', linewidth=1.5, label=r'$\mathrm{Target}$')
        ax2.hist(lat_all, bins_lat, density=True, histtype='step', label=r'$\mathrm{Encoded}$')
        ax2.set_xlabel(r"$z$")
        ax2.set_ylabel(r"$\mathrm{PDF}$")
        ax2.legend(fontsize='small',frameon=False)
        fig.tight_layout()
        fig.savefig('gen_vs_target_pT_and_lat.pdf', dpi = 300, pad_inches = .1, bbox_inches = 'tight')

        # Save trained model as .pth
        torch.save(cSWAE, model_name+'.pth')

if __name__ == "__main__":

#_____________________ TRAINING _____________________#

    # Get the raw first emission data
    pT, labels = load_pT_data()

    # Split data into train, test, validation
    labels_train, labels_val, pT_train, pT_val = train_test_split(labels, pT, test_size=0.2)

    print("Training set size: ", labels_train.shape, "Validation set size: ", labels_val.shape)

    trainset = xyDataset(labels_train, pT_train)
    valset = xyDataset(labels_val, pT_val)

    # Set model hyperparameters
    target_distribution_str = "skew_gauss" 
    target_distribution_pT = skew_gauss
    reg_weight_pT = 20
    num_projection_pT = 25
    epochs_pT = 120
    lat_dim = 4

    # Instantiate the model object
    cSWAE = cSWAE(target_distribution=target_distribution_pT, reg_weight=reg_weight_pT,  latent_dim=lat_dim, num_projection=num_projection_pT, conditional=True, num_labels=2)

    # She's alive
    start_time = time.time()
    cSWAE.Train(trainset, valset, 128, epochs=epochs_pT) 
    end_time = time.time()
    print((end_time - start_time)/60 , "min")

    # pT data 
    labels_train_for_rec =  labels_train[:6400]
    pT_train_for_rec =  pT_train[:6400]

    labels_val_for_rec = labels_val[:1600]
    pT_val_for_rec = pT_val[:1600]


    # Convert momentum data to torch object to be encoded by trained model
    labels_train_torch = torch.tensor(labels_train_for_rec).float()
    pT_train_torch = torch.tensor(pT_train_for_rec).float()
    
    labels_val_torch = torch.tensor(labels_val_for_rec).float()
    pT_val_torch = torch.tensor(pT_val_for_rec).float()

    with torch.no_grad():
        z_tr = cSWAE.encode(pT_train_torch, labels_train_torch).view(-1,  lat_dim)
        z_v = cSWAE.encode(pT_val_torch, labels_val_torch).view(-1, lat_dim)

        lat_all = np.concatenate((z_tr, z_v), axis=0)
        lat_all = np.reshape(lat_all, lat_all.shape[0]*lat_all.shape[1] )
    
    # Sample for decoder (must match target distribution chosen in line 167)
    z = skew_gauss(batch_size=8000, lat_dim = lat_dim)
    #z = triangular(batch_size=8000, lat_dim = lat_dim)

    # Labels for the decoder (pass-through labels)
    labels = np.full((8000,2),[1.0, 1.0])
    c = torch.from_numpy(labels)
    
    # Decoding
    gen = cSWAE.decode(z,c.float())
    gen = gen.detach().numpy()

    # Reshape original training pT data 
    pT_all = np.concatenate((pT_train_for_rec, pT_val_for_rec), axis=0)

    len_of_new_vectors = 100

    # Reshaping the data
    pT_orig = pT_all.reshape(len(pT_all)*len_of_new_vectors,1)
    pT_gen = gen.reshape(len(gen)*len_of_new_vectors,1)

    # Restructure z for plotting
    z_restruc = z.numpy()
    z_restruc = z_restruc.reshape(len(z_restruc)*lat_dim,1)

    # Save complete model
    model_name = rf'pT_SWAE_fe_target_{target_distribution_str}_epoch{epochs_pT}_latd{lat_dim}_rw{reg_weight_pT}_np{num_projection_pT}'
    with cd('model_summaries/pT_SWAE'):
        results(model_name, pT_orig, pT_gen)
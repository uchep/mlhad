## Modeling hadronization using machine learning

This subdirectory contains the open-source code associated with "__Modeling hadronization using machine learning__" ([2203.04983](https://arxiv.org/abs/2203.04983)).

### Summary

This public directory includes example files allowing the user to train and implement cSWAE models in full fragmentation chains. The programs are written in Python and extensively use the ```Pythia```, ```PyTorch``` and ```Sci-kit learn``` libraries. Installation instructions can be found on the respective installation pages for each library.

The provided programs can be split into two categories: training cSWAE models and generating hadronization events. The latter relies on the former. However, we have also provided pre-trained models found in the ```model_summaries/``` folder such that the user can generate hadronization events without explicitly training a model.

Training a unique model configuration can be done by modifying the files ```pT_SWAE.py```, ```pz_SWAE.py```, or ```pz_cSWAE.py```. The SWAE programs contain examples of label-independent training, while the cSWAE program provides an example of label-dependent (energy-dependent) training. The model hyperparameters and target latent distribution have been set to default values to provide a reasonable starting configuration but may be modified. For additional hyperparameter configurations consult Table 1 in the text. Label independent kinematic training datasets for pz and pT have been provided as well as a label-dependent pz dataset.

Full hadronization events use the trained model decoder to generate hadronic kinematics. An example of generating this kinematic data from SWAE-trained model decoders can be found in ```model_pxpypz.py```. The setup of our modified fragmentation chain which utilizes these kinematics can be seen in ```frag_chain.py```.
"""
# pz_SWAE_fe.py is a part of the MLHAD package.
# Copyright (C) 2022 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import torch
from torch.utils.data import DataLoader
from torch import nn
from cSWAE import cSWAE
import matplotlib.pyplot as plt
from math import atan, asinh, sqrt
import numpy as np
from sklearn.model_selection import train_test_split
import time
import shutil
import seaborn as sns
import os
import pickle
import csv
import sys
sns.set()
sns.set_style("ticks")
sns.set_context("paper", font_scale = 3.0)
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 25

def load_pz_data(file_name = 'data/pz_data.csv'):
    """
    Load in Pythia pz data from file_name and generate labels

    Returns pz and label lists
    """
    pz,labels = [],[]
    with open(file_name, 'r') as csvfile:
        # Create csv reader object
        csvreader = csv.reader(csvfile, quoting = csv.QUOTE_NONNUMERIC)

        # Extract row by row
        for row in csvreader:
            pz.append(row)

    # Create labels, in this case to a value which will not effect the training
    vec_len = len(pz[0])
    N = len(pz)*vec_len
    for i in range(int(N/vec_len)):
        c = np.full((1,2),[1.0, 1.0])
        labels.append(c)
    
    # Reshape labels 
    labels = np.array(labels).reshape(int(N/vec_len),2)

    return pz, labels

def triangular(batch_size, lat_dim, c_label = None, E=50.0):
    """
    Produces (batch_size, lat_dim)-dimensional vectors sampled from a triangular probability distribution used
    to compute the Sliced Wasserstein distance.

    a: lower bound
    b: peak
    d: upper bound

    Returns a torch array of size (batch_size, lat_dim) 
    """
    z = np.random.triangular(2, 10, 50, (batch_size, lat_dim))
    return torch.from_numpy(z).type(torch.FloatTensor)

def trapezoid(batch_size, lat_dim, a=2.0,b=8.0,c=12.0,d=50.0, c_label = None, E=50.0):
    """
    Produces (batch_size, lat_dim)-dimensional vectors sampled from a trapezoidal probability distribution used
    to compute the Sliced Wasserstein distance.

    a: lower bound
    b: level start
    c: level end 
    d: upper bound

    Returns a torch array of size (batch_size, lat_dim) 
    """
    trap = []
    for i in range(lat_dim*batch_size):
        u = np.random.uniform()
        if u<=(b-a)/(d+c-a-b):
            C = (1.0/(d+c-a-b))*(1.0/(b-a))
            y = sqrt(u/C)+a
            trap.append(y)
        elif u<=1-(d-c)/(d+c-a-b):
            C = 1.0/(d+c-a-b)
            y = (1.0/2.0)*(u/C +a+b)
            trap.append(y)
        else:
            C = (1.0/(d+c-a-b))*(1.0/(d-c))
            y = d-sqrt((1-u)/C)
            trap.append(y)
    trap = np.array(trap)
    trap = np.reshape(trap, (batch_size, lat_dim))
    trap = np.sort(trap)

    return torch.from_numpy(trap).type(torch.FloatTensor)

def save_checkp(state, filename):
    """
    Saves the trained model in its current state as a .pth file with specified filename 
    """
    f_path = 'saved_models/'+filename
    torch.save(state, f_path)

class cd:
    """
    Context manager for changing the current working directory
    """
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

class xyDataset(torch.utils.data.Dataset):
    """
    Joins the x and y into a dataset, so that it can be used by the pythorch syntax.
    """

    def __init__(self, x, y):
        self.x = torch.tensor(x).to(torch.float)
        self.y = torch.tensor(y).to(torch.float)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        sample = [self.x[idx], self.y[idx]]
        return sample

def results(model_name, p_orig, p_gen):
    """
    This function takes in orginal and generated data for plotting and analysis. The function will create a model
    folder and within all plots will be saved along with the trained model .pth file which is used to generate kinemtaic
    data in frag_chain.py.

    model_name: Path to and name of folder to place produced figures and model file
    p_orig:     List of Pythia-generated data to be compared with model generated data
    p_gen:      List of model-generated data to be compared with Pythia

    Returns None
    """
    # Create a directory name to hold all auxiliary files
    procfolder = model_name
    os.makedirs(procfolder, exist_ok = True)

    # cd into the directory and stay there until all desired plots have been created
    with cd(procfolder):
        bins_p = np.histogram(np.hstack((p_orig.flatten(), p_gen.flatten())), bins=60)[1]
        bins_lat = np.histogram(np.hstack((z_restruc.flatten(),lat_all)), bins=60)[1]

        # Summary plot showing model generated pz vs Pythia and target vs generated latent space
        fig, (ax1,ax2) = plt.subplots(1,2,figsize=(16,8))
        ax1.hist(p_orig, bins_p, label = r'$\mathrm{Pythia}$', density=True, histtype='step', linewidth=1.5)
        ax1.hist(p_gen, bins_p, label = r'$\mathrm{Trained}$ $\mathrm{model}$', density=True, histtype='step')
        ax1.set_xlabel(r"$p_z$")
        ax1.set_ylabel(r"$\mathrm{PDF}$")
        ax1.legend(fontsize='small',frameon=False)
        ax2.hist(z_restruc, bins_lat, density=True, histtype='step', linewidth=1.5, label=r'$\mathrm{Target}$')
        ax2.hist(lat_all, bins_lat, density=True, histtype='step', label=r'$\mathrm{Encoded}$')
        ax2.set_xlabel(r"$z$")
        ax2.set_ylabel(r"$\mathrm{PDF}$")
        ax2.legend(fontsize='small',frameon=False)
        fig.tight_layout()
        fig.savefig('gen_vs_target_pz_and_lat.pdf', dpi = 300, pad_inches = .1, bbox_inches = 'tight')

        # Save trained model as .pth
        torch.save(cSWAE, model_name+'.pth')

if __name__ == "__main__":

#--------------------------------------- TRAINING ---------------------------------------#

    # Get the raw first emission data
    pz, labels = load_pz_data()

    # Split data into train, validation
    labels_train, labels_val, pz_train, pz_val = train_test_split(labels, pz, test_size=0.2)
    
    print("Training set size: ", labels_train.shape, "Validation set size: ", labels_val.shape)

    trainset = xyDataset(labels_train, pz_train)
    valset = xyDataset(labels_val, pz_val)

    # Set model hyperparameters
    target_distribution_str = "trapezoid" 
    target_distribution_pz = trapezoid 
    reg_weight_pz = 40
    num_projection_pz = 30
    epochs_pz = 150
    lat_dim = 2
    
    # Initialize the SWAE algorithm
    cSWAE = cSWAE(target_distribution=target_distribution_pz, reg_weight=reg_weight_pz,  latent_dim=lat_dim, num_projection=num_projection_pz,  conditional=True, num_labels=2)

    # She's alive
    start_time = time.time()
    cSWAE.Train(trainset, valset, 128, epochs = epochs_pz)
    end_time = time.time()
    print((end_time - start_time)/60 , "min")

#--------------------------------------- DATA COLLECTION ---------------------------------------#
    # pz data
    labels_train_for_rec =  labels_train[:6400]
    pz_train_for_rec =  pz_train[:6400]

    labels_val_for_rec = labels_val[:1600]
    pz_val_for_rec = pz_val[:1600]


    # Convert momentum data to torch object to be encoded by trained model
    labels_train_torch = torch.tensor(labels_train_for_rec).float()
    pz_train_torch = torch.tensor(pz_train_for_rec).float()
    
    labels_val_torch = torch.tensor(labels_val_for_rec).float()
    pz_val_torch = torch.tensor(pz_val_for_rec).float()

    # Encode original data to see encoded latent space
    with torch.no_grad():
        z_tr = cSWAE.encode(pz_train_torch, labels_train_torch).view(-1,  lat_dim)
        z_v = cSWAE.encode(pz_val_torch, labels_val_torch).view(-1, lat_dim)
        
        lat_all = np.concatenate((z_tr, z_v), axis=0)
        lat_all = np.reshape(lat_all, lat_all.shape[0]*lat_all.shape[1])
    
#--------------------------------------- GENERATING ---------------------------------------#
    # Sample fed into decoder (must match target distribution chosen in line 191)
    #z = triangular(batch_size=8000, lat_dim = lat_dim)
    z = trapezoid(batch_size=8000, lat_dim = lat_dim)

    # Labels for the decoder (pass-through labels)
    labels = np.full((8000,2),[1.0, 1.0])
    c = torch.from_numpy(labels)
    
    # Decoding
    gen = cSWAE.decode(z,c.float())
    gen = gen.detach().numpy()

    # Reshape original training pz data 
    pz_all = np.concatenate((pz_train_for_rec, pz_val_for_rec), axis=0)

    len_of_new_vectors = 100

    # Reshaping the data
    pz_orig = pz_all.reshape(len(pz_all)*len_of_new_vectors,1)
    pz_gen = gen.reshape(len(gen)*len_of_new_vectors,1)

    # Restructure z for plotting
    z_restruc = z.numpy()
    z_restruc = z_restruc.reshape(len(z_restruc)*lat_dim,1)

    # Save complete model
    model_name = rf'p_SWAE_fe_target_{target_distribution_str}_epoch{epochs_pz}_latd{lat_dim}_rw{reg_weight_pz}_np{num_projection_pz}'
    with cd('model_summaries/pz_SWAE'):
        results(model_name, pz_orig, pz_gen)
"""
# frag_chain.py is a part of the MLHAD package.
# Copyright (C) 2022 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import torch
from torch import nn
from torch.utils.data import DataLoader
from cSWAE import cSWAE
from PDB import ParticleDatabase
import matplotlib.pyplot as plt
from math import sin, cos, atan, asinh, sinh, cosh, sqrt, pi, log
import random
import numpy as np
from pythia8 import Vec4 
from sklearn.model_selection import train_test_split
from model_pxpypz import generate_px_py_pz, trapezoid, skew_gauss
from flavor_selector import prob_flavor_selection

pd = ParticleDatabase()
###########################################################################################################################################

def sqrtPos(val):
    """
    Returns 0 if val is negative, otherwiswe the sqrt.
    """
    return sqrt(val) if val > 0 else 0.

###########################################################################################################################################

def fragmentation(p1, p2, id1, id2, E_threshold, string_end, E_partons = 50.0):
    """
    This function generates a fragmentation event by boosting to the string systems CM frame, generating
     hadronic emission kinematics, and then boosting back to original frame.

    p1  : list, hadronized end four-momentum components input as list
    p2  : list, spectator end four-momentum components input as list
    id1 : int, string id corresponding to hadronizing end (p1)
    id2 : int, string id corresponding to non-hadronizing end (p2)
    E_threshold: float, IR center of mass energy cutoff for the fragmentation process
    string_end: string, fragenting string end input either 'endA' or 'endB'
    E_partons: float, energy of initial parton in the CM frame

    Returns new string and hadron ids and momenta
    """
    
    # Generate new string and hadron IDs
    new_string_id, new_had_id = prob_flavor_selection(id1)
    
    v1 = Vec4(*p1)
    v2 = Vec4(*p2)    
    
    # Boost to the string system COM
    p_sum = v1+v2
    v1.bstback(p_sum)
    
    # Rotate such that v1 is completely along the z-axis
    theta = v1.theta()
    phi   = v1.phi()

    v1.rot(0.0, -phi)
    v1.rot(-theta, phi)
    
    # Sample from new string system CM with energy W^2 = W^+ W^- 
    # Could also just use the energy from the v1 four-vector 
    # E_CM = sqrt((2.*sqrt((p1[0]+px)**2 + (p1[1]+py)**2 + (p1[2])**2)) * (2.*p2[3]))/2.
    E_CM = v1.e()
    
    # Generate kinematics using the CM energy of string end p1 if above threshold
    if E_CM > E_threshold:
        # Sample pz from trained model
        px,py,pz = model_kinematics(E_CM, string_end)

        # Create a vector to hold the boosted value of pz
        w = Vec4(px, py, pz, sqrt(pd[new_had_id].mass**2 +(px)**2 + (py)**2 + (pz)**2))
        w.rot(0.0, -phi)
        w.rot(theta, phi)
        w.bst(p_sum)

        # v1 needs to be in the lab frame
        v1.rot(0.0, -phi)
        v1.rot(theta, phi)
        v1.bst(p_sum)
        # Set pz equal to the boosted back value
        pz = w.pz()

        # New hadron and string four momenta
        s = Vec4(-px, -py, v1.pz()-pz, sqrt(px**2 + py**2 + (v1.pz()-pz)**2))
        h = Vec4(v1.px()+px, v1.py()+py, pz, sqrt(pd[new_had_id].mass**2 +(v1.px()+px)**2 + (v1.py()+py)**2 + (pz)**2))
        
        # Return numpy arrays
        s = np.array([s.px(), s.py(), s.pz(), s.e()])
        h = np.array([h.px(), h.py(), h.pz(), h.e()])
            
    else: 
        # Else return zero valuesto signal termination in the fragmentation chain
        new_string_id = 0
        new_had_id = 0
        s = [0,0,0,0]
        h = [0,0,0,0]
        E_CM = 0.0

    return new_string_id, s, new_had_id, h, E_CM

###########################################################################################################################################

def model_kinematics(E, string_end, sample_size = 8):
    """
    This function generates pz and pT distributions from SWAE.

    E: initial parton energies in GeV
    sample_size: number of pz, pT points to sample from
    model_name: path to model file (.pth)

    Returns px, py, pz to be used in fragmentation chain
    """

    # Load in model data from two_to_one
    px, py, gen_pz = generate_px_py_pz(sample_size)

    # Convert pz to numpy array
    gen_pz = np.array(gen_pz)
    
    gen_pz = np.sort(gen_pz)
    
    # Rescale |p| according to the desired energy
    gen_pz = gen_pz * (E/50.0)

    # Select a random integer 
    rndm = random.randint(0,len(gen_pz)-1)

    pz = gen_pz[rndm]
    
    return px,py, pz

###########################################################################################################################################

def fragmentation_chain(E_partons, E_threshold = 5.0, endA_id = 2, endB_id = -2, mode = 1, print_details = False):
    """
    Generate a chain of fragmentation events using SWAE kinematics.

    E_partons:   Initial parton energy in GeV (easily modified to accomodate two different parton energies)
    E_threshold: IR center of mass energy cutoff for the fragmentation process
    endA_pid:    Initial string flavor for endA
    endB_pid:    Initial string flavor for endB
    mode:        Selects the type of event to generate.
                    0 = single particle.
                    1 = q qbar.
                    2 = g g .
                    3 = g g g.
                    4 = minimal q q q junction topology.
                    5 = q q q junction topology with gluons on the strings.
                    6 = q q qbar qbar dijunction topology, no gluons.
                    7 - 10 = ditto, but with 1 - 4 gluons on string between 
                             junctions.
    print_details: When set to True the details of the entirte fragmentation chain will be output to the console

    Returns lists of hadron names, 4-momenta, and pids
    """
    # Initialize momentum, pid, name log lists
    endA_p_list, endB_p_list, endC_p_list, had_p_list = [],[],[],[]
    endA_pid_list, endB_pid_list, endC_pid_list, had_pid_list = [],[],[],[]
    endA_name_list, endB_name_list, endC_name_list, had_name_list = [],[],[],[]
    E_CM_check = [E_partons]

    # Initialize kinematics
    if mode == 1:
        endA_m = 0.#pd.pdb.m0(endA_id)
        endB_m = 0.#pd.pdb.m0(endB_id)
        endA_p = np.array([0, 0, sqrtPos(E_partons*E_partons - endA_m*endA_m), E_partons])
        endB_p = np.array([0, 0, -sqrtPos(E_partons*E_partons - endB_m*endB_m), E_partons])

        # Record initial system 
        endA_p_list.append(endA_p)
        endB_p_list.append(endB_p)
        endA_pid_list.append(endA_id)
        endB_pid_list.append(endB_id)
        endA_name_list.append(pd[endA_id].name)
        endB_name_list.append(pd[endB_id].name)

        endA_p_0 = [endA_p[0], endA_p[1], endA_p[2], endA_p[3]]
        endB_p_0 = [endB_p[0], endB_p[1], endB_p[2], endB_p[3]]
        
        #sum_had_E = 0
        endA_term, endB_term= 0,0
        term_lim = 1
        had_sum_E_A, had_sum_E_B = 0,0

        # Generate the chain
        while endA_term<term_lim or endB_term<term_lim:
            # Choose endA (>0.5) or endB (<= 0.5)
            r =  np.random.uniform()
            # If less than 0.5 choose endA
            if r > 0.5:
                
                if endA_term < term_lim:
                    # Generate the fragmentation for endA
                    endA_id_new, endA_p_new, had_id_new, had_p_new, CM_A_E_check = fragmentation(endA_p, endB_p, endA_id, endB_id, E_threshold, 'endA', E_partons)
                    
                    # Check if the fragmentation should be logged
                    if endA_p_new[3] > 0 and CM_A_E_check > E_threshold:
                        # endA should always have positive pz
                        if endA_p_new[2] < 0:
                            print("SHOULDNT BE COUNTED")
                        endA_id = endA_id_new 
                        endA_p = endA_p_new 
                        had_id = had_id_new 
                        had_p = had_p_new
                        # Update momenta, pids
                        endA_p_list.append(endA_p.tolist())
                        endB_p_list.append(endB_p.tolist())
                        endC_p_list.append(endA_p.tolist())
                        had_p_list.append(had_p.tolist())
                        endA_pid_list.append(endA_id)
                        endB_pid_list.append(endB_id)
                        endC_pid_list.append(endA_id)
                        had_pid_list.append(had_id)
                        endA_name_list.append(pd[endA_id].name)
                        endB_name_list.append(pd[endB_id].name)
                        endC_name_list.append(pd[endA_id].name)
                        had_name_list.append(pd[had_id].name)
                        E_CM_check.append(CM_A_E_check)
                        had_sum_E_A += had_p[3]

                    else:
                        # Signal endA termination
                        endA_term +=1
                        
            # else choose endB           
            else:
                if endB_term < term_lim:
                    endB_id_new, endB_p_new, had_id_new, had_p_new, CM_B_E_check = fragmentation(endB_p, endA_p, endB_id, endA_id, E_threshold, 'endB', E_partons)
                    if endB_p_new[3] > 0 and CM_B_E_check > E_threshold:
                        # endB should always have negative pz
                        if endB_p_new[2] > 0:
                            print("SHOULDNT BE COUNTED")
                        endB_id = endB_id_new 
                        endB_p = endB_p_new 
                        had_id = had_id_new 
                        had_p = had_p_new
                        # Update momenta, pids
                        endA_p_list.append(endA_p.tolist())
                        endB_p_list.append(endB_p.tolist())
                        endC_p_list.append(endB_p.tolist())
                        had_p_list.append(had_p.tolist())
                        endA_pid_list.append(endA_id)
                        endB_pid_list.append(endB_id)
                        endC_pid_list.append(endB_id)
                        had_pid_list.append(had_id)
                        endA_name_list.append(pd[endA_id].name)
                        endB_name_list.append(pd[endB_id].name)
                        endC_name_list.append(pd[endB_id].name)
                        had_name_list.append(pd[had_id].name)
                        E_CM_check.append(CM_B_E_check)
                        had_sum_E_B += had_p[3]
    
                    else:
                        # Signal endA termination
                        endB_term += 1
    # Print the deatils of the entire chain
    if print_details == True:
        # Print the chain results
       print('endA:')
       for i in range(len(endA_p_list)):
           print('Event no: ', i, ', ', 'pid: ', endA_pid_list[i], '(', endA_name_list[i],'), ', 'px: ', '{:.3f}'.format(endA_p_list[i][0]), ' py: ', '{:.3f}'.format(endA_p_list[i][1]), ' pz: ', "{:.3f}".format(endA_p_list[i][2]), ' E: ', '{:.3f}'.format(endA_p_list[i][3]))
           
       print()
       print('endB:')
       for i in range(len(endB_p_list)):
           print('Event no: ', i, ', ', 'pid: ', endB_pid_list[i], '(', endB_name_list[i],'), ', 'px: ', '{:.3f}'.format(endB_p_list[i][0]), ' py: ', '{:.3f}'.format(endB_p_list[i][1]), ' pz: ', "{:.3f}".format(endB_p_list[i][2]), ' E: ', '{:.3f}'.format(endB_p_list[i][3]))
   
       print()
       print('endC:')
       for i in range(len(endC_p_list)):
           print('Event no: ', i+1, ', ', 'pid: ', endC_pid_list[i], '(', endC_name_list[i],'), ', 'px: ', '{:.3f}'.format(endC_p_list[i][0]), ' py: ', '{:.3f}'.format(endC_p_list[i][1]), ' pz: ', "{:.3f}".format(endC_p_list[i][2]), ' E: ', '{:.3f}'.format(endC_p_list[i][3]))
  
       print()
       print('hadron:')
       for i in range(len(had_p_list)):
           print('Event no: ', i+1, ', ', 'pid: ', had_pid_list[i], '(', had_name_list[i],'), ', 'px: ', '{:.3f}'.format(had_p_list[i][0]), ' py: ', '{:.3f}'.format(had_p_list[i][1]), ' pz: ', "{:.3f}".format(had_p_list[i][2]), ' E: ', '{:.3f}'.format(had_p_list[i][3]))
        
        # Quick check for energy-momentum conservation 
        #sum_px, sum_py, sum_pz, sum_E = 0,0,0,0
        #for i in range(len(had_p_list)):
        #    sum_px+= had_p_list[i][0]
        #    sum_py+= had_p_list[i][1]
        #    sum_pz+= had_p_list[i][2]
        #    sum_E+= had_p_list[i][3]
        #print()
        #print('hadron sum: ', 'px: ', sum_px, 'py: ', sum_py, 'pz: ', sum_pz, 'E: ', sum_E)

    return had_name_list, had_p_list, had_pid_list

if __name__ == "__main__":

    # Generate the hadronization chain for partons with initial energy 50 GeV
    fragmentation_chain(50.0, print_details=True)
    

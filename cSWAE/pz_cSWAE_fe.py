"""
# pz_cSWAE_fe.py is a part of the MLHAD package.
# Copyright (C) 2022 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import torch
from torch.utils.data import DataLoader
from torch import nn
from cSWAE import cSWAE
import matplotlib.pyplot as plt
from math import atan, asinh, sqrt
import numpy as np
from sklearn.model_selection import train_test_split
import time
import shutil
import seaborn as sns
import os
import pickle
import csv
sns.set()
sns.set_style("ticks")
sns.set_context("paper", font_scale = 3.0)
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 25

def load_labeled_pz_data(file_name = 'data/pz_labeled_data.csv'):
    """
    Load in Pythia pz data and labels from file_name

    Returns pz and label lists
    """
    pz,labels = [],[]
    with open(file_name, 'r') as csvfile:
        # Create csv reader object
        csvreader = csv.reader(csvfile, quoting = csv.QUOTE_NONNUMERIC)

        # Extract row by row
        i = 0
        for row in csvreader:
            if i%2==0:
                labels.append(row)
                i+=1
            else: 
                pz.append(row)
                i+=1

    return pz, labels

def firstEmission_data(N=400000, E_partons=50.0, mode=1, vec_len=100):
    """
    This function produces the first emission energy-labeled p_z events
    used for comparison to model generated data
     
    N: Total number of sampled events going into the training set
    mode: Desired event topology, mode=1 is the q-qbar system and is the only mode currently supported 
    vec_len: Length of the training vectors

    Returns lists of training data and associated labels 
    """
    gen = pgun_mT.ParticleGun()
    pz = []
    labels = []
    E_min = 5.0
    E_max = 1000.0
    n=0
    # Generate events and collect first emission data
    while n != N:
        gen.next(mode, E_partons)
        if gen.strings.endC[1].e() == gen.strings.endA[2].e():
            n+=1
            pz.append(gen.strings.hads[1].pz())
    pz = np.array(pz) 

    # Create labels, in this case to a value which will not effect the NN
    for i in range(int(N/vec_len)):
        c = np.full((1,2),[1 - (E_partons - E_max)/(E_min - E_max), (E_partons - E_max)/(E_min - E_max) ])
        labels.append(c)
    
    # Reshape labels 
    labels = np.array(labels).reshape(int(N/vec_len),2)

    # Reshaping into 100 dimensional arrays for input into cSWAE
    pz_raw = pz.reshape(int(len(pz)/vec_len),vec_len)

    # Sorting 100 dimensional arrays from lowest -> highest
    pz_raw = np.sort(pz_raw)

    return pz_raw, labels

def trapezoid(batch_size, lat_dim, c_label = None, E=50.0):
    """
    Produces label-dependent (batch_size, lat_dim)-dimensional vectors sampled from a trapezoidal probability distribution used
    to compute the Sliced Wasserstein distance.

    a: lower bound
    b: level start
    c: level end 
    d: upper bound
    c_label: energy label
    E: reference energy of partons

    Returns a torch array of size (batch_size, lat_dim) 
    """

    if c_label !=None:
        c_label = c_label.numpy()
        # Set the values for E_max and E_min 
        E_min =5.0
        E_max =1000.0
        trap = []
        for j in range(len(c_label)):
            C_label = c_label[j][1]
            
            # Solving for E from c = (E- E_max)/(E_min - E_max)
            E = E_min * C_label + E_max * (1-C_label) 
            
            a = 0.04 * E
            b = 0.16 * E 
            c = 0.24 * E 
            d = E 
            trap_i = []
            for i in range(lat_dim):
                u = np.random.uniform()
                if u<=(b-a)/(d+c-a-b):
                    C = (1.0/(d+c-a-b))*(1.0/(b-a))
                    y = sqrt(u/C)+a
                    trap_i.append(y)
                elif u<=1-(d-c)/(d+c-a-b):
                    C = 1.0/(d+c-a-b)
                    y = (1.0/2.0)*(u/C +a+b)
                    trap_i.append(y)
                else:
                    C = (1.0/(d+c-a-b))*(1.0/(d-c))
                    y = d-sqrt((1-u)/C)
                    trap_i.append(y)
            trap_i = np.sort(trap_i)
            trap.append(trap_i)
            
        trap = np.array(trap)
        
    else:
        a = 0.04 * E
        b = 0.16 * E 
        c = 0.24 * E 
        d = E
        trap = []
        for i in range(lat_dim*batch_size):
            u = np.random.uniform()
            if u<=(b-a)/(d+c-a-b):
                C = (1.0/(d+c-a-b))*(1.0/(b-a))
                y = sqrt(u/C)+a
                trap.append(y)
            elif u<=1-(d-c)/(d+c-a-b):
                C = 1.0/(d+c-a-b)
                y = (1.0/2.0)*(u/C +a+b)
                trap.append(y)
            else:
                C = (1.0/(d+c-a-b))*(1.0/(d-c))
                y = d-sqrt((1-u)/C)
                trap.append(y)
        trap = np.array(trap)
        trap = np.reshape(trap, (batch_size, lat_dim))
        trap = np.sort(trap)

    return torch.from_numpy(trap).type(torch.FloatTensor)

class cd:
    """
    Context manager for changing the current working directory
    """
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

class xyDataset(torch.utils.data.Dataset):
    """
    Joins the x and y into a dataset, so that it can be used by the pythorch syntax.
    """

    def __init__(self, x, y):
        self.x = torch.tensor(x).to(torch.float)
        self.y = torch.tensor(y).to(torch.float)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        sample = [self.x[idx], self.y[idx]]
        return sample

def results(model_name, p_1, p_2, p_3, p_4):
    """
    This function takes in orginal and generated data for four different energy labels to be used for plotting and analysis.
    The function will create a model folder and within all plots will be saved.

    model_name:     Path to and name of folder to  to place produced figures
    p1, p2, p3, p4: List of model-generated data to be compared with Pythia

    Returns None
    """
    # Create a directory name to hold all auxiliary files
    procfolder = model_name
    
    # Create the directory
    os.makedirs(procfolder, exist_ok = True)

    # cd into the directory and stay there until all of the diagrams have been created
    with cd(procfolder):
        bins_pz = np.histogram(np.hstack((p_1.flatten(), p_2.flatten(), p_3.flatten(), p_4.flatten(), p_orig_1, p_orig_2, p_orig_3, p_orig_4)), bins=60)[1]
        bins_p_1 = np.histogram(np.hstack((p_1,p_2)), bins=60)[1]
        bins_p_2 = np.histogram(np.hstack((p_3,p_4)), bins=60)[1]
        bins_lat_1 = np.histogram(np.hstack((lat_1,lat_2)), bins=60)[1]
        bins_lat_2 = np.histogram(np.hstack((lat_3,lat_4)), bins=60)[1]

        # Compare four model-generated datasets to respective Pythia equivalent
        fig1, ax1 = plt.subplots(1,1, figsize=(13,8))
        ax1.hist(p_1, bins_pz, label = rf'$E={E_partons[0]}$', density=True, histtype='step', linewidth=2.0)
        ax1.hist(p_2, bins_pz, label = rf'$E={E_partons[1]}$', density=True, histtype='step', linewidth=2.0)
        ax1.hist(p_3, bins_pz, label = rf'$E={E_partons[2]}$', density=True, histtype='step', linewidth=2.0)
        ax1.hist(p_4, bins_pz, label = rf'$E={E_partons[3]}$', density=True, histtype='step', linewidth=2.0)
        ax1.hist(p_orig_1, bins_pz, density=True, histtype='step', linewidth=2.0, label=rf'$\mathrm{{Pythia}}$', color = 'tab:purple')
        ax1.hist(p_orig_2, bins_pz, density=True, histtype='step', linewidth=2.0, color = 'tab:purple')
        ax1.hist(p_orig_3, bins_pz, density=True, histtype='step', linewidth=2.0, color = 'tab:purple')
        ax1.hist(p_orig_4, bins_pz, density=True, histtype='step', linewidth=2.0, color = 'tab:purple')
        ax1.set_xlabel(r"$p_z$")
        ax1.set_ylabel(r"$\mathrm{PDF}$")
        ax1.legend(fontsize='x-small',frameon=False)
        fig1.tight_layout()
        fig1.savefig('p_orig_gen_overlay.pdf', dpi = 300, pad_inches = .1, bbox_inches = 'tight')

        # Focus on the first two datasets and see what their endoed latant spaces look like
        fig2, (ax2,ax3) = plt.subplots(1,2,figsize=(16,8))
        ax2.hist(p_orig_1, bins_p_1, label = r'$\mathrm{Pythia}$', density=True, histtype='step', color = 'tab:blue')
        ax2.hist(p_1, bins_p_1, label = r'$\mathrm{Trained}$ $\mathrm{model}$', density=True, histtype='step', color = 'tab:orange')
        ax2.hist(p_orig_2, bins_p_1, density=True, histtype='step', color = 'tab:blue')
        ax2.hist(p_2, bins_p_1, density=True, histtype='step', color = 'tab:orange')
        ax2.set_xlabel(r"$p_z$")
        ax2.set_ylabel(r"$\mathrm{PDF}$")
        ax2.legend(fontsize='x-small',frameon=False)
        ax3.hist(z_1, bins_lat_1, density=True, histtype='step', label=r'$\mathrm{Target}$ $(\mathrm{Trapezoidal})$', color = 'tab:blue')
        ax3.hist(lat_1, bins_lat_1, density=True, histtype='step', label=r'$\mathrm{Encoded}$',color = 'tab:orange')
        ax3.hist(z_2, bins_lat_1, density=True, histtype='step')
        ax3.hist(lat_2, bins_lat_1, density=True, histtype='step')
        ax3.set_xlabel(r"$z$")
        ax3.set_ylabel(r"$\mathrm{PDF}$")
        ax3.legend(fontsize='x-small',frameon=False)
        fig2.tight_layout()
        fig2.savefig('gen_vs_target_p_and_lat_mult_E.pdf', dpi = 300, pad_inches = .1, bbox_inches = 'tight')

        # Focus on the second two datasets and see what their endoed latant spaces look like
        fig3, (ax4,ax5) = plt.subplots(1,2,figsize=(16,8))
        ax4.hist(p_orig_3, bins_p_2, label = r'$\mathrm{Pythia}$', density=True, histtype='step', color = 'tab:blue')
        ax4.hist(p_3, bins_p_2, label = r'$\mathrm{Trained}$ $\mathrm{model}$', density=True, histtype='step', color = 'tab:orange')
        ax4.hist(p_orig_4, bins_p_2, density=True, histtype='step', color = 'b')
        ax4.hist(p_4, bins_p_2, density=True, histtype='step', color = 'tab:orange')
        ax4.set_xlabel(r"$p_z$")
        ax4.set_ylabel(r"$\mathrm{PDF}$")
        ax4.legend(fontsize='x-small',frameon=False)
        ax5.hist(z_3, bins_lat_2, density=True, histtype='step', label=r'$\mathrm{Target}$ $(\mathrm{Trapezoidal})$', color = 'tab:blue')
        ax5.hist(lat_3, bins_lat_2, density=True, histtype='step', label=r'$\mathrm{Encoded}$',color = 'tab:orange')
        ax5.hist(z_4, bins_lat_2, density=True, histtype='step', color = 'tab:blue')
        ax5.hist(lat_4, bins_lat_2, density=True, histtype='step',color = 'tab:orange')
        ax5.set_xlabel(r"$z$")
        ax5.set_ylabel(r"$\mathrm{PDF}$")
        ax5.legend(fontsize='x-small',frameon=False)
        fig3.tight_layout()
        fig3.savefig('gen_vs_target_p_and_lat_mult_E_1.pdf', dpi = 300, pad_inches = .1, bbox_inches = 'tight')

        # Save trained model as .pth
        torch.save(cSWAE, model_name+'.pth')

if __name__ == "__main__":
#------------------------------------------- TRAINING -------------------------------------------#

    # Get the raw label-dependent first emission data
    pz, labels = load_labeled_pz_data()

    # Split data into train, test, validation
    labels_train, labels_val, pz_train, pz_val = train_test_split(labels, pz, test_size=0.2)

    trainset = xyDataset(labels_train, pz_train)
    valset = xyDataset(labels_val, pz_val)
    
    # Setting model configuration
    target_distribution_str = "trapezoid" 
    target_distribution_pz = trapezoid 
    reg_weight_pz = 10
    num_projection_pz = 30
    epochs_pz = 350 
    lat_dim = 30

    cSWAE = cSWAE(target_distribution=target_distribution_pz, reg_weight=reg_weight_pz,  latent_dim=lat_dim, 
        num_projection=num_projection_pz, conditional=True, 
        num_labels=2)

    # She's alive
    start_time = time.time()
    cSWAE.Train(trainset, valset, 128, epochs = epochs_pz)
    end_time = time.time()
    print((end_time - start_time)/60 , "min")
    
    # Generate data for four unique (not trained on) labels
    len_of_new_vectors = 100
    E_min = 5.0
    E_max = 1000.0
    E_partons = [100.0, 200.0, 300.0, 400.0]
    c_partons = [(E_partons[i]- E_max)/(E_min - E_max) for i in range(len(E_partons))]
    batch_size = 8000 * 2

    # Load Pythia generated data to compare too.
    pz_orig, labels_orig = load_labeled_pz_data('data/pz_labeled_ref_data.csv')
    pz_check, labels_check = [],[]
    for i in range(len(E_partons)):
        pz_orig_i, labels_orig_i = [],[]
        for j in range(2000):
            pz_orig_i.append(pz_orig[j])
            labels_orig_i.append(labels_orig[j])
        pz_check.append(pz_orig_i)
        labels_check.append(labels_orig_i)
        del pz_orig[0:2000]
        del labels_orig[0:2000]

#------------------------------------------- GENERATING -------------------------------------------#
    z_1 = trapezoid(batch_size=batch_size, lat_dim = lat_dim, E=E_partons[0])
    c_1 = np.full((batch_size,2),[1 - c_partons[0], c_partons[0]])
    c_1 = torch.from_numpy(c_1)
    gen_1 = cSWAE.decode(z_1, c_1.float())
    gen_1 = gen_1.detach().numpy()
    p_gen_1 = gen_1.reshape(len(gen_1)*len_of_new_vectors,1)
    p_orig_1, label_orig_1 = np.array(pz_check[0]), np.array(labels_check[0])
    p_orig_1 = torch.from_numpy(p_orig_1).type(torch.FloatTensor)
    label_orig_1 = torch.from_numpy(label_orig_1).type(torch.FloatTensor)
    
    z_2 = trapezoid(batch_size=batch_size, lat_dim = lat_dim, E=E_partons[1])
    c_2 = np.full((batch_size,2),[1 - c_partons[1], c_partons[1]])
    c_2 = torch.from_numpy(c_2)
    gen_2 = cSWAE.decode(z_2, c_2.float())
    gen_2 = gen_2.detach().numpy()
    p_gen_2 = gen_2.reshape(len(gen_2)*len_of_new_vectors,1)
    p_orig_2, label_orig_2 = np.array(pz_check[1]), np.array(labels_check[1])
    p_orig_2 = torch.from_numpy(p_orig_2).type(torch.FloatTensor)
    label_orig_2 = torch.from_numpy(label_orig_2).type(torch.FloatTensor)
    
    z_3 = trapezoid(batch_size=batch_size, lat_dim = lat_dim, E=E_partons[2])
    c_3 = np.full((batch_size,2),[1 - c_partons[2], c_partons[2]])
    c_3 = torch.from_numpy(c_3)
    gen_3 = cSWAE.decode(z_3, c_3.float())
    gen_3 = gen_3.detach().numpy()
    p_gen_3 = gen_3.reshape(len(gen_3)*len_of_new_vectors,1)
    p_orig_3, label_orig_3 = np.array(pz_check[2]), np.array(labels_check[2])
    p_orig_3 = torch.from_numpy(p_orig_3).type(torch.FloatTensor)
    label_orig_3 = torch.from_numpy(label_orig_3).type(torch.FloatTensor)
   
    z_4 = trapezoid(batch_size=batch_size, lat_dim = lat_dim, E=E_partons[3])
    c_4 = np.full((batch_size,2),[1 - c_partons[3], c_partons[3]])
    c_4 = torch.from_numpy(c_4)
    gen_4 = cSWAE.decode(z_4, c_4.float())
    gen_4 = gen_4.detach().numpy()
    p_gen_4 = gen_4.reshape(len(gen_4)*len_of_new_vectors,1)
    p_orig_4, label_orig_4 = np.array(pz_check[3]), np.array(labels_check[3])
    p_orig_4 = torch.from_numpy(p_orig_4).type(torch.FloatTensor)
    label_orig_4 = torch.from_numpy(label_orig_4).type(torch.FloatTensor)

    # Gathering encoded latent space data
    with torch.no_grad():
        lat_1 = cSWAE.encode(p_orig_1, label_orig_1).view(-1,  lat_dim)
        lat_2 = cSWAE.encode(p_orig_2, label_orig_2).view(-1,  lat_dim)
        lat_3 = cSWAE.encode(p_orig_3, label_orig_3).view(-1,  lat_dim)
        lat_4 = cSWAE.encode(p_orig_4, label_orig_4).view(-1,  lat_dim)

        lat_1 = lat_1.detach().numpy()
        lat_1 = np.reshape(lat_1, lat_1.shape[0]*lat_1.shape[1])
        lat_2 = lat_2.detach().numpy()
        lat_2 = np.reshape(lat_2, lat_2.shape[0]*lat_2.shape[1])
        lat_3 = lat_3.detach().numpy()
        lat_3 = np.reshape(lat_3, lat_3.shape[0]*lat_3.shape[1])
        lat_4 = lat_4.detach().numpy()
        lat_4 = np.reshape(lat_4, lat_4.shape[0]*lat_4.shape[1])

    # Reshaping Pythia-generated data
    p_orig_1 = p_orig_1.detach().numpy()
    p_orig_1 = np.reshape(p_orig_1, p_orig_1.shape[0]*p_orig_1.shape[1])
    p_orig_2 = p_orig_2.detach().numpy()
    p_orig_2 = np.reshape(p_orig_2, p_orig_2.shape[0]*p_orig_2.shape[1])
    p_orig_3 = p_orig_3.detach().numpy()
    p_orig_3 = np.reshape(p_orig_3, p_orig_3.shape[0]*p_orig_3.shape[1])
    p_orig_4 = p_orig_4.detach().numpy()
    p_orig_4 = np.reshape(p_orig_4, p_orig_4.shape[0]*p_orig_4.shape[1])

    # Reshaping target latent space vectors
    z_1 = z_1.detach().numpy()
    z_1 = z_1.flatten()
    z_2 = z_2.detach().numpy()
    z_2 = z_2.flatten()
    z_3 = z_3.detach().numpy()
    z_3 = z_3.flatten()
    z_4 = z_4.detach().numpy()
    z_4 = z_4.flatten()

    # Save complete model
    model_name = rf'pz_cSWAE_fe_target_{target_distribution_str}_epoch{epochs_pz}_latd{lat_dim}_rw{reg_weight_pz}_np{num_projection_pz}'
    with cd('model_summaries/pz_cSWAE'):
        results(model_name, p_gen_1, p_gen_2, p_gen_3, p_gen_4)
from frag_chain import fragmentation_chain
from model_pxpypz import generate_px_py_pz, trapezoid, skew_gauss
import time

# Generate 1000 events
N_events = 1000

print("Starting cSWAE test.")

# Start the timer
start_cSWAE = time.time()
for i in range(N_events):
    fragmentation_chain(E_partons = 50.0, E_threshold = 5.0, print_details=False)

# Output the generation time
end_cSWAE = time.time()
print('cSWAE generated', N_events, 'events in', end_cSWAE - start_cSWAE, 's.') # ~170 seconds

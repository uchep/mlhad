"""
# model_pxpypz.py is a part of the MLHAD package.
# Copyright (C) 2022 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import torch
from torch import nn
from torch.utils.data import DataLoader
from cSWAE import cSWAE
from math import sin, cos, atan, asinh, sinh, cosh, sqrt, pi
import random
import numpy as np
from sklearn.model_selection import train_test_split
from scipy.stats import skewnorm

def trapezoid(batch_size, lat_dim, a=2.0,b=8.0,c=12.0,d=50.0):
    """
    Produces a trapezoidal probability distribution to be used in SWAE decoder

    a: lower bound
    b: level start
    c: level end 
    d: upper bound

    Returns a torch array of size (batch_size, lat_dim)
    """
    trap = []
    for i in range(lat_dim*batch_size):
        u = np.random.uniform()
        if u<=(b-a)/(d+c-a-b):
            C = (1.0/(d+c-a-b))*(1.0/(b-a))
            y = sqrt(u/C)+a
            trap.append(y)
        elif u<=1-(d-c)/(d+c-a-b):
            C = 1.0/(d+c-a-b)
            y = (1.0/2.0)*(u/C +a+b)
            trap.append(y)
        else:
            C = (1.0/(d+c-a-b))*(1.0/(d-c))
            y = d-sqrt((1-u)/C)
            trap.append(y)
    trap = np.array(trap)
    trap = np.reshape(trap, (batch_size, lat_dim))
    trap = np.sort(trap)
    return torch.from_numpy(trap).type(torch.FloatTensor)

def skew_gauss(batch_size, lat_dim=2):
    """
    Generate list of number according to a skewed-gaussian to be used in SWAE decoder

    Returns Returns a torch array of size (batch_size, lat_dim)
    """
    z = np.array(skewnorm.rvs(a=4.2592,loc=0.0988,scale=0.2570, size=(batch_size,lat_dim)))
    return torch.from_numpy(z).type(torch.FloatTensor)

def generate_pz(sample_size, model_pz='model_summaries/pz_SWAE/p_SWAE_fe_target_trapezoid_epoch150_latd2_rw40_np30/p_SWAE_fe_target_trapezoid_epoch150_latd2_rw40_np30.pth'):
    """
    Generates pz values from trained SWAE models to be used in the fragmentation chain
    
    sample_size: int, number of points to sample from
    model_pz:    string, path to model file 

    Returns pz list of size sample_size
    """
    label = torch.from_numpy(np.full((sample_size,2),[1.0, 1.0]))
    # Import the trained model for p
    cSWAE_pz = torch.load(model_pz)
    cSWAE_pz.eval()
    lat_input_pz = trapezoid(batch_size=sample_size, lat_dim = 2)
    gen_pz = cSWAE_pz.decode(lat_input_pz, label.float())
    gen_pz = gen_pz.detach().numpy()
    gen_pz = gen_pz.flatten().tolist()
	
    return gen_pz

def generate_pT(sample_size, model_pT='model_summaries/pT_SWAE/pT_SWAE_fe_target_skew_gauss_epoch120_latd4_rw20_np25/pT_SWAE_fe_target_skew_gauss_epoch120_latd4_rw20_np25.pth'):
    """
    Generates pT values from trained SWAE models to be used in the fragmentation chain
    
    sample_size: int, number of points to sample from
    model_pT:    string, path to model file 

    Returns pT list of size sample_size
    """
    label = torch.from_numpy(np.full((sample_size,2),[1.0, 1.0]))
    # Import the trained model for p
    cSWAE_pT = torch.load(model_pT)
    cSWAE_pT.eval()
    lat_input_pT = skew_gauss(batch_size=sample_size, lat_dim = 4)
    gen_pT = cSWAE_pT.decode(lat_input_pT, label.float())
    gen_pT = gen_pT.detach().numpy()
    gen_pT = gen_pT.flatten().tolist()

    return gen_pT

def generate_px_py_pz(sample_size):
    """
    Generates px, py, pz values from trained SWAE models to be used in the fragmentation chain
    
    sample_size: number of points to sample from

    Returns px,py,pz floats
    """
    pz = generate_pz(sample_size)
    pT = generate_pT(sample_size)

    # Convert to numpy arrays
    pT = np.array(pT)
    # Sort array
    pT = np.sort(pT)
    # Select a random integer 
    rndm = random.randint(0,len(pT)-1)
    # Select pT
    pT = pT[rndm]
    # Sample uniform phi
    phi = 2*pi*np.random.uniform()
    # Select px and py
    px = pT*cos(phi)
    py = pT*sin(phi)


    return px, py, pz 
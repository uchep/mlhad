"""
# cSWAE.py is a part of the MLHAD package.
# Copyright (C) 2022 MLHAD authors (see AUTHORS for details).
# MLHAD is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
"""

import torch
from torch import nn
from torch.utils.data import DataLoader
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

class cSWAE(nn.Module):
    """
    Conditional Sliced Wasserstein Autoencoder implementation (cSWAE); The SWAE part follows by part arXiv:1804.01947
    """
    def __init__(self, target_distribution, p=2, num_projection=50, reg_weight=100, 
                 input_dim=100,  output_dim=100, latent_dim=20, conditional=True, num_labels=10, 
                 ):
        """
        Initialize the class.

        Args:
            target_distribution (str): distribtution of the latent space
            p (int)                  : power of the distance metric
            num_projection (int)     : Number of random projections
            reg_weight (float)       : regulates the contribution of the sliced Wasserstein distance
            input_dim (int)          : input dimension of the data
            output_dim (int)         : output dimension of the data
            latent_dim (int)         : dimension of the embedding in the latent space
            conditional (bool)       : set to True for passing a condition
            num_labels (int)         : if conditional, set the number of labels for the condition
        """
        super().__init__()
        self.target_distribution = target_distribution
        self.p = p
        self.num_projection = num_projection
        self.reg_weight = reg_weight
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.latent_dim = latent_dim
        self.conditional = conditional
        self.num_labels = num_labels

        self.init_num_filters_ = 16
        lrelu_slope_ = 0.2
        inter_fc_dim_ = 128
#        self.latent_dim = latent_dim 
#        n_dim = 100
        in_channels = 1

        # Define the device
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu") 

        if conditional:
            ## Encoder with condition first layer initialization ##
            # NN bringing the size of the one hot encoded label (condition) to the
            # same size as the input data
            self.embed_class = nn.Linear(num_labels, self.input_dim)
            self.embed_data = nn.Conv1d(in_channels, in_channels, kernel_size=1)
            in_channels += 1
            initial_num_filter = self.init_num_filters_

            ## Decoder with condition first layer initialization ##           
            # Adding the number of labels to the size of the latent dim 
            # since they will be merged in the decoding part
            dec_input_size = self.latent_dim + num_labels
            
        else:
            # if there is no condition the decoder input dimension size will be 
            # the same as the latent dimension size
            dec_input_size = self.latent_dim
            initial_num_filter = self.init_num_filters_

        ########### ENCODER ###########
        # convolution layers to extract more features
        self.features_encode = nn.Sequential(
            nn.Conv1d(in_channels, self.init_num_filters_ * 1, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),
                
            nn.Conv1d(self.init_num_filters_ * 1, self.init_num_filters_ * 1, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),
            nn.AvgPool1d(kernel_size=2, padding=0),
                
            nn.Conv1d(self.init_num_filters_ * 1, self.init_num_filters_ * 2, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),
                
            nn.Conv1d(self.init_num_filters_ * 2, self.init_num_filters_ * 2, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),
            nn.AvgPool1d(kernel_size=2, padding=0),
                
            nn.Conv1d(self.init_num_filters_ * 2, self.init_num_filters_ * 4, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),
                
            nn.Conv1d(self.init_num_filters_ * 4, self.init_num_filters_ * 4, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),
            nn.AvgPool1d(kernel_size=2, padding=1)
        )
            
        # Final layer of the encoder (using fully connected (fc) layers). Embedding the data to the right latent dimension
        self.fc_encode = nn.Sequential(
            nn.Linear(int(1664/2), inter_fc_dim_),
            nn.ReLU(inplace=True),
            nn.Linear(inter_fc_dim_, self.latent_dim)
        )
        
        ############ DECODER  ############
        # fully connected layers (fc) 
        self.fc_decode = nn.Sequential(
            nn.Linear(dec_input_size, inter_fc_dim_),
            nn.Linear(inter_fc_dim_, self.init_num_filters_ * 4 * 4 *4),
            nn.ReLU(inplace=True)
        )
        # convolution layers to learn more features
        self.features_decode = nn.Sequential(
            nn.Upsample(scale_factor=2),
            
            # for input_dim =100
            nn.Conv1d(self.init_num_filters_*4 , self.init_num_filters_ * 4, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),

            nn.Conv1d(self.init_num_filters_ * 4, self.init_num_filters_ * 4, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),

            nn.Upsample(scale_factor=2),
            nn.Conv1d(self.init_num_filters_ * 4, self.init_num_filters_ * 4, kernel_size=3, padding=0),
            nn.LeakyReLU(lrelu_slope_, inplace=True),
            
            nn.Conv1d(self.init_num_filters_ * 4, self.init_num_filters_ * 4, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),

            nn.Upsample(scale_factor=2),
            nn.Conv1d(self.init_num_filters_ * 4, self.init_num_filters_ * 2, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),
                
            # If input_dim=100 
            nn.Conv1d(self.init_num_filters_ * 2, 100, kernel_size=3, padding=1),
            nn.LeakyReLU(lrelu_slope_, inplace=True),
            nn.Conv1d(self.output_dim, 1, kernel_size=111, padding=1),
           ) 
        #####################################################################################
    def idx2onehot(idx, n):
        """
        Given an index "idx" and the size of the class "n", this function returns a one hot
        vector of the index.
        """
        assert torch.max(idx).item() < n

        if idx.dim() == 1:
            idx = idx.unsqueeze(1)
        onehot = torch.zeros(idx.size(0), n).to(idx.device)
        onehot.scatter_(1, idx, 1)
    
        return onehot

    def encode(self, x, c=None):
        """
        Encodes the input and returns the latent dimension.

        Args:
            x : input data
            c : condition vector
        """
        if self.conditional:
            c_hot = c            
            # Neural net, which takes the one hot vector and returns it in the size of the input data
            embedded_class = self.embed_class(c_hot.float())
            embedded_class = embedded_class.view(embedded_class.size()[0], 1, embedded_class.size()[1] )#.unsqueeze(1)# (128,1,100)
            
            x = x.view(x.size()[0],1, x.size()[1])
            # embedd the input data
            embedded_input = self.embed_data(x)
            # merge the input data with the embedded class
            x = torch.cat([embedded_input, embedded_class], dim = 1)
        
        else:
            x = x.view(-1,1,x.shape[1])

        # pass the input data through the feature layers
        x = self.features_encode(x)
        self.feat_en_out_shape = x.shape[1]*x.shape[2] 
        # reshape the data such we can pass it through the fully connected layer
        x = x.view(-1, x.shape[1]*x.shape[2])
            
        x = self.fc_encode(x)            
        return x
    
    def decode(self, z, c=None):
        """
        Maps the latent dimension to the output dimension.

        Args:
            z : latent dimension
            c : conditional vector
        """
        if self.conditional:
            # merging the latent dimenion and the condition vector
            z = torch.cat((z,c), dim=1)

        # passing z through the fully connected layers        
        z = self.fc_decode(z)
        # reshape z
        z = z.view(-1, 4*self.init_num_filters_, 4*4*4)
        # passing z through the feature layers
        z = self.features_decode(z)
        z = z.view(-1,100)
        return z        

    def forward(self, x, c=None):
        """
        Function that defines the network structure.

        Args:
            x : Input data
            c : condition vector
        """

        # Encoding the input
        z = self.encode(x,c)
        
        # Decoding the latent space
        if self.conditional: dec_z = self.decode(z,c)
        else: dec_z = self.decode(z)

        return dec_z , z
    
    def projections(self,latent_dim, n_samples):
        """
        Generates random samples from the latent space unit sphere.
        """ 
        # Normalized sample from a normal distribution with L2 normalization
        projections = [w / np.sqrt((w**2).sum()) for w in np.random.normal(size=(n_samples,latent_dim))]
        projections = np.asarray(projections)

        return torch.from_numpy(projections).type(torch.FloatTensor)
    
    def sliced_wasserstein_distance(self, z, reg_weight, c=None, p=2):
        """
        SW distance between the encoded samples and the target distribution samples.
        
        Args:
            z : encoded laten dimension
            p : power of distance metric
            c : condition vector

        Returns the Sliced Wasserstein Distance (swd) loss. 
        """
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        distribution_fn= self.target_distribution
        
        batch_size = z.size(0)
        num_proj= self.num_projection

        # target distribution
        if self.conditional:
            target_distribution_sample = distribution_fn(batch_size=batch_size, lat_dim=self.latent_dim, c_label=c).to(device)
        else: 
            target_distribution_sample = distribution_fn(batch_size=batch_size, lat_dim=self.latent_dim).to(device)
           
        # Create the projection matrix
        projection_matrix = self.projections( self.latent_dim, n_samples = num_proj).to(device)
        
        # Projection of the encoded samples (the encoded sample gets projected)
        lat_projection = z.matmul(projection_matrix.t())
        
        # Projection of the target distribution
        target_distribution_projection = target_distribution_sample.matmul(projection_matrix.t()) 
        
        # Computing the Wasserstein distance from the two projected distribution
        # The Wasserstein distance is the sorted difference   
        wasserstein_distance = (torch.sort(lat_projection.t(), dim=1)[0] -
                               torch.sort(target_distribution_projection.t(), dim=1)[0] )
        
        wasserstein_distance = torch.pow(wasserstein_distance, p)

        return reg_weight*wasserstein_distance.mean()

        
    def loss_function(self, x_hat, x, z, c =None):
        """
        This function calculates the total loss, which is composed of the recunstruction and 
        the sliced Wasserstein distance loss.

        Args:
            x_hat : recunstructed data
            x     : input data
            z     : embedded latent dimension
            c     : condition vector

        Returns the total loss, swd loss and the reconstruction loss.
        """

        out_dim = self.output_dim
        
        #------------------------- RECONSTRUCTION LOSS --------------------------#
        recons_loss_l1 = nn.functional.l1_loss(x_hat, x)
        recons_loss_l2 = nn.functional.mse_loss(x_hat,x)
        
        #------------------------------- SWD LOSS -------------------------------#
        if self.conditional:
            swd_loss = self.sliced_wasserstein_distance(z,  reg_weight= self.reg_weight, p=self.p, c=c)
        else:
            swd_loss = self.sliced_wasserstein_distance(z,  reg_weight= self.reg_weight, p=self.p)
        #------------------------------ TOTAL LOSS ------------------------------#
        loss = recons_loss_l1 + recons_loss_l2 + swd_loss
      
        return loss, swd_loss, recons_loss_l1, recons_loss_l2
    
    def Train(self, train, test, batch_size=1024,epochs=3,
                 learning_rate=0.001, num_workers=0):
        """
        This is the training function.

        Args:
            train : training data set
            test  : test data set
        """
        # Define the device
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        # Load the data in via PyTorch utilities
        train_loader = torch.utils.data.DataLoader(train, batch_size=batch_size,
                                            shuffle=False, num_workers=num_workers)

        test_loader = torch.utils.data.DataLoader(test, batch_size=batch_size,
                                            shuffle=False, num_workers=num_workers)

        # Setting the optimizer 
        optimizer = torch.optim.Adam(self.parameters(),lr=learning_rate)

        # regulator for training; if the training doesn't improve over 10 epochs: stop the training ; to avoid over fitting
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode= 'min', factor = 0.85, patience = 10)

        # Training and testing cSWAE    
        codes = dict(μ=list(), logσ2=list(), y=list())
        avg_loss = []
        epoch_list = []
        for epoch in tqdm(range(0, epochs + 1), ncols = 100):
            # Training
            if epoch > 0:  # test untrained net first
                self.train() 
                train_loss, rec_loss_l1, rec_loss_l2, swd_loss = 0, 0, 0, 0
                for y, x in train_loader:
                    x, y = x.to(device), y.to(device)
                    # ===================forward=====================
                    if self.conditional:
                        #c = idx2onehot(y, n=10)
                        c = y
                        x_hat, z = self(x,c)
                        loss, swd, recl1, recl2 = self.loss_function(x_hat, x, z, c)
                    else:
                        x_hat, z = self(x) #model(x)
                        loss, swd, recl1, recl2 = self.loss_function(x_hat, x, z)

                    train_loss += loss.item()
                    rec_loss_l1 += recl1.item()
                    rec_loss_l2 += recl2.item()
                    swd_loss += swd.item()

                    # =================== backward ====================
                    optimizer.zero_grad()
                    loss.backward()
                    optimizer.step()
                scheduler.step(loss.item())

                # =================== log ========================
                if epoch%5==0:
                    print()
                    print('----> Learning rate: ', optimizer.param_groups[0]['lr'])
                    print(f'----> Epoch: {epoch} Average loss: {train_loss/ len(train_loader.dataset):.5f} - rec_loss_l1: {rec_loss_l1/ len(train_loader.dataset):.5f}  - rec_loss_l2: {rec_loss_l2/ len(train_loader.dataset):.5f} - swd_loss: {swd_loss/ len(train_loader.dataset):.5f}') 
                    avg_loss.append(train_loss/ len(train_loader.dataset))
                    epoch_list.append(epoch)

            ################### TESTING ###################

            with torch.no_grad():
                self.eval() 
                test_loss, rec_loss_l1, rec_loss_l2, swd_loss = 0, 0, 0, 0
    
                # y are the labels which we usually do not need, here save them for the analysis later
                for y, x in test_loader: 
                    x, y = x.to(device), y.to(device)
                    # ===================forward=====================
                    if self.conditional:
                        c =y
                        x_hat, z = self(x,c)
                        loss, swd, recl1, recl2  = self.loss_function(x_hat, x, z, c)
                    else:
                        x_hat, z =  self(x) # model(x)
                        loss, swd, recl1, recl2  = self.loss_function(x_hat, x, z)

                    #loss_function(x_hat, x, mu, logvar)
                    test_loss += loss.item()
                    rec_loss_l1 += recl1.item()
                    rec_loss_l2 += recl2.item()
                    swd_loss += swd.item()

        # ===================log========================

            test_loss /= len(test_loader.dataset)
            rec_loss_l1 /= len(test_loader.dataset)
            rec_loss_l2 /= len(test_loader.dataset)

            swd_loss /= len(test_loader.dataset)
            # print the loss after every 5 epochs
            if epoch%5==0 and epoch !=0:
                print(f'----> Test set loss: {test_loss:.5f} - rec_loss_l1: {rec_loss_l1:.5f} - rec_loss_l2: {rec_loss_l2:.5f} - swd_loss: {swd_loss:.5f}') 